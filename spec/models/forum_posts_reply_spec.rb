require 'spec_helper'

describe ForumPostsReply do
  it 'processes messages' do
    stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(1), :headers => {})
    stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=100&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(2), :headers => {})
    stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=200&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(3), :headers => {})
    stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_posts_response(1), :headers => {})
    stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=100&submit=Search").to_return(:status => 200, :body => test_forums_posts_response(2), :headers => {})
    stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=200&submit=Search").to_return(:status => 200, :body => test_forums_posts_response(3), :headers => {})
    ForumPostsReply.prepare_forum_posts_reply 2013
    real_data = test_reply_data("forum_posts",2)
    expect(ForumPostsReply.count).to eq(real_data.length)
    real_data.each do |entry|
      expect(ForumPostsReply.where(entry).count).to eq(1)
    end
  end
end
