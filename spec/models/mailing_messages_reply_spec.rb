require 'spec_helper'

describe MailingMessagesReply do
  it 'generates entries for a mailing list' do
    expect(Project).to receive(:all_messages).with("test3.mbox").and_return(test_all_messages("test3.mbox"))
    MailingMessagesReply.generate_for("test3.mbox")
    real_data = test_reply_data("mailing_messages",3)
    expect(MailingMessagesReply.count).to eq(real_data.length)
    real_data.each do |entry|
      expect(MailingMessagesReply.where(entry).count).to eq(1)
    end
  end
end
