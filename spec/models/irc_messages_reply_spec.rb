require 'spec_helper'

describe IrcMessagesReply do
  it 'processes messages' do
    Timecop.freeze(Time.new(2015,6,22,17,34,3)) # Using freeze because we need consistent calculations here.
    messages = irc_test_messages(2)
    real_data = test_reply_data("irc_messages",2)
    messages.each do |message|
      IrcMessagesReply.process_message(*message)
    end
    expect(IrcMessagesReply.count).to eq(real_data.length)
    real_data.each do |entry|
      expect(IrcMessagesReply.where(entry).count).to eq(1)
    end
    Timecop.return
  end
end
