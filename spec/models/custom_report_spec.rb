require 'spec_helper'

describe CustomReport do
  # Marks generate type report as checked in the given custom report.
  # This does not make sure others are not checked.
  def add_types_to_cr(cr, to_include)
    to_include.each do |type|
      cr["generate_#{type}_report"] = true
    end
    cr.save
  end

  describe 'is created and validated' do
    it 'has a valid factory' do
      expect(FactoryGirl.build(:custom_report)).to be_valid
    end

    it 'is invalid without a title' do
      expect(FactoryGirl.build(:custom_report, title: nil)).not_to be_valid
    end

    it 'should have many projects' do
      project = FactoryGirl.create(:project)
      cr = FactoryGirl.create(:custom_report, projects: [project])
      expect(cr.projects.count).to eq(1)
    end
  end

  it 'adds delayed jobs' do
    expect do
      FactoryGirl.create(:custom_report)
    end.to change(Delayed::Job, :count).by(1)
  end

  it 'recognizes the included reports' do
    cr = FactoryGirl.create(:commits_custom_report)
    expect(cr.is_included?("commits")).to be true
    expect(cr.is_included?("forum_posts")).to be false
  end

  it 'prepares reports' do
    cr = FactoryGirl.create(:custom_report)
    expect{cr.prepare_reports}.to change(cr, :generated).from(false).to(true)
  end

end
