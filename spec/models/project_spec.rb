require 'spec_helper'
require "forwardable"

describe Project do

  describe 'is created and validated' do

    it 'has a valid factory' do
      FactoryGirl.create(:project).should be_valid
    end

    it 'is invalid without a title' do
      FactoryGirl.build(:project, title: nil).should_not be_valid
    end

    it 'is valid without a description' do
      FactoryGirl.build(:project, description: nil).should be_valid
    end

    it 'is valid without a large description' do
      FactoryGirl.build(:project, large_description: nil).should be_valid
    end

    it 'is valid without bugzilla products' do
      FactoryGirl.build(:project, bugzilla_products: nil).should be_valid
    end

    it 'is valid without git repos' do
      FactoryGirl.build(:project, git_repos: nil).should be_valid
    end

    it 'is valid without mailing lists' do
      FactoryGirl.build(:project, mailing_lists: nil).should be_valid
    end

    it 'is valid without irc channels' do
      FactoryGirl.build(:project, irc_channels: nil).should be_valid
    end

    it 'is valid without a parent id' do
      FactoryGirl.build(:project, parent_id: nil).should be_valid
    end

    it 'is valid without bugs report fields array' do
      FactoryGirl.build(:project, bugs_report_fields: nil).should be_valid
    end

    it 'calls do_jobs after being committed' do
      Project.any_instance.should_receive(:do_jobs)
      FactoryGirl.create(:project)
    end

  end

  describe 'bugs part' do

    before(:each) do
      # I have to make it this long because doing it elegantly doesn't work with FactoryGirl.
      # Consider using Fabricator.
      @project = FactoryGirl.build(:project)
      @project.bugs_data = FactoryGirl.build(:test_bugs_data_1)
      @project.save
      @bugs_data = @project.bugs_data
      @bugs_data.total_number.should eq(9)
    end

    describe 'handles data' do

      it 'updates data for an old project' do
        number_of_bugs = rand(500000)
        @project.set_bugs_data ["total_number"],[number_of_bugs]
        Project.find(@project.id).bugs_data.total_number.should eq(number_of_bugs)
      end

      it 'updates multiple data at once' do
        test_keys = ["total_number","assigned_to","net_open_line"]
        test_values = [rand(500000),[["a@m.com",4]],[rand(5000),rand(5000)]]
        @project.set_bugs_data test_keys,test_values
        bugs_data = Project.find(@project.id).bugs_data
        test_keys.zip(test_values).each do |key,value|
          bugs_data[key].should eq(value)
        end
      end

      it 'gets all data for a project' do
        fetched_bugs_data = @project.get_all_bugs_data
        fetched_bugs_data.should eq(@bugs_data)
      end

      it 'gets bug day index' do
        @project.get_bug_day_index(Time.now.utc).should eq(4)
        @project.get_bug_day_index(4.days.ago).should eq(0)
      end

    end

    context 'list bugs' do

      before(:each) do
        @project.bugs_data.total_number = 100
      end

      it 'provides a list of latest 10 bugs by default' do
        stub_request(:get,  "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22abakus%22,%22aki%22%5D,%22include_fields%22:%5B%22id%22,%22assigned_to%22,%22component%22,%22last_change_time%22,%22status%22,%22summary%22%5D,%22limit%22:10,%22offset%22:90,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return{ |request| {:status => 200, :body => bugs_list(90)} }
        bugs_array = @project.list_of_bugs
        real_bugs_ids = bugs_list_ids(90).reverse
        bugs_array[0]["id"].should eq(real_bugs_ids[0])
        bugs_array[4]["id"].should eq(real_bugs_ids[4])
        bugs_array[9]["id"].should eq(real_bugs_ids[9])
      end

      it 'provides a list of the latest number of bugs' do
        stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22abakus%22,%22aki%22%5D,%22include_fields%22:%5B%22id%22,%22assigned_to%22,%22component%22,%22last_change_time%22,%22status%22,%22summary%22%5D,%22limit%22:5,%22offset%22:95,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_list(95,5))
        bugs_array = @project.list_of_bugs(1,5)
        real_bugs_ids = bugs_list_ids(85,15).reverse
        bugs_array[0]["id"].should eq(real_bugs_ids[0])
        stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22abakus%22,%22aki%22%5D,%22include_fields%22:%5B%22id%22,%22assigned_to%22,%22component%22,%22last_change_time%22,%22status%22,%22summary%22%5D,%22limit%22:15,%22offset%22:85,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_list(85,15))
        bugs_array = @project.list_of_bugs(1,15)
        bugs_array.last["id"].should eq(real_bugs_ids.last)
      end

    end

    it 'provides general report data' do
      the_bugs_array = bugs_list_array
      @project.should_receive(:list_of_bugs).and_return(the_bugs_array)
      general_report_data = @project.bugs_general_report
      general_report_data.class.should eq(Hash)
      general_report_data["number_of_bugs"].should eq(@bugs_data.total_number)
      general_report_data["latest_bugs"].should eq(the_bugs_array)
      top_bug_resolvers = Array.new
      top_bug_resolvers << {"contributor" => "aaa@aa.com", "number_of_resolved_bugs" => 3}
      general_report_data["top_bug_resolvers"].should eq(top_bug_resolvers)
      top_bug_reporters = Array.new
      top_bug_reporters << {"contributor" => "xxx@xx.com", "number_of_created_bugs" => 5}
      top_bug_reporters << {"contributor" => "xx@xx.com", "number_of_created_bugs" => 4}
      general_report_data["top_bug_reporters"].should eq(top_bug_reporters)
    end

    it 'generates bugs report' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22abakus%22,%22aki%22%5D,%22include_fields%22:%5B%22assigned_to%22,%22classification%22,%22component%22,%22creator%22,%22op_sys%22,%22platform%22,%22priority%22,%22product%22,%22severity%22,%22status%22,%22version%22,%22creation_time%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_report_test_response(1), :headers => {})
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22abakus%22,%22aki%22%5D,%22include_fields%22:%5B%22creation_time%22,%22last_change_time%22,%22status%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_line_graph_report_test_response(1))
      Project.any_instance.should_receive(:prepare_bugs_report_files)
      Timecop.travel(Date.new(2014,5,14))
      @project.prepare_bugs_report
      Timecop.return
      new_bugs_data = Project.find(@project.id).bugs_data
      real_bugs_data = bugs_test_report(1).merge(bugs_line_graph_report_test_report(1))
      real_bugs_data.each do |key,value|
        if value.is_a? Array
          new_bugs_data[key].should match_array(value)
        else
          new_bugs_data[key].should eq(value)
        end
      end
    end

    it 'generates bugs report when number of products is more than 10' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22AVKode%22,%22Active%22,%22Akonadi%22,%22Artikulate%22,%22Baloo%22,%22Granatier%22,%22HIG%22,%22KAddressbook%20Mobile%22,%22KBibTeX%22,%22KCall%22%5D,%22include_fields%22:%5B%22assigned_to%22,%22classification%22,%22component%22,%22creator%22,%22op_sys%22,%22platform%22,%22priority%22,%22product%22,%22severity%22,%22status%22,%22version%22,%22creation_time%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_report_test_response(3))
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22KDE%20PIM%20Mobile%22,%22KDE-Eclipse%22,%22KExtProcess%22,%22KJots%20Mobile%22,%22KMail%20Mobile%22%5D,%22include_fields%22:%5B%22assigned_to%22,%22classification%22,%22component%22,%22creator%22,%22op_sys%22,%22platform%22,%22priority%22,%22product%22,%22severity%22,%22status%22,%22version%22,%22creation_time%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_report_test_response(4))
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22AVKode%22,%22Active%22,%22Akonadi%22,%22Artikulate%22,%22Baloo%22,%22Granatier%22,%22HIG%22,%22KAddressbook%20Mobile%22,%22KBibTeX%22,%22KCall%22%5D,%22include_fields%22:%5B%22creation_time%22,%22last_change_time%22,%22status%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_line_graph_report_test_response(3))
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22KDE%20PIM%20Mobile%22,%22KDE-Eclipse%22,%22KExtProcess%22,%22KJots%20Mobile%22,%22KMail%20Mobile%22%5D,%22include_fields%22:%5B%22creation_time%22,%22last_change_time%22,%22status%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_line_graph_report_test_response(4))
      Project.any_instance.should_receive(:prepare_bugs_report_files)
      @project.bugzilla_products = ["AVKode", "Active", "Akonadi", "Artikulate", "Baloo", "Granatier", "HIG", "KAddressbook Mobile", "KBibTeX", "KCall", "KDE PIM Mobile", "KDE-Eclipse", "KExtProcess", "KJots Mobile", "KMail Mobile"]
      @project.save
      @project.prepare_bugs_report
      data = @project.bugs_data
      data["platform"].should eq([["unspecified",1],["Compiled Sources",2]])
      data["assigned_to"].should eq([["xzekecomax@gmail.com",3]])
    end

    it 'updates bugs line graphs daily' do
      old_reported_line = @bugs_data["reported_line"].dup
      old_resolved_line = @bugs_data["resolved_line"].dup
      old_total_created_line = @bugs_data.total_created_line.dup
      old_net_open_line = @bugs_data.net_open_line.dup
      @project.update_bugs_line_graphs_daily
      new_bugs_data = Project.find(@project.id).bugs_data
      new_bugs_data["reported_line"].should eq((old_reported_line << 0))
      new_bugs_data["resolved_line"].should eq((old_resolved_line << 0))
      new_bugs_data["total_created_line"].should eq((old_total_created_line << old_total_created_line.last))
      new_bugs_data["net_open_line"].should eq((old_net_open_line << old_net_open_line.last))
    end

    it 'provides bugs report' do
      bugs_report = @project.get_bugs_report
      real_bugs_data = bugs_data_test 1
      real_bugs_data.each do |key,value|
        bugs_report[key].should(eq(value)) unless key == "total_number" || key == "status"
      end
      bugs_report["bugs_by_status"].should eq(real_bugs_data["status"])
      bugs_report.keys.length.should eq(real_bugs_data.keys.length)
    end

    it 'prepares all time line graphs' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22abakus%22,%22aki%22%5D,%22include_fields%22:%5B%22creation_time%22,%22last_change_time%22,%22status%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_line_graph_report_test_response(1))
      Timecop.travel(Date.new(2014,5,14))
      bugs_line_graphs = @project.prepare_all_time_bugs_line_graphs
      real_bugs_line_graphs = bugs_line_graph_report_test_report 1
      Timecop.return
      real_bugs_line_graphs.each do |key,value|
        bugs_line_graphs[key].should eq(value)
      end
    end

    it 'counts bugs by properties' do
      result = @project.count_bugs_by("priority",count_bugs_by_successful(1))
      result.should eq(count_bugs_by_result(1,"priority"))
      result = @project.count_bugs_by("assigned_to",count_bugs_by_successful(1),["status","RESOLVED"])
      result.should eq(count_bugs_by_result(1,"resolved_by_assigned_to"))
    end

    it 'generates needed files to view bugs report' do
      creators = [["t1", 100], ["t2", 101], ["t3", 102], ["t4", 103], ["t5", 104], ["t6", 105], ["t7", 106], ["t8", 107], ["t9", 108], ["t10", 109], ["t11", 110], ["t12", 111], ["t13", 112], ["t14", 113], ["t15", 114], ["t16", 115]]
      @project.bugs_data = FactoryGirl.build(:test_bugs_data_1, :creator => creators, :severity => [])
      @project.title = "myproject"
      @project.save
      real_files = generated_report_files
      FakeFS do
        @project.prepare_bugs_report_files
        path = "public/en/projects/#{@project.path}/bugs/"
        real_files.each do |k,v|
          File.open(path+k+".json").read.should eq(v)
        end
        File.exist?(path+"severity.json").should eq(false)
      end
    end

    it 'provides the number of bugs' do
      @project.number_of_bugs.should eq(@bugs_data.total_number)
    end

    it 'fetches a bug with id' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.get&params=%5B%7B%22ids%22:%5B335083%5D%7D%5D").to_return(:status => 200, :body => fetch_bug_response(335083))
      the_bug = Project.fetch_bug(335083)
      real_bug = fetch_bug_result(335083)
      real_bug.each do |key,value|
        the_bug[key].should eq(value)
      end
    end

    it 'fetches comments for a bug' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.comments&params=%5B%7B%22ids%22:%5B335083%5D%7D%5D").to_return(:status => 200, :body => fetch_comments_response(335083))
      comments = Project.fetch_comments(335083)
      real_comments = fetch_comments_result(335083)
      comments.length.should eq(real_comments.length)
      real_comments.zip(comments).each do |real,fetched|
        real.each do |key,value|
          fetched[key].should eq(value)
        end
      end
    end

    it 'updates bugs report for projects containing products affected by the new bug' do
      data = "test_data"
      Project.any_instance.should_receive(:perform_bugs_report_update).with(data) # @project is affected.
      Project.any_instance.should_receive(:prepare_bugs_report_files)
      Project.update_bugs "aki", data
    end

    it 'does not update bugs report for projects not containing products affected by the new bug' do
      @project.destroy
      FactoryGirl.create(:project, bugzilla_products: ["calligraauthor","amarok"])
      Project.any_instance.should_not_receive(:perform_bugs_report_update)
      Project.any_instance.should_not_receive(:prepare_bugs_report_files)
      Project.update_bugs "aki", "test_data"
    end

    it 'fetches a list of selectable products' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Product.get_selectable_products").to_return(:status => 200, :body => fetch_selectable_products_ids)
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Product.get&params=%5B%7B%22include_fields%22:%5B%22name%22%5D,%22ids%22:%5B%22347%22,%20%22512%22,%20%22338%22,%20%22433%22%5D%7D%5D").to_return(:status => 200, :body => fetch_selectable_products_names)
      selectable_products = Project.list_selectable_products
      real_selectable_products = real_selectable_products_result
      real_selectable_products.count.should eq(selectable_products.count)
      real_selectable_products.each do |val|
        selectable_products.include?(val).should eq(true)
      end
    end

    context 'updates bugs report when a new bug is reported' do

      before(:each) do
        @old_bugs_data = @project.bugs_data.dup
        @bug_data = fetch_bug_result(335083)
        @project.perform_bugs_report_update(@bug_data)
      end

      it 'increments the total number of bugs' do
        @project.bugs_data.total_number.should eq(@old_bugs_data.total_number + 1)
      end

      it 'makes no changes to the start date' do
        @project.bugs_data.start_date.should eq(@old_bugs_data.start_date)
      end

      context 'pie charts' do

        describe "classification" do
          it_should_behave_like "updated_property_on_new_bug","classification"
        end

        describe "component" do
          it_should_behave_like "updated_property_on_new_bug","component"
        end

        describe "creator" do
          it_should_behave_like "updated_property_on_new_bug","creator"
        end

        describe "op_sys" do
          it_should_behave_like "updated_property_on_new_bug","op_sys"
        end

        describe "open by priority" do
          it_should_behave_like "updated_property_on_new_bug","open_by_priority","priority"
        end

        describe "platform" do
          it_should_behave_like "updated_property_on_new_bug","platform"
        end

        describe "priority" do
          it_should_behave_like "updated_property_on_new_bug","priority"
        end

        describe "product" do
          it_should_behave_like "updated_property_on_new_bug","product"
        end

        describe "severity" do
          it_should_behave_like "updated_property_on_new_bug","severity"
        end

        describe "status" do
          it_should_behave_like "updated_property_on_new_bug","status"
        end

        describe "version" do
          it_should_behave_like "updated_property_on_new_bug","version"
        end

        it 'makes no change to resolved by assigned to' do
          @project.bugs_data.resolved_by_assigned_to.should eq(@old_bugs_data.resolved_by_assigned_to)
        end

      end

      context 'line_charts' do

        describe 'net open line' do
          it_should_behave_like "updated_line", "net_open_line", "increment"
        end

        describe 'net open line' do
          it_should_behave_like "updated_line", "still_open_line", "increment"
        end

        describe 'reported line' do
          it_should_behave_like "updated_line", "reported_line", "increment"
        end

        describe 'total created line' do
          it_should_behave_like "updated_line", "total_created_line", "increment"
        end

        describe 'resolved line' do
          it_should_behave_like "updated_line", "resolved_line", "no-change" # This one should not be changed.
        end

      end

    end

    it 'updates bugs report for projects containing products affected by existing bug change' do
      product = "aki"
      data = "test_data"
      bug_id = rand(50000)
      Project.any_instance.should_receive(:perform_bugs_report_update_existing).with(data, bug_id) # @project is affected.
      Project.any_instance.should_receive(:prepare_bugs_report_files)
      Project.update_existing_bug product, data, bug_id
    end

    it 'does not update bugs report for projects not containing products affected by existing bug change' do
      product = "aki"
      data = "test_data"
      bug_id = rand(50000)
      @project.destroy
      FactoryGirl.create(:project, bugzilla_products: ["calligraauthor","amarok"])
      Project.any_instance.should_not_receive(:perform_bugs_report_update)
      Project.any_instance.should_not_receive(:prepare_bugs_report_files)
      Project.update_existing_bug product, data, bug_id
    end

    context 'updates bugs report when an existing bug is updated' do

      before(:each) do
        @project.bugs_data.start_date = Date.new(2014,5,18)
        @old_bugs_data = @project.bugs_data.dup
        @bug_id = 335083.to_s
      end

      context "pie_charts" do

        describe "Assigned to" do
          data = [["Assignee", "aa@aaa.com", "sometester@tester.com"]]
          modified_items = ["assigned_to"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        describe "Classification" do
          data = [["Classification", "Unclassified", "Classified"]]
          modified_items = ["classification"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        describe "Component" do
          data = [["Component", "general", "Context"]]
          modified_items = ["component"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        it 'does not edit creator' do
          @project.bugs_data.creator.should eq(@old_bugs_data.creator)
          @project.perform_bugs_report_update_existing([["Component", "general", "Context"]],@bug_id)
        end

        describe "Operating system" do
          data = [["OS", "Linux", "All"]]
          modified_items = ["op_sys"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        describe 'Open by priority' do

          before(:each) do
            Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
          end

          it 'decrements the bug priority when resolved' do
            data = [["Status","CONFIRMED","RESOLVED"]]
            @project.perform_bugs_report_update_existing data, @bug_id
            bug_data = fetch_bug_result(@bug_id)
            old_data = @old_bugs_data.open_by_priority
            new_data = @project.bugs_data.open_by_priority
            old_data.each do |item|
              unless item[0] == bug_data["priority"]
                new_data.include?(item).should eq(true)
              else
                new_num = item[1]-1
                if new_num == 0
                  new_data.length.should eq(old_data.length-1)
                else
                  updated = [item[0],new_num]
                  new_data.include?(updated).should eq(true)
                  new_data.length.should eq(old_data.length)
                end
              end
            end
          end

          it 'increments the bug priority when reopnend' do
            data = [["Status","RESOLVED","REOPENED"]]
            bug_id = 2
            @project.perform_bugs_report_update_existing data, bug_id
            @project.bugs_data.open_by_priority.should eq([["NOR",7]])
          end

          it 'adds new entry in priority when bug is reopened if there was no entry for it' do
            data = [["Status","CLOSED","REOPENED"]]
            bug_id = 3
            @project.perform_bugs_report_update_existing data, bug_id
            @project.bugs_data.open_by_priority.should eq([["HI",1],["NOR",6]])
          end

        end

        describe "Platform" do
          data = [["Platform", "unspecified", "Ubuntu Packages"]]
          modified_items = ["platform"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        describe "Priority" do
          data = [["Priority", "NOR", "HI"]]
          modified_items = ["priority"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        describe "Product" do
          data = [["Product", "abakus", "aki"]]
          modified_items = ["product"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        # These tests depend on the data in the test_bugs_data_1 factory. Changing the data might fail them.
        describe "Resolved by assigned to" do

          before do
            Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
          end

          context "resolved" do

            before do
              @data = [["Status","CONFIRMED","RESOLVED"]]
            end

            it 'increments old entry for the assignee' do
              bug_id = 1
              @project.perform_bugs_report_update_existing @data, bug_id
              new_data = @project.bugs_data.resolved_by_assigned_to
              new_data.should eq([["aaa@aa.com", 4]])
            end

            it 'adds new entry for the assignee when he had no entry before' do
              @project.perform_bugs_report_update_existing @data, @bug_id
              new_data = @project.bugs_data.resolved_by_assigned_to
              new_data.should eq([["amarok-bugs-dist@kde.org",1],["aaa@aa.com", 3]])
            end

          end

          context "Reopened" do

            before do
              @data = [["Status","RESOLVED","REOPENED"]]
              @bug_id = 2
            end

            it 'decrements old entry for the assignee' do
              @project.perform_bugs_report_update_existing @data, @bug_id
              new_data = @project.bugs_data.resolved_by_assigned_to
              new_data.should eq([["aaa@aa.com",2]])
            end

            it 'removes old entry for the assignee when he had 1 initially' do
              @project.bugs_data.resolved_by_assigned_to = [["aaa@aa.com",1]]
              @project.save
              @project.perform_bugs_report_update_existing @data, @bug_id
              @project.bugs_data.resolved_by_assigned_to.should eq([])
            end

          end

          it 'is not changed when the bug becomes REOPENED from state other than RESOLVED' do
            data = [["Status","CLOSED","REOPENED"]]
            bug_id = 2
            @project.perform_bugs_report_update_existing data, bug_id
            @project.bugs_data.resolved_by_assigned_to.should eq(@old_bugs_data.resolved_by_assigned_to)
          end

        end

        describe "Severity" do
          data = [["Severity", "normal", "crash"]]
          modified_items = ["severity"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        it 'does not change start date' do
          data = [["Severity", "normal", "crash"]]
          bug_id = 1
          @project.perform_bugs_report_update_existing data, bug_id
          @project.bugs_data.start_date.should eq(@old_bugs_data.start_date)
        end

        describe "Status" do
          data = [["Status", "UNCONFIRMED", "CONFIRMED"]]
          modified_items = ["status"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        it 'does not change total number' do
          data = [["Severity", "normal", "crash"]]
          bug_id = 1
          @project.perform_bugs_report_update_existing data, bug_id
          @project.bugs_data.total_number.should eq(@old_bugs_data.total_number)
        end

        describe "Version" do
          data = [["Version", "unspecified", "0.0.2"]]
          modified_items = ["version"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

        describe "multiple changes at a time" do
          data = [["Version", "unspecified", "0.0.2"],["Status", "UNCONFIRMED","CONFIRMED"]]
          modified_items = ["version","status"]
          it_should_behave_like "updated_property_on_existing_bug_update",data,modified_items
        end

      end

      context 'line graphs' do

        before(:each) do
          Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
        end

        describe 'Net open line when a bug is reopened' do
          data = [["Status", "RESOLVED", "REOPENED"]]
          it_should_behave_like "updated_line","net_open_line", "increment", data
        end

        describe 'Net open line when a bug is resolved' do
          data = [["Status", "CONFIRMED", "RESOLVED"]]
          it_should_behave_like "updated_line","net_open_line", "decrement", data
        end

        describe 'Net open line when a bug is resolved after being reopened' do
          data = [["Status", "REOPENED", "RESOLVED"]]
          it_should_behave_like "updated_line","net_open_line", "decrement", data
        end

        describe 'Net open line when a bug is changed to NEEDSINFO' do
          data = [["Status", "UNCONFIRMED", "NEEDSINFO"]]
          it_should_behave_like "updated_line","net_open_line", "decrement", data
        end

        describe 'Net open line when a bug is changed to CLOSED' do
          data = [["Status", "UNCONFIRMED", "CLOSED"]]
          it_should_behave_like "updated_line","net_open_line", "decrement", data
        end

        it 'does not change reported line' do
          @project.perform_bugs_report_update_existing([["Status", "RESOLVED", "REOPENED"]],@bug_id)
          @project.bugs_data.reported_line.should eq(@old_bugs_data.reported_line)
        end

        describe 'Resolved line when a bug is resolved' do
          data = [["Status", "CONFIRMED", "RESOLVED"]]
          it_should_behave_like "updated_line","resolved_line", "increment", data
        end

        describe 'Resolved line when a bug is closed' do
          # Notice that Resolved line indicates number of bugs that are not open anymore.
          data = [["Status", "CONFIRMED", "CLOSED"]]
          it_should_behave_like "updated_line","resolved_line", "increment", data
        end

        describe 'Resolved line when a bug is closed' do
          # Notice that Resolved line indicates number of bugs that are not open anymore.
          data = [["Status", "CONFIRMED", "NEEDSINFO"]]
          it_should_behave_like "updated_line","resolved_line", "increment", data
        end

        describe 'Total created line' do
          data = [["Status", "CONFIRMED", "CLOSED"]]
          it_should_behave_like "updated_line","total_created_line", "no_change", data
        end

        it 'updates average close time line on closed bug' do
          orig = [0,0,2,0]
          @project.bugs_data.average_close_time_line.should eq(orig)
          @project.perform_bugs_report_update_existing([["Status", "CONFIRMED", "RESOLVED"]],@bug_id)
          updated = [0,0,1,0]
          @project.bugs_data.average_close_time_line.should eq(updated)
        end

        it 'updates still open line on closed bug' do
          orig = [1,1,1,3]
          @project.bugs_data.still_open_line.should eq(orig)
          @project.perform_bugs_report_update_existing([["Status", "CONFIRMED", "RESOLVED"]],@bug_id)
          updated = [1,1,0,3]
          @project.bugs_data.still_open_line.should eq(updated)
        end

        it 'updates still open line on reopened bug' do
          orig = [1,1,1,3]
          @project.bugs_data.still_open_line.should eq(orig)
          @project.perform_bugs_report_update_existing([["Status", "RESOLVED", "REOPENED"]],335085)
          updated = [1,1,2,3]
          @project.bugs_data.still_open_line.should eq(updated)
        end

      end

      it 'updates total average close time on closed bug' do
        Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
        @project.bugs_data.total_average_close_time.should eq(5)
        @project.perform_bugs_report_update_existing([["Status", "CONFIRMED", "RESOLVED"]],@bug_id)
        @project.bugs_data.total_average_close_time.should eq(15.0/4)
        @project.perform_bugs_report_update_existing([["Status", "CONFIRMED", "RESOLVED"]],335080)
        @project.bugs_data.total_average_close_time.should eq(17.0/5)
      end

    end

  end

  describe 'Commits part' do

    before(:each) do
      Project.stub(:repo) do |product|
        test_repo(product)
      end
    end

    it 'prepares commits data for a project with multiple repos' do
      git_repos = ["quickgit-kde-org.git"]
      project = FactoryGirl.create(:project, git_repos: git_repos.join("\n"))
      project.should_receive(:prepare_report_files).with("commits",false)
      project.prepare_commits_data
      real_commits_data = test_real_commits_data(git_repos)
      project.number_of_commits.should eq(test_real_number_of_commits(git_repos))
      project.commits_data.count.should eq(real_commits_data.count)
      real_commits_data.each do |real_data_entry|
        CommitsData.where(contributor: real_data_entry["contributor"], day: real_data_entry["day"], number: real_data_entry["number"],insertions: real_data_entry["insertions"], deletions: real_data_entry["deletions"], project_id: project.id).count.should eq(1)
      end
    end


    describe 'Last explored commit' do

      before :each do
        git_repos = ["dummy.git","sso-kde-org.git"]
        @project = FactoryGirl.create(:project, git_repos: git_repos.join("\n"))
        @last_sha = ["ca8084d42263d753c8c2cd5a65020a04771b06c5","e0058c534aa9c0260c20102b9b16ed02e5ddb080"]
      end

      it 'resets the last explored commit in each repo' do
        @project.update_attributes(repos_last_sha: @last_sha)
        @project.restart_repos_sha
        @project.repos_last_sha.should eq([nil,nil])
      end

      it 'updates data about last explored commit in each repo' do
        @project.search_commits
        @last_sha.each_with_index do |sha, index|
          sha.should eq(@project.repos_last_sha[index])
        end
      end

      it 'gets commits since the last explored commit in each repo' do
        @last_sha = ["adc0bcd4f85ae77e1015b2b8fa1f2f5df191a5aa","e56b810cef5d7e3ee79bc437cd060e1f70c852bf"]
        @project.update_attributes(repos_last_sha: @last_sha)
        commits = @project.search_commits
        commits.length.should eq(4)
      end

      describe 'When last explored commit is invalid' do
        before :each do
          @project.prepare_commits_data
          @last_sha[0] = "e0058c534aa9c0260c20102b9b16ed02e5ddb080"
          @project.update_attributes(repos_last_sha: @last_sha)
        end

        it 'does not raise an error' do
          expect { @project.search_commits }.to_not raise_error
        end

        it 'resets commits report' do
          @project.search_commits
          expect(@project.commits_data.count).to eq(0)
        end

        it 'returns all commits' do
          @project.search_commits.length.should eq(225)
        end
      end
    end

    it 'updates commits data for projects with git repos' do
      project = FactoryGirl.create(:project, git_repos: "dummy.git")
      Project.any_instance.should_receive(:update_project_commits_data)
      Project.update_all_projects_commits_data
      project.update_attributes(git_repos: "")
      Project.any_instance.should_not_receive(:update_project_commits_data)
      Project.update_all_projects_commits_data
    end

    it 'updates affected projects data' do
      project = FactoryGirl.create(:project, git_repos: "aki.git\ndummy.git\ncalligra.git")
      repo = "dummy.git"
      Project.any_instance.should_receive(:update_project_commits_data)
      Project.update_commits_data repo
    end

    it 'does not update unaffected projects data' do
      project = FactoryGirl.create(:project, git_repos: "aki.git\ncalligra.git")
      repo = "dummy.git"
      Project.any_instance.should_not_receive(:update_project_commits_data)
      Project.update_commits_data repo
    end

    it 'adds new commits data when the author has no commits today for this project' do
      repo = "sso-kde-org.git"
      author = "Sayak Banerjee"
      date = Date.new(2014,6,25)
      project = FactoryGirl.create(:project, git_repos: repo)
      project.add_new_commits_data author, date
      commits_data = project.commits_data.first
      commits_data.contributor.should eq(author)
      commits_data.day.should eq(date)
      commits_data.number.should eq(1)
    end

    it 'edits old commits data when the author has done a previous commit today for this project' do
      repo = "sso-kde-org.git"
      author = "Sayak Banerjee"
      date = Date.new(2014,6,25)
      number = rand(15)
      project = FactoryGirl.create(:project, git_repos: repo)
      project.commits_data << CommitsData.new({contributor: author, day: date, number: number})
      project.add_new_commits_data author, date
      commits_data = project.commits_data.where({contributor: author, day: date}).first
      commits_data.number.should eq(number+1)
    end

    it 'lists a products branches' do
      branches = ["master","new-branch-test"]
      Project.list_branches("dummy.git").should eq(branches)
    end

  end

  describe 'Commits report' do

      it 'determines start date for commits' do
        projectA = FactoryGirl.create(:empty_project)
        projectA.commits_start_date = Date.today
        projectA.save
        projectB = FactoryGirl.create(:empty_project)
        projectB.commits_start_date = Date.today - 10.days
        projectB.save
        Project.start_date_for_all("commits").should eq(Date.today - 10.days)
      end

      it_should_behave_like "generated_pie_chart","commits","test1.git"

      it_should_behave_like "generated_line_graph","commits","test2.git"

      it_should_behave_like "prepared_report_files", "commits"

      it_should_behave_like "generated_all_kde_report", "commits"

    end

  describe 'Mailing lists part' do

    before(:each) do
      Project.stub(:all_messages) do |mlist|
        test_all_messages(mlist)
      end
      @mailing_lists = ["test1.mbox","test2.mbox"]
      @project = FactoryGirl.create(:project, mailing_lists: @mailing_lists.join("\n"))
    end

    it 'prepares mailing messages data for a project with multiple mailing lists' do
      @mailing_lists.each do |mlist|
        MailingListsData.should_receive(:split_mbox_file).with(mlist)
      end
      @project.should_receive(:prepare_report_files).with("mailing_messages")
      FactoryGirl.create(:kde_community_project)
      @project.prepare_mailing_messages_data
      real_mailing_messages_data = test_real_mailing_messages_data(@mailing_lists)
      @project.mailing_messages_data.count.should eq(real_mailing_messages_data.count)
      real_mailing_messages_data.each do |real_data_entry|
        MailingMessagesData.where(contributor: real_data_entry["contributor"], day: real_data_entry["day"], number: real_data_entry["number"], project_id: @project.id).count.should eq(1)
      end
    end

    it 'updates affected projects data' do
      mlist = "test2.mbox"
      max_num = 1 # Edit this when a new message is added to the test suite.
      (1..max_num).each do |num|
        email = test_email num
        data = test_email_data num
        date = Date.new(data["year"].to_i,data["month"].to_i,data["day"].to_i)
        MailingListsData.should_receive(:increment_number_of_messages).with(mlist)
        MailingListsData.should_receive(:add_message_to_mbox).with(mlist,data["month_folder"],email)
        Project.any_instance.should_receive(:add_new_mailing_message_data).with(data["sender"],date)
        Project.any_instance.should_receive(:prepare_report_files).with("mailing_messages")
        Project.update_mailing_list mlist, email
      end
    end

    it 'does not update unaffected projects data' do
      mlist = "test3.mbox" # It's not included in the project's mailing lists.
      email = test_email 1
      data = test_email_data 1
      project = FactoryGirl.create(:project, git_repos: "aki.git\ncalligra.git")
      Project.any_instance.should_not_receive(:add_new_mailing_message_data)
      MailingListsData.should_receive(:increment_number_of_messages).with(mlist)
      MailingListsData.should_receive(:add_message_to_mbox).with(mlist,data["month_folder"],email)
      Project.update_mailing_list mlist, email
    end

    it 'adds a new entry in mailing messages data when contributor has sent no messages this day in this project' do
      contributor = "tester"
      day = Date.today
      @project.mailing_messages_data.where(contributor: contributor, day: day).count.should eq(0)
      @project.add_new_mailing_message_data contributor, day
      @project.mailing_messages_data.find_by(contributor: contributor, day: day).number.should eq(1)
    end

    it 'edits old entry in mailing messages data when contributor has already send a message this day in this project' do
      contributor = "tester"
      day = Date.today
      number = 3
      @project.mailing_messages_data << MailingMessagesData.new(contributor: contributor, day: day, number: number)
      @project.save
      @project.add_new_mailing_message_data contributor, day
      @project.mailing_messages_data.find_by(contributor: contributor, day: day).number.should eq(number+1)
    end

  end

  describe 'Mailing Lists report' do

    it_should_behave_like "generated_pie_chart","mailing_messages","test3.mbox"

    it_should_behave_like "generated_line_graph","mailing_messages","test3.mbox"

    it_should_behave_like "prepared_report_files", "mailing_messages"

    it_should_behave_like "generated_all_kde_report", "mailing_messages"

    it_should_behave_like "generated_average_reply_line", "mailing_messages"

  end

  describe 'Irc channels part' do

    before(:each) do
      @project = FactoryGirl.create(:project, irc_channels: '#test1\n#test2')
    end

    it 'prepares irc messages data for a project' do
      @project.irc_messages_start_date.should eq(nil)
      @project.should_receive(:prepare_report_files).with("irc_messages")
      @project.prepare_irc_messages_data
      @project.irc_messages_start_date.should eq(Date.today)
      @project.number_of_irc_messages.should eq(0)
    end

    describe 'Updates' do

      before :each do
        @sender = "tester1"
        @message = "a message"
        @time = Time.now
      end

      it 'updates affected projects data' do
        channel = "#test1"
        Project.any_instance.should_receive(:prepare_report_files).with("irc_messages")
        Project.irc_message_update channel, @sender, @message, @time
      end

      it 'does not update unaffected projects data' do
        channel = "#test3" # This is not included in the project's IRC Channels.
        Project.any_instance.should_not_receive(:add_new_irc_message_data)
        Project.irc_message_update channel, @sender, @message, @time
      end
    end

    it 'adds new entry in irc messages data when the contributor hasn sent no messages this day in this project' do
      contributor = "tester"
      day = Date.today
      @project.irc_messages_data.where(contributor: contributor, day: day).count.should eq(0)
      @project.add_new_irc_message_data contributor
      @project.irc_messages_data.find_by(contributor: contributor, day: day).number.should eq(1)
    end

    it 'edits old entry in irc messages data when the contributor has send messages this day in this project' do
      contributor = "tester"
      day = Date.today
      number = 5
      @project.irc_messages_data << IrcMessagesData.new(contributor: contributor, day: day, number: number)
      @project.save
      @project.add_new_irc_message_data contributor
      @project.irc_messages_data.find_by(contributor: contributor, day: day).number.should eq(number+1)
    end

  end

  describe 'Irc Channels report' do

    it_should_behave_like "generated_pie_chart","irc_messages","test1"

    it_should_behave_like "generated_line_graph","irc_messages","test2"

    it_should_behave_like "prepared_report_files", "irc_messages"

    it_should_behave_like "generated_all_kde_report", "irc_messages"

    it_should_behave_like "generated_average_reply_line", "irc_messages"

  end

  describe 'Facebook part' do

    before(:each) do
      @project = FactoryGirl.create(:project, facebook_pages: "test1")
    end

    it 'prepares data for a project' do
      stub_facebook
      @project.facebook_posts_start_date.should eq(nil)
      @project.prepare_facebook_posts_data
      real_doers_data = test_real_facebook_messages_data("doers",1)
      real_posts_data = test_real_facebook_messages_data("posts",1)
      @project.facebook_posts_data.count.should eq(real_posts_data.count)
      @project.facebook_doers_data.count.should eq(real_doers_data.count)
      real_posts_data.each do |real_data_entry|
        prepared_data = real_data_entry
        FacebookPostsData.where(prepared_data).count.should eq(1)
      end
      real_doers_data.each do |real_data_entry|
        prepared_data = real_data_entry.merge(project_id: @project.id)
        FacebookDoersData.where(prepared_data).count.should eq(1)
      end
      real_date = Date.new(2014,7,18)
      @project.facebook_posts_start_date.should eq(real_date)
    end

    describe 'Facebook posts report' do

      before(:each) do
        new_date = Date.new(2014,7,25)
        Timecop.travel(new_date)
        posts_data = test_real_facebook_messages_data "posts",1
        posts_data.map! {|item| FacebookPostsData.new(item)}
        doers_data = test_real_facebook_messages_data "doers",1
        doers_data.map! {|item| FacebookDoersData.new(item)}
        start_date = Date.new(2014,7,18)
        @project.facebook_posts_start_date = start_date
        @project.facebook_posts_data = posts_data
        @project.facebook_doers_data = doers_data
        @project.save
      end

      after(:each) do
        Timecop.return
      end

      it 'prepares posts line charts' do
        res = @project.get_multiple_objects_line_charts "facebook_posts-objects"
        real_data = test_real_facebook_line_graphs("posts_lines")
        real_data.each do |k,v|
          res[k].should eq(v)
        end
      end

      it 'prepares comments line chart' do
        res = @project.get_objects_line_chart "facebook_posts", false, "comments", @project.facebook_posts_start_date
        real_data = test_real_facebook_line_graphs("comments_line")
        res.should eq(real_data)
      end

      describe 'Pie charts' do

        context 'commenters' do

          it 'prepares total' do
            res = @project.get_doers_chart("facebook_posts",nil,false,"doer","comments")
            real_data = test_real_facebook_pie_charts("total_commenters")
            real_data.should eq(res)
          end

          it 'prepares today' do
            @project.facebook_doers_data << FacebookDoersData.new(day: Date.today, doer: "tester1", comments: 4)
            @project.facebook_doers_data << FacebookDoersData.new(day: Date.today, doer: "tester3", comments: 1)
            res = @project.get_doers_chart("facebook_posts",Date.today,false,"doer","comments")
            real_data = [["tester3",1],["tester1",4]]
            res.should eq(real_data)
          end

        end

        context 'likers' do

          it 'prepares total' do
            res = @project.get_doers_chart("facebook_posts",nil,false,"doer","likes")
            real_data = test_real_facebook_pie_charts("total_likers")
            res.should eq(real_data)
          end

          it 'prepares today' do
            @project.facebook_doers_data << FacebookDoersData.new(day: Date.today, doer: "tester1", likes: 4)
            @project.facebook_doers_data << FacebookDoersData.new(day: Date.today, doer: "tester3", likes: 1)
            res = @project.get_doers_chart("facebook_posts",Date.today,false,"doer","likes")
            real_data = [["tester3",1],["tester1",4]]
            res.should eq(real_data)
          end

        end

      end

    end

    describe 'Facebook doers report' do

      after(:each) {
        Timecop.return
      }

      it 'prepares daily doers lines chart' do
        start_date = Date.new(2014,7,18)
        @project.facebook_posts_start_date = start_date
        doers_data = test_real_facebook_messages_data "doers",1
        doers_data.map! {|item| FacebookDoersData.new(item)}
        @project.facebook_doers_data = doers_data
        @project.save
        new_date = Date.new(2014,7,22)
        Timecop.travel(new_date)
        #res = @project.get_daily_doers_multiple_lines_chart "facebook_posts"
        res = @project.get_doers_lines "facebook_posts", "daily"
        real_data = test_real_facebook_line_graphs("daily_doers")
        real_data.each do |k,v|
          res[k].should eq(v)
        end
      end

      it 'prepares daily doers lines chart when no likes nor comments were made in first day' do
        start_date = Date.new(2014,7,16)
        @project.facebook_posts_start_date = start_date
        doers_data = test_real_facebook_messages_data "doers",1
        doers_data.map! {|item| FacebookDoersData.new(item)}
        @project.facebook_doers_data = doers_data
        @project.save
        new_date = Date.new(2014,7,25)
        Timecop.travel(new_date)
        #res = @project.get_daily_doers_multiple_lines_chart "facebook_posts"
        res = @project.get_doers_lines "facebook_posts", "daily"
        real_data = test_real_facebook_line_graphs("daily_doers-no-actions-first-day")
        real_data.each do |k,v|
          res[k].should eq(v)
        end
      end

      it 'prepares monthly doers lines chart' do
        start_date = Date.new(2013,11,15)
        @project.facebook_posts_start_date = start_date
        doers_data = test_real_facebook_messages_data "doers",2
        doers_data.map! {|item| FacebookDoersData.new(item)}
        @project.facebook_doers_data = doers_data
        @project.save
        new_date = Date.new(2014,3,25)
        Timecop.travel(new_date)
        #res = @project.get_monthly_doers_multiple_lines_chart "facebook_posts"
        res = @project.get_doers_lines "facebook_posts", "monthly"
        real_data = test_real_facebook_line_graphs("monthly_doers")
        real_data.each do |k,v|
          res[k].should eq(v)
        end
      end

      it 'prepares monthly doers lines chart when no likes nor comments were made in first month' do
        start_date = Date.new(2013,10,15)
        @project.facebook_posts_start_date = start_date
        doers_data = test_real_facebook_messages_data "doers",2
        doers_data.map! {|item| FacebookDoersData.new(item)}
        @project.facebook_doers_data = doers_data
        @project.save
        new_date = Date.new(2014,2,25)
        Timecop.travel(new_date)
        #res = @project.get_monthly_doers_multiple_lines_chart "facebook_posts"
        res = @project.get_doers_lines "facebook_posts", "monthly"
        real_data = test_real_facebook_line_graphs("monthly_doers-no-actions-first-month")
        real_data.each do |k,v|
          res[k].should eq(v)
        end
      end

      it 'prepares yearly doers lines chart' do
        start_date = Date.new(2013,5,15)
        @project.facebook_posts_start_date = start_date
        doers_data = test_real_facebook_messages_data "doers",3
        doers_data.map! {|item| FacebookDoersData.new(item)}
        @project.facebook_doers_data = doers_data
        @project.save
        new_date = Date.new(2016,3,25)
        Timecop.travel(new_date)
        #res = @project.get_yearly_doers_multiple_lines_chart "facebook_posts"
        res = @project.get_doers_lines "facebook_posts", "yearly"
        real_data = test_real_facebook_line_graphs("yearly_doers")
        real_data.each do |k,v|
          res[k].should eq(v)
        end
      end

      it 'prepares yearly doers lines chart when no likes nor comments were made in first year' do
        start_date = Date.new(2012,10,15)
        @project.facebook_posts_start_date = start_date
        doers_data = test_real_facebook_messages_data "doers",3
        doers_data.map! {|item| FacebookDoersData.new(item)}
        @project.facebook_doers_data = doers_data
        @project.save
        new_date = Date.new(2017,2,25)
        Timecop.travel(new_date)
        res = @project.get_doers_lines "facebook_posts", "yearly"
        real_data = test_real_facebook_line_graphs("yearly_doers-no-actions-first-month")
        real_data.each do |k,v|
          res[k].should eq(v)
        end
      end

    end

    it 'generates all files needed to display the report' do
      start_date = Date.new(2012,12,20)
      @project.facebook_posts_start_date = start_date
      @project.title = "test"
      doers_data = test_real_facebook_messages_data "doers",4
      doers_data.map! {|item| FacebookDoersData.new(item)}
      @project.facebook_doers_data = doers_data
      @project.save
      new_date = Date.new(2014,1,22)
      Timecop.travel(new_date)
      res = test_generated_files_for "facebook_pages"
      FakeFS do
        @project.prepare_facebook_posts_files
        test_correctness_of_generated_files_for "test","facebook_posts", res
      end
      Timecop.return
    end

  end

  describe 'Twitter part' do

    before :each do
      @project = FactoryGirl.create(:project)
    end

    it 'prepares Twitter profile id' do
      stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=kdecommunity").to_return(:status => 200, :body => twitter_user_response("kdecommunity"), :headers => {})
      real = 16173592
      @project.prepare_twitter_profile_id
      @project.twitter_profile_id.should eq(real)
    end

    it 'prepares Twitter tweets data' do
      stub_request(:get, "https://api.twitter.com/1.1/statuses/user_timeline.json?count=200&screen_name=kdecommunity").to_return(:status => 200, :body => twitter_user_timeline_response(1), :headers => {})
      stub_request(:get, "https://api.twitter.com/1.1/statuses/user_timeline.json?count=200&max_id=489805540923817983&screen_name=kdecommunity").to_return(:status => 200,:body => twitter_user_timeline_response(2) , :headers => {})
      stub_request(:get, "https://api.twitter.com/1.1/statuses/user_timeline.json?count=200&max_id=489331963078410239&screen_name=kdecommunity").to_return(:status => 200,:body => "[]" , :headers => {})
      @project.prepare_tweets_data
      real_data = test_real_tweets_data(1)
      real_date = Date.new(2014,7,16)
      @project.twitter_tweets_start_date.should eq(real_date)
      real_data.each do |item|
        item[:project_id] = @project.id
        TwitterTweetsData.where(item).count.should eq(1)
      end
    end

    it 'prepares Twitter doers data' do
      stub_request(:get, "https://api.twitter.com/1.1/statuses/retweets/492401096900767744.json").to_return(:status => 200, :body => twitter_retweets_response(1), :headers => {})
      stub_request(:get, "https://api.twitter.com/1.1/statuses/retweets/489805540923817984.json").to_return(:status => 200, :body => twitter_retweets_response(2), :headers => {})
      stub_request(:get, "https://api.twitter.com/1.1/statuses/retweets/489331963078410240.json").to_return(:status => 200, :body => twitter_retweets_response(3), :headers => {})
      tweets_data = test_real_tweets_data(1)
      tweets_data.each do |item|
        @project.twitter_tweets_data << TwitterTweetsData.new(item)
      end
      @project.prepare_retweeters_data
      real_data = test_real_retweeters_data(1)
      real_data.each do |item|
        item[:project_id] = @project.id
        TwitterDoersData.where(item).count.should eq(1)
      end
    end

    it 'prepares Twitter data' do
      @project.should_receive(:prepare_twitter_profile_id)
      @project.should_receive(:prepare_tweets_data)
      @project.should_receive(:prepare_retweeters_data)
      @project.prepare_twitter_data
    end

    describe 'Report' do

      after :each do
        Timecop.return
      end

      context 'Objects' do

        it 'prepares tweets line graphs' do
          tweets_data = test_real_tweets_data(1)
          tweets_data.each do |item|
            @project.twitter_tweets_data << TwitterTweetsData.new(item)
          end
          @project.twitter_tweets_data << TwitterTweetsData.new({day: Date.new(2014,7,17), favorite_count: 5, retweet_count: 2, tweet_id: 1})
          new_date = Date.new(2014,7,26)
          Timecop.travel(new_date)
          res = @project.get_multiple_objects_line_charts "twitter_tweets-tweets"
          real_graph = test_real_twitter_graph("objects")
          real_graph.keys.length.should eq(res.keys.length)
          real_graph.each do |k,v|
            res[k].should eq(v)
          end
        end

      end

      context 'Doers' do

        before :each do
          tweeters_data = test_real_retweeters_data(2)
          tweeters_data.each do |item|
            @project.twitter_doers_data << TwitterDoersData.new(item)
          end
          start_date = Date.new(2013,12,20)
          @project.twitter_tweets_start_date = start_date
          @project.save
          new_date = Date.new(2014,2,1)
          Timecop.travel(new_date)
        end

        context 'Line graphs' do

          it 'prepares retweets' do
            res = @project.get_objects_line_chart "twitter_tweets", false, "retweets", @project.twitter_tweets_start_date
            real_graph = test_real_twitter_graph("retweets")
            res.should eq(real_graph)
          end

          it 'prepares daily retweeters' do
            res = @project.get_doers_lines "twitter_tweets","daily"
            real_graph = test_real_twitter_graph("doers_daily")
            res.should eq(real_graph)
          end

          it 'prepares monthly retweeters' do
            res = @project.get_doers_lines "twitter_tweets","monthly"
            real_graph = test_real_twitter_graph("doers_monthly")
            res.should eq(real_graph)
          end

          it 'prepares yearly retweeters' do
            res = @project.get_doers_lines "twitter_tweets","yearly"
            real_graph = test_real_twitter_graph("doers_yearly")
            res.should eq(real_graph)
          end

        end

        context 'Pie charts' do

          it 'prepares total retweeters' do
            res = @project.get_twitter_doers_chart "total"
            real = test_real_twitter_pie_chart("total")
            res.should eq(real)
          end

          it 'prepares this year retweeters' do
            res = @project.get_twitter_doers_chart "year"
            real = test_real_twitter_pie_chart("year")
            res.should eq(real)
          end

          it 'prepares today retweeters' do
            res = @project.get_twitter_doers_chart "today"
            real = test_real_twitter_pie_chart("today")
            res.should eq(real)
          end

        end

      end

    end

    it 'prepares files' do
      files = Array.new
      (0...6).each do |num|
        files << ('a'..'z').to_a.shuffle[0,8].join
      end
      @project.title = "test"
      @project.twitter_tweets_start_date = Date.new(2014,7,27)
      @project.save
      @project.should_receive(:get_multiple_objects_line_charts).with("twitter_tweets-tweets").and_return(files[0])
      @project.should_receive(:get_multiple_objects_line_charts).with("twitter_tweets-retweets",false,@project.twitter_tweets_start_date).and_return(files[1])
      @project.should_receive(:get_objects_line_chart).with("twitter_tweets", false, "retweets", @project.twitter_tweets_start_date).and_return(files[2])
      @project.should_receive(:get_doers_lines).with("twitter_tweets", "daily").and_return(files[3])
      @project.should_receive(:get_doers_lines).with("twitter_tweets", "monthly").and_return(files[4])
      @project.should_receive(:get_doers_lines).with("twitter_tweets", "yearly").and_return(files[5])
      @project.should_receive(:prepare_pie_charts).with("twitter_tweets",false,"get_twitter_doers_chart")
      FakeFS do
        @project.prepare_twitter_files
        path = "public/en/projects/#{@project.path}/twitter_tweets"
        charts = ["tweets","retweets-page","retweets-users","doers_daily","doers_monthly","doers_yearly"]
        (0...6).each do |num|
          File.open("#{path}/#{charts[num]}.json").read.should eq(files[num].to_json)
        end
      end
    end

  end

  describe 'Google Plus posts part' do

    before :each do
      @project = FactoryGirl.create(:project)
    end

    it 'prepares data' do
      stub_request(:get, "https://www.googleapis.com/plus/v1/people/105126786256705328374/activities/public?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("people",1), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z13xwljrzqmex5wt323yihuzmqbasbd1m/comments?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("comments",1), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z13xwljrzqmex5wt323yihuzmqbasbd1m/people/plusoners?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("plusoners",0), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z13xwljrzqmex5wt323yihuzmqbasbd1m/people/resharers?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("resharers",1), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z13xwljrzqmex5wt323yihuzmqbasbd1m/people/resharers?key=#{Tokens.google_plus_token}&maxResults=100&pageToken=CAEQ8vyF7Pso&pp=0").to_return(:status => 200, :body => gplus_response("resharers",2), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z12punhiazrqgflgg04cjttrfyeswv0x4ac/comments?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("comments",0), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z12punhiazrqgflgg04cjttrfyeswv0x4ac/people/plusoners?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("plusoners",1), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z12punhiazrqgflgg04cjttrfyeswv0x4ac/people/plusoners?key=#{Tokens.google_plus_token}&maxResults=100&pageToken=KmgKOvf5mrm7_pd_aZielp7Fz8_Pz8_PzczHm8jGm8uby8XMyMXMyN7Lz8jPyM3Ly8bGzMnMz8fOy8_M__4QASGm_otWHi7TTDEFPoDs8IeYBjmWgGgBREZlBlAAWgsJK8RQNyChcZQQATINCgsKACi38I-zzYLAAg&pp=0").to_return(:status => 200, :body => gplus_response("plusoners",2), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z12punhiazrqgflgg04cjttrfyeswv0x4ac/people/resharers?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("resharers",0), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/people/105126786256705328374/activities/public?key=#{Tokens.google_plus_token}&maxResults=100&pageToken=EgkI-On_0s_qvwIo-Kqbv4KBwAIwmtq_976HwAI4AkAC&pp=0").to_return(:status => 200, :body => gplus_response("people",2), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z132z3qbvob0jtxmk23yihuzmqbasbd1m/comments?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("comments",2), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z132z3qbvob0jtxmk23yihuzmqbasbd1m/comments?key=#{Tokens.google_plus_token}&maxResults=100&pageToken=CAEQmb-R7fso&pp=0").to_return(:status => 200, :body => gplus_response("comments",3), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z132z3qbvob0jtxmk23yihuzmqbasbd1m/people/plusoners?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("plusoners",0), :headers => {})
      stub_request(:get, "https://www.googleapis.com/plus/v1/activities/z132z3qbvob0jtxmk23yihuzmqbasbd1m/people/resharers?key=#{Tokens.google_plus_token}&maxResults=100&pp=0").to_return(:status => 200, :body => gplus_response("resharers",0), :headers => {})
      @project.prepare_google_plus_data
      start_date = Date.new(2014,7,29)
      @project.google_plus_posts_start_date.should eq(start_date)
      posts = @project.google_plus_posts_data
      real_posts = gplus_data("posts",1)
      real_posts.each do |entry|
        posts.where(entry).count.should eq(1)
      end
      doers = @project.google_plus_doers_data
      real_doers = gplus_data("doers",1)
      real_doers.each do |entry|
        doers.where(entry).count.should eq(1)
      end
    end

    describe 'Report' do

      after :each do
        Timecop.return
      end

      context "objects" do

        it 'prepares objects lines' do
          new_date = Date.new(2014,8,7)
          Timecop.travel(new_date)
          data = gplus_data("posts",2).map { |entry| GooglePlusPostsData.new(entry) }
          @project.google_plus_posts_data = data
          @project.save
          res = @project.get_multiple_objects_line_charts "google_plus_posts-objects"
          real = test_gplus_graph("line", "objects")
          real.each do |key,value|
            res[key].should eq(value)
          end
        end

        it 'prepares replies line' do
          new_date = Date.new(2014,2,1)
          Timecop.travel(new_date)
          data = gplus_data("doers",2).map { |entry| GooglePlusDoersData.new(entry) }
          @project.google_plus_doers_data = data
          start_date = Date.new(2013,12,22)
          @project.google_plus_posts_start_date = start_date
          @project.save
          res = @project.get_objects_line_chart "google_plus_posts", false, "replies", @project.google_plus_posts_start_date
          real = test_gplus_graph("line", "replies")
          res.should eq(real)
        end

      end

      context "doers" do

        before :each do
          data = gplus_data("doers",2).map { |entry| GooglePlusDoersData.new(entry) }
          @project.google_plus_doers_data = data
          start_date = Date.new(2013,12,21)
          @project.google_plus_posts_start_date = start_date
          @project.save
          @project.save
          new_date = Date.new(2014,2,1)
          Timecop.travel(new_date)
        end

        it 'prepares doers daily lines' do
          res = @project.get_doers_lines "google_plus_posts","daily"
          real = test_gplus_graph("line","doers-daily")
          real.each do |key,value|
            res[key].should eq(value)
          end
        end

        it 'prepares doers monthly lines' do
          res = @project.get_doers_lines "google_plus_posts","monthly"
          real = test_gplus_graph("line","doers-monthly")
          real.each do |key,value|
            res[key].should eq(value)
          end
        end

        it 'prepares doers yearly lines' do
          res = @project.get_doers_lines "google_plus_posts","yearly"
          real = test_gplus_graph("line","doers-yearly")
          real.each do |key,value|
            res[key].should eq(value)
          end
        end

        it 'prepares total pie chart for replies' do
          res = @project.get_google_plus_posts_doers_chart "total", "replies"
          real = test_gplus_graph("pie","total-replies")
          res.should eq(real)
        end

        it 'prepares total pie chart for reshares' do
          res = @project.get_google_plus_posts_doers_chart "total", "reshares"
          real = test_gplus_graph("pie","total-reshares")
          res.should eq(real)
        end

        it 'prepares total pie chart for plusones' do
          res = @project.get_google_plus_posts_doers_chart "total", "plusones"
          real = test_gplus_graph("pie","total-plusones")
          res.should eq(real)
        end

        it 'prepares this week pie chart for replies' do
          res = @project.get_google_plus_posts_doers_chart "week", "replies"
          real = test_gplus_graph("pie","week-replies")
          res.should eq(real)
        end

      end

    end

  end

  describe 'Forum posts part' do

    before :each do
      @project = FactoryGirl.create(:project)
    end
    
    it 'gets the products of forums' do
      fids = Array.new
      5.times.each do
        fids << rand(500).to_s
      end
      @project.forum_ids = fids.join("\n")
      @project.save
      expect(@project.get_products_of("forum_posts")).to eq(fids)
    end

    it 'gets the number of topics' do
      @project.forum_ids = "154\n121"
      data = test_forums_data(2)
      data.map { |entry| ForumPostsData.create(entry) }
      @project.number_of_forums_topics.should eq(5)
      ForumPostsData.create(contributor: "tester6",fid: 154, day: Date.new(2013,5,21), posts: 2, resolved: 1, threads: 2)
      @project.number_of_forums_topics.should eq(7)
      ForumPostsData.create(contributor: "tester6",fid: 155, day: Date.new(2013,5,21), posts: 2, resolved: 1, threads: 2)
      @project.number_of_forums_topics.should eq(7)
    end

    it 'shows latest 10 topics' do
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=10&feed_style=COMPACT&feed_type=RSS2.0&fid%5B0%5D=154&fid%5B1%5D=121&sd=d&sf=titleonly&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_threads_response("latest"), :headers => {})
      res = @project.forum_topics
      res.count.should eq(10)
      real = test_forum_lists "latest_topics"
      res.zip(real).each do |testing,actual|
        testing.category.content.should eq(actual["category"])
        testing.title.should eq(actual["title"])
        testing.pubDate.should eq(actual["pubDate"])
        testing.link.should eq(actual["link"])
        testing.author.should eq(actual["author"])
      end
    end

    it 'gets forums ids for link' do
      real = "&fid[]=154&fid[]=121"
      res = @project.get_forum_ids_for_link
      res.should eq(real)
    end

    it 'gets array of forum ids' do
      real = ["154","121"]
      res = @project.forum_ids_array
      res.count.should eq(res.count)
      real.each do |item|
        res.include?(item).should eq(true)
      end
    end

    it 'prepares forum posts data' do
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=0&submit=Search&sv=1").to_return(:status => 200, :body => test_forums_resolved_response(1), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=100&submit=Search&sv=1").to_return(:status => 200, :body => test_forums_resolved_response(2), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=200&submit=Search&sv=1").to_return(:status => 200, :body => test_forums_resolved_response(3), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(1), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=100&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(2), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=200&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(3), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_posts_response(1), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=100&submit=Search").to_return(:status => 200, :body => test_forums_posts_response(2), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=200&submit=Search").to_return(:status => 200, :body => test_forums_posts_response(3), :headers => {})
      last_time = Time.utc(2014,2,17,18,21,45)
      GeneralObjects.should_receive(:set_last_updated).with("forum",last_time)
      Project.prepare_forum_posts_data
      real = test_forums_data(1)
      real.each do |item|
        ForumPostsData.where(item).count.should eq(1)
      end
    end

    it 'updates forum posts data' do
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=0&submit=Search&sv=1").to_return(:status => 200, :body => test_forums_resolved_response(4), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(4), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_posts_response(4), :headers => {})
      data = test_forums_data(1)
      data.map { |entry| ForumPostsData.create(entry) }
      FactoryGirl.create(:empty_project, forum_ids: "124\n24")
      FactoryGirl.create(:kde_community_project)
      normal = 0
      all = 0
      allow_any_instance_of(Project).to receive(:prepare_forum_posts_files) do |obj,arg|
        if arg
          all+= 1
        else
          normal+= 1
        end
      end
      Project.update_forum_posts_data Date.new(2014,2,17)
      real = test_forums_data(2)
      real.each do |item|
        ForumPostsData.where(item).count.should eq(1)
      end
      normal.should eq(1)
      all.should eq(1)
    end

    describe "Report" do

      before :each do
        data = test_forums_data(1)
        data.map { |entry| ForumPostsData.create(entry) }
      end

      after :each do
        Timecop.return
      end

      it 'gets forum posts data for a project' do
        data = @project.forum_posts_data
        fids = [121,154]
        real = test_forums_data(1)
        cnt = 0
        real.each do |entry|
          if fids.include?(entry["fid"])
            data.where(entry).count.should eq(1)
            cnt+=1
          end
        end
        data.count.should eq(cnt)
      end

      context 'Line graphs' do

        before :each do
          new_date = Date.new(2014,2,25)
          Timecop.travel(new_date)
        end

        it 'prepares forum posts start data' do
          real_start_date = Date.new(2014,2,16)
          @project.prepare_forum_posts_start_date
          @project.forum_posts_start_date.should eq(real_start_date)
        end

        it 'prepares objects line graphs for a project' do
          res = @project.get_multiple_objects_line_charts "forum_posts",false
          real = test_real_forums_graph "objects"
          real.each do |key,value|
            res[key].should eq(value)
          end
        end

        it 'prepares objects line graphs for KDE Community' do
          @project.title = "KDE Community"
          @project.forum_ids = ""
          @project.save
          res = @project.get_multiple_objects_line_charts "forum_posts",true
          real = test_real_forums_graph "objects-all"
          real.each do |key,value|
            res[key].should eq(value)
          end
        end

        it 'prepares daily doers line graphs for a project' do
          res = @project.get_daily_doers_line_chart "forum_posts"
          real = test_real_forums_graph "doers_daily"
          res.should eq(res)
        end

        it 'prepares daily doers line graphs for KDE Community' do
          res = @project.get_daily_doers_line_chart "forum_posts", true
          real = test_real_forums_graph "doers_daily-all"
          res.should eq(res)
        end

        it 'prepares monthly doers line graphs for a project' do
          ForumPostsData.create(day: Date.new(2013,12,15), contributor: "Tester1", posts: 3, threads: 3, resolved: 1,fid: 121)
          res = @project.get_monthly_doers_line_chart "forum_posts"
          real = test_real_forums_graph "doers_monthly"
          res.should eq(res)
        end

        it 'prepares monthly doers line graphs for KDE Community' do
          ForumPostsData.create(day: Date.new(2013,12,15), contributor: "Tester1", posts: 3, threads: 3, resolved: 1,fid: 229)
          res = @project.get_monthly_doers_line_chart "forum_posts", true
          real = test_real_forums_graph "doers_monthly-all"
          res.should eq(res)
        end

        it 'prepares yearly doers line graphs for a project' do
          ForumPostsData.create(day: Date.new(2011,1,15), contributor: "Tester1", posts: 3, threads: 3, resolved: 1,fid: 121)
          res = @project.get_yearly_doers_line_chart "forum_posts"
          real = test_real_forums_graph "doers_yearly"
          res.should eq(res)
        end

        it 'prepares yearly doers line graphs for KDE Community' do
          ForumPostsData.create(day: Date.new(2011,1,15), contributor: "Tester1", posts: 3, threads: 3, resolved: 1,fid: 229)
          res = @project.get_yearly_doers_line_chart "forum_posts", true
          real = test_real_forums_graph "doers_yearly"
          res.should eq(res)
        end

        it_should_behave_like "generated_average_reply_line", "forum_posts"

      end

      context 'Pie charts' do

        it 'prepares today posters chart for a project' do
          new_date = Date.new(2014,2,17)
          Timecop.travel(new_date)
          res = @project.get_forum_doers_chart "today"
          real = test_real_forums_chart "today"
          res.should eq(real)
        end

        it 'prepares total posters chart for a project' do
          ForumPostsData.create(day: Date.new(2011,1,15), contributor: "Tester1", posts: 5, threads: 3, resolved: 1,fid: 121)
          ForumPostsData.create(day: Date.new(2011,1,15), contributor: "Tester3", posts: 7, threads: 3, resolved: 1,fid: 121)
          res = @project.get_forum_doers_chart "total"
          real = test_real_forums_chart "total"
          res.should eq(real)
        end

        it 'prepares total posters chart for KDE Community' do
          ForumPostsData.create(day: Date.new(2011,1,15), contributor: "Tester1", posts: 5, threads: 0, resolved: 0,fid: 131)
          ForumPostsData.create(day: Date.new(2011,1,15), contributor: "Tester3", posts: 7, threads: 0, resolved: 0,fid: 121)
          ForumPostsData.create(day: Date.new(2011,1,15), contributor: "Tester4", posts: 1, threads: 0, resolved: 0,fid: 105)
          res = @project.get_forum_doers_chart "total",true
          real = test_real_forums_chart "total-all"
          res.should eq(real)
        end

      end

    end

  end

  describe 'Planetkde' do

    it 'updates data' do
      stub_request(:get, "http://planetkde.org/rss20.xml").to_return(:status => 200, :body => test_planet_response(1))
      Project.should_receive(:prepare_all_kde_files).with("planet_posts")
      Project.update_planet_report Date.new(2013,1,1)
      real = test_planet_data(1)
      real.each do |entry|
        PlanetPostsData.where(entry).count.should eq(1)
      end
    end

    it 'does not change anything when there is no new data' do
      stub_request(:get, "http://planetkde.org/rss20.xml").to_return(:status => 200, :body => test_planet_response(1))
      data = test_planet_data(1)
      data.map { |entry| PlanetPostsData.create(entry) }
      Project.should_receive(:prepare_all_kde_files).with("planet_posts")
      Project.update_planet_report Date.new(2015,1,1)
      PlanetPostsData.count.should eq(data.count)
    end

  end

  describe 'Dot.kde.org' do

    it 'prepares data' do
      stub_request(:get, "https://dot.kde.org/all-content.json?page=0").to_return(:status => 200, :body => test_dot_response(1), :headers => {})
      stub_request(:get, "https://dot.kde.org/all-content.json?page=1").to_return(:status => 200, :body => test_dot_response(2), :headers => {})
      stub_request(:get, "https://dot.kde.org/all-content.json?page=2").to_return(:status => 200, :body => test_dot_response(3), :headers => {})
      Project.prepare_dot_data
      real = test_dot_data(1)
      real.each do |entry|
        DotPostsData.where(entry).count.should eq(1)
      end
    end

    it 'updates data' do
      stub_request(:get, "https://dot.kde.org/all-content.json?page=0").to_return(:status => 200, :body => test_dot_response(4), :headers => {})
      stub_request(:get, "https://dot.kde.org/all-content.json?page=1").to_return(:status => 200, :body => test_dot_response(1), :headers => {})
      data = test_dot_data(1)
      data.map { |entry| DotPostsData.create(entry) }
      Project.prepare_dot_data Date.new(2014,8,13)
      real = test_dot_data(2)
      real.each do |entry|
        DotPostsData.where(entry).count.should eq(1)
      end
    end

    describe 'Report' do

      before :each do
        @project = FactoryGirl.create(:kde_community_project)
        data = test_dot_data(1)
        data.map { |entry| DotPostsData.create(entry) }
        new_time = Date.new(2014,8,15)
        Timecop.travel(new_time)
      end

      after :each do
        Timecop.return
      end

      it 'prepares objects line graph' do
        res = @project.get_multiple_objects_line_charts "dot_posts"
        real = test_real_dot_graph "objects"
        real.each do |key,value|
          res[key].should eq(value)
        end
      end

      it 'prepares daily posters line graph' do
        res = @project.get_daily_doers_line_chart "dot_posts"
        real = test_real_dot_graph "doers_daily"
        res.should eq(res)
      end

      it 'prepares monthly posters line graph' do
        res = @project.get_monthly_doers_line_chart "dot_posts"
        real = test_real_dot_graph "doers_monthly"
        res.should eq(res)
      end

      it 'prepares yearly posters line graph' do
        res = @project.get_yearly_doers_line_chart "dot_posts"
        real = test_real_dot_graph "doers_yearly"
        res.should eq(res)
      end

      describe 'Pie charts' do

        context 'Contributors' do

          it 'prepares total' do
            res = @project.get_dot_doers_chart "total"
            real = test_real_dot_chart "contributors","total"
            res.should eq(real)
          end

          it 'prepares year' do
            res = @project.get_dot_doers_chart "year"
            real = test_real_dot_chart "contributors","year"
            res.should eq(real)
          end

          it 'prepares month' do
            res = @project.get_dot_doers_chart "month"
            real = test_real_dot_chart "contributors","month"
            res.should eq(real)
          end

          it 'prepares week' do
            res = @project.get_dot_doers_chart "week"
            real = test_real_dot_chart "contributors","week"
            res.should eq(real)
          end

        end

        context 'Categories' do

          it 'prepares total' do
            res = @project.get_dot_categories_chart "total"
            real = test_real_dot_chart "categories","total"
            res.should match_array(real)
          end

          it 'prepares year' do
            res = @project.get_dot_categories_chart "year"
            real = test_real_dot_chart "categories","year"
            res.should match_array(real)
          end

          it 'prepares month' do
            res = @project.get_dot_categories_chart "month"
            real = test_real_dot_chart "categories","month"
            res.should match_array(real)
          end

          it 'prepares week' do
            res = @project.get_dot_categories_chart "week"
            real = test_real_dot_chart "categories","week"
            res.should match_array(real)
          end

        end

        it 'prepares files' do
          res = test_generated_files_for "dot_posts"
          FakeFS do
            @project.prepare_dot_posts_files
            test_correctness_of_generated_files_for "kde-community","dot_posts", res
          end
        end

      end

    end

  end

end