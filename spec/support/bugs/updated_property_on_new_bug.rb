require 'spec_helper'

shared_examples "updated_property_on_new_bug" do |prop,result_prop|
  result_prop = result_prop ||= prop
  project = FactoryGirl.build(:project)
  project.bugs_data = FactoryGirl.build(:test_bugs_data_1)
  project.save
  old_bugs_data = project.bugs_data.dup
  bug_id = 335083
  bug_data = JSON.parse(File.new('spec/factories/bugs/fetch_bug_result/'+bug_id.to_s+'.json').read)
  project.perform_bugs_report_update(bug_data)
  found = false
  old_bugs_data[prop].each do |entry|
    found = true if entry.first == bug_data[result_prop]
  end
  if found
    it 'increments the property of the bug when it is in the required fields and had an entry before' do
      # The old_data should have and entry for classification before which is the case in our bug.
      new_data = project.bugs_data[prop]
      old_data = old_bugs_data[prop]
      new_data.length.should eq(old_data.length)
      old_data.each do |item|
        unless item[0] == bug_data[result_prop]
          new_data.include?(item).should eq(true)
        else
          updated = item.dup
          updated[1]+= 1
          new_data.include?(updated).should eq(true)
        end
      end
    end
  else
    it 'adds a new entry for the property when it is in the required fields and had no entry before' do
      # The old_data should not have and entry for classification before which is the case in our bug.
      new_data = @project.bugs_data[prop]
      old_data = @old_bugs_data[prop]
      new_data.length.should eq(old_data.length + 1)
      old_data.each do |item|
        new_data.include?(item).should eq(true)
      end
      new_item = [@bug_data[prop],1]
      new_data.include?(new_item).should eq(true)
    end
  end
end