shared_examples "updated_line" do |type,change,data|
  project = FactoryGirl.build(:project)
  project.bugs_data = FactoryGirl.build(:test_bugs_data_1)
  project.bugs_data.start_date = Date.new(2014,5,18)
  project.save
  old_bugs_data = project.bugs_data.dup
  bug_id = 335083
  if change == "increment"
    it 'increments last element in the line' do
      if (data ||= nil)
        Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
        project.perform_bugs_report_update_existing data, bug_id
      else
        bug_data = JSON.parse(File.new('spec/factories/bugs/fetch_bug_result/'+bug_id.to_s+'.json').read)
        project.perform_bugs_report_update(bug_data)    
      end
      new_line = project.bugs_data[type]
      old_line = old_bugs_data[type]
      new_line.pop.should eq(old_line.pop + 1)
      new_line.should == old_line
    end
  elsif change == "decrement"
    it 'decrements last element in the line' do
      if (data ||= nil)
        Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
        project.perform_bugs_report_update_existing data, bug_id
      else
        bug_data = JSON.parse(File.new('spec/factories/bugs/fetch_bug_result/'+bug_id.to_s+'.json').read)
        project.perform_bugs_report_update(bug_data)    
      end
      new_line = project.bugs_data[type]
      old_line = old_bugs_data[type]
      new_line.pop.should eq(old_line.pop - 1)
      new_line.should == old_line
    end
  else
    it 'does no change to the line' do
      if (data ||= nil)
        Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
        project.perform_bugs_report_update_existing data, bug_id
      else
        bug_data = JSON.parse(File.new('spec/factories/bugs/fetch_bug_result/'+bug_id.to_s+'.json').read)
        project.perform_bugs_report_update(bug_data)    
      end
      project.bugs_data[type].should == old_bugs_data[type]
    end
  end
end