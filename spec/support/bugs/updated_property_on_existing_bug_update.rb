shared_examples "updated_property_on_existing_bug_update" do |data,modified_items|
  project = FactoryGirl.build(:project)
  project.bugs_data = FactoryGirl.build(:test_bugs_data_1)
  project.save
  old_bugs_data = project.bugs_data.dup
  bug_id = 335083.to_s
  first = true
  data.zip(modified_items).each do |d,modified_item|
    old_data = old_bugs_data[modified_item]
    found = false
    old_data.each do |entry|
      found = true if entry.first == d[2]
    end
    unless found == true
      it 'adds a new entry for the property when it is in the required fields and had no entry before' do
        if first
          Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
          project.perform_bugs_report_update_existing data, bug_id
          first = false
        end
        new_data = project.bugs_data[modified_item]
        new_data.length.should eq(old_data.length+1)
        new_data.each do |item|
          unless item[0] == data[1]
            new_data.include?(item).should eq(true)
          else
            updated = item.dup
            updated[1]+= 1
            old_data.include?(updated).should eq(true)
          end
        end
        new_data.include?([d[2],1]).should eq(true)
      end
    else
      it 'increments the property of the bug when it is in the required fields and had an entry before' do
        if first
          Project.stub(:fetch_bug) { |id| fetch_bug_result(id) }
          project.perform_bugs_report_update_existing data, bug_id
          first = false
        end
        new_data = project.bugs_data[modified_item]
        new_data.length.should eq(old_data.length)
        old_data.each do |item|
          if item[0] == d[1]
            updated = [item[0],item[1]-1]
          elsif item[0] == d[2]
            updated = [item[0],item[1]+1]
          else
            updated = item
          end
          new_data.include?(updated).should eq(true)
        end
      end
    end
  end
end