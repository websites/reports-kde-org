shared_examples "generated_average_reply_line" do |type|
  describe 'Average reply line' do
    before(:each) do
      @res = [10.0/3,0,0,0,6,0,0,0]
      Timecop.travel(Time.utc(2015,6,22,18,37,6))
      @project = FactoryGirl.create(:reply_project)
      reply_class_for(type).create(test_reply_data(type,1))
    end

    after(:each) do
      Timecop.return
    end

    it 'prepares average reply line' do
      @project.get_average_reply_line(type).should eq(@res)
    end
  end
end