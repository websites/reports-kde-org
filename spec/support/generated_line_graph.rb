shared_examples "generated_line_graph" do |type,obj|

  describe 'Line graphs' do

    before(:each) do
      #TODO: replace that with some tests of the generation of the average reply files.
      Project.any_instance.stub(:empty_entries_array) do
        []
      end
      @project = FactoryGirl.create(:project)
      res = test_real_data_for_assignment(type,obj)
      @project.send("#{type}_data") << res
      @project.save
      new_date = Date.new(2014,06,28)
      Timecop.travel(new_date)
      @real_data = test_real_line_graphs_data type
      FakeFS.activate!
      @project.prepare_line_graphs type
      @locale = "en"
    end

    after(:each) do
      Timecop.return
      FakeFS.deactivate!
    end

    it 'prepares daily objects' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/objects.json").should eq(@real_data["objects"])
    end

    it 'prepares daily doers' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/doers_daily.json").should eq(@real_data["doers_daily"])
    end

    it 'prepares monthly doers' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/doers_monthly.json").should eq(@real_data["doers_monthly"])
    end

    it 'prepares yearly doers' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/doers_yearly.json").should eq(@real_data["doers_yearly"])
    end

  end

end
