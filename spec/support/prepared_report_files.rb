shared_examples "prepared_report_files" do |type|
    
  it 'calls suitable functions to (re)generate report files' do
    project = FactoryGirl.create(:empty_project)
    project.should_receive(:prepare_pie_charts).with(type)
    project.should_receive(:prepare_line_graphs).with(type)
    Project.should_receive(:prepare_all_kde_files).with(type)
    project.prepare_report_files(type)
  end
  
  it 'prepares all kde files' do
    project = FactoryGirl.create(:empty_project, title: "All KDE")
    Project.any_instance.should_receive(:prepare_pie_charts).with(type,true)
    Project.any_instance.should_receive(:prepare_line_graphs).with(type,true)
    Project.prepare_all_kde_files(type)
  end
  
end