shared_examples "generated_pie_chart" do |type,obj|
  #TODO: needs generalization. For now it assumes that always data has entires less than max number in all charts except total.

  describe 'Pie charts' do

    before(:each) do
      @project = FactoryGirl.create(:project)
      res = test_real_data_for_assignment type,obj
      @project.send("#{type}_data") << res
      @project.save
      @real_data = test_real_pie_charts_data type
      FakeFS.activate!
      new_time = Date.new(2012,06,21)
      Timecop.travel(new_time)
      @project.prepare_pie_charts type
      @locale = "en"
    end

    after(:each) do
      Timecop.return
      FakeFS.deactivate!
    end

    it 'prepares today' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/today.json").should eq(@real_data["today"])
    end

    it 'prepares this week' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/week.json").should eq(@real_data["week"])
    end

    it 'prepares this month' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/month.json").should eq(@real_data["month"])
    end

    it 'prepares this year' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      File.read("public#{path}/#{type}/year.json").should eq(@real_data["year"])
    end

    it 'prepares total, handles the case where number of contributors is more than max. to show top 10 and all' do
      path = Rails.application.routes.url_helpers.project_path(@locale,@project)
      test_json_equality File.read("public#{path}/#{type}/total_all.json"),@real_data["total_all"]
      test_json_equality File.read("public#{path}/#{type}/total_top_10.json"),@real_data["total_top_10"]
    end

    it 'puts correct data in drawables fields' do
      real = {"total" => true, "year" => false, "month" => false, "week" => false, "today" => false}
      @project.drawables[type].should eq(real)
    end

  end
end