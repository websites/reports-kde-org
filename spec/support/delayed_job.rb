def delayed_job_work
  Delayed::Job.all.each do |job|
    job.payload_object.perform
    job.destroy
  end
end