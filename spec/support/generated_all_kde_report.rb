shared_examples "generated_all_kde_report" do |type|

  describe 'All KDE report' do

    before(:each) do
      @objects = {"commits" => CommitsData, "mailing_messages" => MailingMessagesData, "irc_messages" => IrcMessagesData}
      @project = FactoryGirl.create(:kde_community_project)
      @locale = "en"
    end

    context 'Line graphs' do

      before(:each) do
        @objects[type].destroy_all
        project = FactoryGirl.create(:project)
        project.send("#{type}_data") << @objects[type].new({contributor: "Tester1", day: Date.new(2014,6,29), number: 3})
        project2 = FactoryGirl.create(:project)
        project2.send("#{type}_data") << @objects[type].new({contributor: "Tester2", day: Date.new(2014,6,29), number: 5})
        project2.send("#{type}_data") << @objects[type].new({contributor: "Tester1", day: Date.new(2014,7,4), number: 2})
        Timecop.travel(Date.new(2014,7,5))
        @project.set_all_kde_start_dates
        FakeFS.activate!
        @project.prepare_line_graphs type, true
      end

      after(:each) do
        Timecop.return
        FakeFS.deactivate!
      end

      it 'prepares daily objects' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = [8,0,0,0,0,2,0]
        File.read("public#{path}/#{type}/objects.json").should eq(real.to_json)
      end

      it 'prepares daily doers' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = [2,0,0,0,0,1,0]
        File.read("public#{path}/#{type}/doers_daily.json").should eq(real.to_json)
      end

      it 'prepares monthly doers' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = [2,1]
        File.read("public#{path}/#{type}/doers_monthly.json").should eq(real.to_json)
      end

      it 'prepares yearly doers' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = [2]
        File.read("public#{path}/#{type}/doers_yearly.json").should eq(real.to_json)
      end

    end

    context 'Pie charts' do

      before(:each) do
        @objects[type].destroy_all
        Timecop.travel(Date.new(2014,7,4))
        res = test_real_data_for_assignment type, "general"
        FakeFS.activate!
        project = FactoryGirl.create(:project)
        project.send("#{type}_data") << @objects[type].new({contributor: "Tester1", day: Date.new(2014,6,29), number: 3})
        project2 = FactoryGirl.create(:project)
        project2.send("#{type}_data") << @objects[type].new({contributor: "Tester2", day: Date.new(2014,6,29), number: 5})
        project2.send("#{type}_data") << @objects[type].new({contributor: "Tester1", day: Date.new(2014,7,4), number: 2})
        project3 = FactoryGirl.create(:project)
        project3.send("#{type}_data") << res
        @project.prepare_pie_charts type, true
      end

      after(:each) do
        Timecop.return
        FakeFS.deactivate!
      end

      it 'prepares today' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = {"data"=>[["Tester1", 2]], "colors"=>["#dc3912"]}
        test_json_equality(File.read("public#{path}/#{type}/today.json"),real.to_json).should eq(true)
      end

      it 'prepares this week' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = {"data"=>[["Tester1", 2]], "colors"=>["#dc3912"]}
        test_json_equality(File.read("public#{path}/#{type}/week.json"),real.to_json).should eq(true)
      end

      it 'prepares this month' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = {"data"=>[["Tester1", 2]], "colors"=>["#dc3912"]}
        test_json_equality(File.read("public#{path}/#{type}/month.json"),real.to_json).should eq(true)
      end

      it 'prepares this year' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        real = {"data"=>[["Tester1", 5], ["Tester2", 5]], "colors"=>["#dc3912", "#ff9900"]}
        test_json_equality(File.read("public#{path}/#{type}/year.json"),real.to_json).should eq(true)
      end

      it 'prepares total, it shows top 10 only when the number of contributors is more than max' do
        path = Rails.application.routes.url_helpers.project_path(@locale,@project)
        data = JSON.parse(File.read("public#{path}/#{type}/total.json"))
        data["data"].length.should eq(11)
        data["colors"].length.should eq(11)
      end

      it 'puts correct data in drawables fields' do
        real = {"total" => false, "year" => false, "month" => false, "week" => false, "today" => false}
        @project.drawables[type].should eq(real)
      end

    end

  end

end