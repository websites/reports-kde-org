require 'spec_helper'
include Warden::Test::Helpers

shared_examples "a developer" do
  it "Doesn't create new news" do
    visit news_index_url
    page.should_not have_link "New News", new_news_url
    visit new_news_url
    page.should have_content "You are not authorized to access this page."
  end

  it "Sees news" do
    news = FactoryGirl.create(:news)
    visit news_index_url
    page.should have_link news.title, news_url("en", news)
    visit news_url("en", news)
    page.should have_content news.content
    page.should_not have_content "You are not authorized to access this page."
  end

  it "Doesn't edit news" do
    news = FactoryGirl.create(:news)
    visit news_url("en", news)
    page.should_not have_link "Edit", edit_news_url("en", news)
    visit edit_news_url("en", news)
    page.should have_content "You are not authorized to access this page."
  end
end

shared_examples "an editor" do
  it "Creates new news" do
    visit news_index_url
    page.should have_link "New news", new_news_url
    visit new_news_url
    page.should_not have_content "You are not authorized to access this page."
  end

  it "Sees news" do
          news = FactoryGirl.create(:news)
    visit news_index_url
    page.should have_link news.title, news_url("en", news)
    visit news_url("en", news)
    page.should have_content news.content
    page.should_not have_content "You are not authorized to access this page."
  end

  it "Creates News and sees its new News" do
    visit news_index_url
    expect{
      click_link "New news"
      fill_in "Title", with: "Test"
      fill_in "Content", with: "This is a Test Content"
      click_button "Create News"
    }.to change(News, :count).by(1)
    page.should have_content "News was successfully created."
    within 'h1' do
      page.should have_content "Test"
    end
    page.should have_content "This is a Test Content"
  end

  it "Edits its own news" do
    visit news_index_url
    expect{
      click_link "New news"
      fill_in "Title", with: "Test"
      fill_in "Content", with: "This is a Test Content"
      click_button "Create News"
    }.to change(News, :count).by(1)
    page.should have_content "News was successfully created."
    expect{
      click_link "Edit"
      fill_in "Title", with: "New Content"
      click_button "Update News"
    }.to change(Project, :count).by(0)
    within 'h1' do
      page.should have_content "New Content"
      page.should_not have_content "Test"
    end
  end
end

describe "Reports app", type: :feature do

  before :each do
    User.any_instance.stub(:assign_role).and_return(true)
    stub_request(:get, "http://planetkde.org/rss20.xml").to_return(:status => 200, :body => test_planet_response(1))
  end

  describe "Manage news" do

    context "When not logged in" do
      it_behaves_like "a developer"
    end

    context "When logged in as a developer" do
      before :each do
        user = FactoryGirl.create(:developer)
        login_as user, scope: :user
      end
      it_behaves_like "a developer"
    end

    context "When logged in as a super developer" do
      before :each do
        user = FactoryGirl.create(:super_developer)
        login_as user, scope: :user
      end
      it_behaves_like "a developer"
    end

    context "When logged in as an editor" do
      before :each do
        user = FactoryGirl.create(:editor)
        login_as user, scope: :user
      end
      it_behaves_like "an editor"

      it "Doesn't edit news that don't belong to it" do
        editor = FactoryGirl.create(:editor)
        news = FactoryGirl.create(:news, user: editor)
        visit news_url("en", news)
        page.should_not have_content "You are not authorized to access this page."
        visit edit_news_url("en", news)
        page.should have_content "You are not authorized to access this page."
      end
    end

    context "When logged in as a super editor" do
      before :each do
        user = FactoryGirl.create(:super_editor)
        login_as user, scope: :user
      end
      it_behaves_like "an editor"

      it "Edits news that don't belong to it" do
        editor = FactoryGirl.create(:editor)
        news = FactoryGirl.create(:news, user: editor)
        visit news_url("en", news)
        page.should have_link "Edit", edit_news_url("en", news)
        visit edit_news_url("en", news)
        page.should_not have_content "You are not authorized to access this page."
      end
    end

    context "When logged in as an admin" do
      before :each do
        user = FactoryGirl.create(:admin)
        login_as user, scope: :user
      end

      it "Creates new news" do
        visit news_index_url
        page.should have_link "New news", new_news_url
        visit new_news_url
        page.should_not have_content "You are not authorized to access this page."
      end
    end
  end
end
