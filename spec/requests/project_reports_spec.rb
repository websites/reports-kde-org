#!/bin/env ruby
# encoding: utf-8

require 'spec_helper'
include Warden::Test::Helpers

shared_examples "a not logged in user" do
  it "Doesn't Access the Admin Panel" do
    visit admin_index_path
    page.should have_content "You are not authorized to access this page."
  end

  it "Doesn't see New Project button" do
    visit projects_path
    page.should_not have_link "New Project", new_project_path
  end

  it "Doesn't access New Project page" do
    visit new_project_path
    page.should have_content "You are not authorized to access this page."
  end

  it "Sees Projects" do
    project = FactoryGirl.create(:project, title: "Test Title", large_description: "Test Large Description", description: "Test Description")
    visit projects_path
    page.should have_content "Test Title"
    page.should have_content "Test Description"
    visit project_path "en", project
    within 'h1' do
      page.should have_content "Test Title"
    end
    page.should have_content "Test Large Description"
    page.should_not have_content "Test Description"
  end

  it "Doesn't edit projects" do
    project = FactoryGirl.create(:project)
    visit project_path("en", project)
    page.should_not have_link "Edit", edit_project_path("en", project)
    visit edit_project_path("en", project)
    page.should have_content "You are not authorized to access this page."
  end
end

describe "Reports app", type: :feature do

  before :each do
    stub_request(:get, "http://planetkde.org/rss20.xml").to_return(:status => 200, :body => test_planet_response(1))
    stub_request(:get, "https://build.kde.org/api/json?tree=jobs%5Bname%5D").to_return(:status => 200, :body => test_build("all_jobs"))
  end

  describe "Manage projects" do

    before :each do
      User.any_instance.stub(:assign_role).and_return(true)
      # Stub Bugzilla API calls.
      stub_request(:get, 'https://bugs.kde.org/jsonrpc.cgi?method=Product.get_selectable_products').to_return(:status => 200, :body => fetch_selectable_products_ids)
      stub_request(:get, 'https://bugs.kde.org/jsonrpc.cgi?method=Product.get&params=%5B%7B%22include_fields%22:%5B%22name%22%5D,%22ids%22:%5B%22347%22,%20%22512%22,%20%22338%22,%20%22433%22%5D%7D%5D').to_return(:status => 200, :body => fetch_selectable_products_names)
    end

    context "When not logged in" do
      it_behaves_like "a not logged in user"
    end

    context "When logged in as a developer" do
      before :each do
        user = FactoryGirl.create(:developer)
        login_as user, scope: :user
      end

      it "Doesn't Access the Admin Panel" do
        visit admin_index_path
        page.should have_content "You are not authorized to access this page."
      end

      it "Sees New Project button" do
        visit projects_path
        page.should have_link "New Project", new_project_path
      end

      it "Accesses New Project page" do
        visit new_project_path
        page.should_not have_content "You are not authorized to access this page."
      end

      it "Adds a new project with title only and shows the project but doesn't show links to any report except project report" do
        visit projects_path
        expect{
          click_link "New Project"
          fill_all_blanks
          fill_in "Title", with: "Test"
          click_button "Create Project"
        }.to change(Project, :count).by(1)
        page.should have_content "Project was successfully created."
        within 'h1' do
          page.should have_content "Test"
        end
        test_all_side_bar_items
        delayed_job_work
        visit current_path
        test_all_side_bar_items
      end

      it "Adds a new project with title and community wiki description page and shows the content of the wiki page as large description" do
        stub_request(:post, "https://community.kde.org/api.php").with(:body => {"action"=>"parse", "format"=>"json", "page"=>"Test page", "prop"=>"text"}).to_return(:status => 200, :body => test_wikis_response("content",1), :headers => {})
        visit projects_path
        expect{
            click_link "New Project"
            fill_all_blanks
            fill_in "Title", with: "Test"
            fill_in "Community wiki description page", with: "Test page"
            click_button "Create Project"
        }.to change(Project, :count).by(1)
        delayed_job_work
        visit projects_path
        click_link "Test"
        expect(page).to have_link("An internal link", href: "https://community.kde.org/Test22")
        expect(page).to have_link("An external link", href: "https://reports.kde.org")
      end

      it "Adds a new project with title, large description and Bugzilla products and shows links to Bugs and Project Report but not to other reports" do
        visit projects_path
        expect{
            click_link "New Project"
            fill_all_blanks
            fill_in "Title", with: "Test"
            fill_in "Large description", with: "I am a test large description"
            select "Active", :from => "project_bugzilla_products"
            click_button "Create Project"
        }.to change(Project, :count).by(1)
        page.should have_content "Project was successfully created."
        within 'h1' do
          page.should have_content "Test"
        end
        expect(page).to have_content "I am a test large description"
        test_all_side_bar_items []
        bugs_stubber
        delayed_job_work
        visit current_path
        test_all_side_bar_items ["BUGS", "Bugs Report", "Bugs List"]
      end

      it "Adds a new project with title, large description, Bugzilla products and Git Repos and shows links to Bugs, Commits and Project Report but not to other reports" do
        visit projects_path
        expect{
            click_link "New Project"
            fill_all_blanks
            fill_in "Title", with: "Test"
            fill_in "Large description", with: "I am a test large description"
            select "Active", :from => "project_bugzilla_products"
            fill_in "Git repos", with: "dummy.git"
            click_button "Create Project"
        }.to change(Project, :count).by(1)
        page.should have_content "Project was successfully created."
        within 'h1' do
          page.should have_content "Test"
        end
        bugs_stubber
        commits_stubber
        page.should have_content "I am a test large description"
        test_all_side_bar_items
        delayed_job_work
        visit current_path
        test_all_side_bar_items ["BUGS", "Bugs Report", "Bugs List", "COMMITS", "Commits Report", "Commits List", "Repository"]
      end

      it "Adds a new project with title and facebook profile and sees a link to facebook report" do
        visit projects_path
        expect{
          click_link "New Project"
          fill_all_blanks
          fill_in "Title", with: "Test FB"
          fill_in "Facebook pages", with: "Test1\nTest2"
          click_button "Create Project"
        }.to change(Project, :count).by(1)
        test_all_side_bar_items
        stub_request(:get, "https://graph.facebook.com/Test1/notes?access_token=#{Tokens.facebook_token}&fields=comments.fields(id).limit(1).summary(1),created_time&limit=500").to_return(:status => 200, :body => facebook_pages_response(1,3), :headers => {})
        stub_request(:get, "https://graph.facebook.com/Test2/notes?access_token=#{Tokens.facebook_token}&fields=comments.fields(id).limit(1).summary(1),created_time&limit=500").to_return(:status => 200, :body => facebook_pages_response(1,3), :headers => {})
        stub_request(:get, "https://graph.facebook.com/Test1/posts?access_token=#{Tokens.facebook_token}&fields=likes.limit(1).summary(1),comments.fields(id).limit(1).summary(1),shares&limit=500").to_return(:status => 200, :body => facebook_pages_response(1,3), :headers => {})
        stub_request(:get, "https://graph.facebook.com/Test2/posts?access_token=#{Tokens.facebook_token}&fields=likes.limit(1).summary(1),comments.fields(id).limit(1).summary(1),shares&limit=500").to_return(:status => 200, :body => facebook_pages_response(1,3), :headers => {})
        delayed_job_work
        visit(current_path)
        test_all_side_bar_items ["SOCIAL NETWORKS", "Facebook Pages Report"]
      end

      it "Adds a new project with title and twitter profile and sees a link to facebook report" do
        stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=Test1").to_return(:status => 200, :body => twitter_user_response("kdecommunity"), :headers => {})
        stub_request(:get, "https://api.twitter.com/1.1/statuses/user_timeline.json?count=200&screen_name=Test1").to_return(:status => 200,:body => "[]" , :headers => {})
        GeneralObjects.should_receive(:restart_twitter_stream)
        visit projects_path
        expect{
          click_link "New Project"
          fill_all_blanks
          fill_in "Title", with: "Test Twitter"
          fill_in "Twitter profile", with: "Test1"
          click_button "Create Project"
        }.to change(Project, :count).by(1)
        test_all_side_bar_items []
        delayed_job_work
        visit current_path
        test_all_side_bar_items ["SOCIAL NETWORKS", "Twitter Report"]
      end

      it "Adds a new project with no title, shows error title can not be blank" do
        visit projects_path
        click_link "New Project"
        fill_all_blanks
        fill_in "Title", with: ""
        click_button "Create Project"

        expect(page).to have_content "Title can't be blank"
      end

      it "Edits its own project" do
        visit projects_path
        expect{
            click_link "New Project"
            fill_in "Title", with: "Test"
            fill_in "Large description", with: "I am a test large description"
            select "Active", :from => "project_bugzilla_products"
            fill_in "Git repos", with: "test_repo"
            fill_in "Mailing lists", with: ""
            fill_in "Irc channels", with: ""
            click_button "Create Project"
        }.to change(Project, :count).by(1)
        within "h1" do
          page.should have_content "Test"
        end
        expect{
          click_link "Edit"
          fill_in "Title", with: "New Content"
          click_button "Update Project"
        }.to change(Project, :count).by(0)
        within 'h1' do
          page.should have_content "New Content"
          page.should_not have_content "Test"
        end
      end

      it "Edits project title with blank, shows error title can not be blank" do
        visit projects_path
        expect{
          click_link "New Project"
          fill_in "Title", with: "Test"
          click_button "Create Project"
        }.to change(Project, :count).by(1)
        click_link "Edit"
        fill_in "Title", with: ""
        click_button "Update Project"

        expect(page).to have_content "Title can't be blank"
      end

      it "Doesn't edit projects it didn't create" do
        project = FactoryGirl.create(:project)
        visit project_path("en", project)
        visit edit_project_path("en", project)
        page.should have_content "You are not authorized to access this page."
      end
    end

    context "When logged in as a super developer" do
      before :each do
        user = FactoryGirl.create(:super_developer)
        login_as user, scope: :user
      end

      it "Doesn't Access the Admin Panel" do
        visit admin_index_path
        page.should have_content "You are not authorized to access this page."
      end

      it "Creates and edits its own projects" do
        visit projects_path
        expect{
          click_link "New Project"
          fill_in "Title", with: "Test"
          fill_in "Git repos", with: ""
          fill_in "Mailing lists", with: ""
          fill_in "Irc channels", with: ""
          click_button "Create Project"
        }.to change(Project, :count).by(1)
        page.should have_content "Project was successfully created."
        within 'h1' do
          page.should have_content "Test"
        end
        expect{
          click_link "Edit"
          fill_in "Title", with: "New Content"
          click_button "Update Project"
        }.to change(Project, :count).by(0)
        within 'h1' do
          page.should have_content "New Content"
          page.should_not have_content "Test"
        end
      end

      it "Edits other developers projects" do
        project = FactoryGirl.create(:project)
        visit project_path("en", project)
        page.should have_link "Edit", edit_project_path("en", project)
        visit edit_project_path("en", project)
        page.should_not have_content "You are not authorized to access this page."
      end

      it "Doesn't edit all KDE project" do
        project = FactoryGirl.create(:kde_community_project)
        visit project_path("en", project)
        page.should_not have_link "Edit", edit_project_path("en", project)
        visit edit_project_path("en", project)
        page.should have_content "You are not authorized to access this page."
      end
    end

    context "When logged in as an editor" do
      before :each do
        user = FactoryGirl.create(:editor)
        login_as user, scope: :user
      end
      it_behaves_like "a not logged in user"
    end

    context "When logged in as a super editor" do
      before :each do
        user = FactoryGirl.create(:super_editor)
        login_as user, scope: :user
      end
      it_behaves_like "a not logged in user"
    end

    context "When logged in as an admin" do
      before :each do
        user = FactoryGirl.create(:admin)
        login_as user, scope: :user
      end

      it "Sees a link to Admin panel" do
        visit root_path
        page.should have_link "Admin Panel", admin_index_path
      end

      it "Accesses the Admin Panel" do
        visit admin_index_path
        page.should_not have_content "You are not authorized to access this page."
      end

      it "Sees a link to New Project when logged in as admin" do
        visit projects_path
        page.should have_link "New Project", new_project_path
      end

      it "Adds a new project page when logged in as admin" do
        visit new_project_path
        page.should_not have_content "You are not authorized to access this page."
      end
    end
  end

  describe 'Bugs' do

    before(:each) do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22amarok%22%5D,%22include_fields%22:%5B%22assigned_to%22,%22classification%22,%22component%22,%22creator%22,%22op_sys%22,%22platform%22,%22priority%22,%22product%22,%22severity%22,%22status%22,%22version%22,%22creation_time%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_report_test_response(2), :headers => {})
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22amarok%22%5D,%22include_fields%22:%5B%22creation_time%22,%22last_change_time%22,%22status%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_line_graph_report_test_response(2))
      bugzilla_products = ["amarok"]
      bugs_report_fields = ["assigned_to","classification","component","creator","op_sys","platform","priority","product","severity","status","version"]
      @project = FactoryGirl.create(:empty_project, bugzilla_products: bugzilla_products, bugs_report_fields: bugs_report_fields)
      delayed_job_work
    end

    after(:each) do
      locale = "en"
      FileUtils.rm_rf("public/#{project_path(locale,@project)}")
    end

    it 'sees report' do
      visit project_path("en",@project)
      click_link "Bugs Report"
      page.status_code.should be(200)
    end

    it 'navigates through list' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22amarok%22%5D,%22include_fields%22:%5B%22id%22,%22assigned_to%22,%22component%22,%22last_change_time%22,%22status%22,%22summary%22%5D,%22limit%22:10,%22offset%22:90,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_list(90), :headers => {})
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22amarok%22%5D,%22include_fields%22:%5B%22id%22,%22assigned_to%22,%22component%22,%22last_change_time%22,%22status%22,%22summary%22%5D,%22limit%22:10,%22offset%22:80,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_list(80), :headers => {})
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.get&params=%5B%7B%22ids%22:%5B78272%5D%7D%5D").to_return(:status => 200, :body => fetch_bug_response(78272))
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.comments&params=%5B%7B%22ids%22:%5B78272%5D%7D%5D").to_return(:status => 200, :body => fetch_comments_response(78272))
      visit project_path("en",@project)
      click_link "Bugs List"
      bugs = bugs_list_ids(90)
      bugs.each do |bug_id|
        page.should have_link bug_id, bug_project_path("en", @project,bug_id)
      end
      no_bug = bugs_list_ids(89).first
      page.should_not have_content(no_bug)
      click_link "2"
      page.should have_link no_bug, bug_project_path("en", @project,no_bug)
      click_link no_bug
      page.status_code.should be(200)
    end

    it 'sees One' do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.get&params=%5B%7B%22ids%22:%5B335083%5D%7D%5D").to_return(:status => 200, :body => fetch_bug_response(335083))
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.comments&params=%5B%7B%22ids%22:%5B335083%5D%7D%5D").to_return(:status => 200, :body => fetch_comments_response(335083))
      bug_id = 335083
      bug = fetch_bug_result(bug_id)
      visit bug_project_path("en", @project,bug_id)
      bug.each do |k,val|
        next if ["creation_time","last_change_time","is_creator_accessible"].include?(k)
        if val.class == Array
          val.each do |v|
            page.should have_content v
          end
        else
          page.should have_content val
        end
      end
      comments = fetch_comments_result(335083)
      comments.each do |comment|
        page.should have_content comment["creator"]
        page.should have_content comment["text"]
      end
    end

  end

  describe 'Commits' do

    before(:each) do
      Project.stub(:repo) do |product|
        test_repo(product)
      end

      @repo = "dummy.git"
      @project = FactoryGirl.create(:empty_project, git_repos: @repo)
      delayed_job_work
    end

    after(:each) do
      locale = "en"
      FileUtils.rm_rf("public/#{project_path(locale,@project)}")
    end

    it 'sees report' do
      visit project_path("en",@project)
      click_link "Commits Report"
      page.status_code.should be(200)
    end

    it 'navigates through list' do
      Project.stub(:repo) do |product|
        test_repo(product)
      end
      visit project_path("en",@project)
      click_link "Commits List"
      g = test_repo(@repo)
      shas = g.log(10).to_a.map {|i| i.sha}
      shas.each do |sha|
        page.should have_link sha[0...8], commit_project_path("en", @project,@repo,sha)
        page.should have_content g.gcommit(sha).author.name
      end
      no_sha = g.log(11).last.sha
      page.should_not have_content no_sha[0...8]
      click_link "2"
      page.should have_link no_sha[0...8], commit_project_path("en", @project,@repo,no_sha)
      click_link no_sha[0...8]
      page.status_code.should be(200)
    end

    it 'sees One' do
      Project.stub(:repo) do |product|
        test_repo(product)
      end
      g = test_repo(@repo)
      commit = g.log(1).first
      visit commit_project_path("en", @project,@repo,commit.sha)
      page.should have_content commit.author.name
      page.should have_content commit.author.email
      page.should have_content commit.author.date
      #TODO: It'd be better to use a commit here with author is different than committer.
      page.should have_content commit.committer.name
      page.should have_content commit.committer.email
      page.should have_content commit.committer.date
      page.should have_content commit.message.replace("\n")
      click_link "Back"
      current_path.should == commits_list_project_path("en",@project)
    end

  end

  describe 'Mailing Lists' do

    before(:each) do
      Project.stub(:all_messages) do |mlist|
        test_all_messages(mlist)
      end
      Project.stub(:all_messages) do |mlist|
        test_all_messages(mlist)
      end
      Project.any_instance.stub(:fetch_mbox_file)
      @mlists = ["test1.mbox","test2.mbox"]
      @project = FactoryGirl.create(:empty_project, mailing_lists: @mlists.join("\n"))
      FactoryGirl.create(:kde_community_project)
      delayed_job_work
    end

    after(:each) do
      locale = "en"
      FileUtils.rm_rf("public/#{project_path(locale,@project)}")
      @mlists.each do |list|
        FileUtils.rm_rf("mbox_files/#{list[0...-5]}")
      end
    end

    it 'sees report' do
      visit project_path("en",@project)
      click_link "Mailing lists Report"
      page.status_code.should be(200)
    end

    it 'Sees list' do
      visit project_path("en",@project)
      click_link "Messages list"
      @mlists.each do |list|
        page.should have_link list, messages_list_mbox_project_path("en", @project,list)
      end
      list = @mlists.first
      click_link list
      month = Date.new(2014,6,1)
      page.should have_link month.strftime("%B %Y"), messages_of_month_project_path("en",@project,list,month.strftime("%b%Y"))
      click_link month.strftime("%B %Y"), messages_of_month_project_path("en",@project,list,month.strftime("%b%Y"))
      subject = "libetonyek, odfgen etc packages for ubuntu?"
      page.should have_link subject, message_project_path("en",@project,list,month.strftime("%b%Y"),0)
      click_link subject
      page.status_code.should be(200)
    end

    it 'Sees One' do
      list = @mlists.first
      month = Date.new(2014,6,1)
      the_path = message_project_path("en",@project,list,month.strftime("%b%Y"),0)
      visit the_path
      subject = "libetonyek, odfgen etc packages for ubuntu?"
      message = "Hello,

It's been a while, so I thought I'd do a git pull and rebuild my Calligra packages. I've migrated to Kubuntu 14.04 since the last time, but I have the impression that Calligra itself now requires newer versions of the odfgen, etonyek (etc) packages than those I built when I first started evaluation 2.9 .
So my question is: is there a ppa where I can find the most importance of those dependencies, or another source of prebuilt packages?

Thanks,
René"
      page.should have_content subject
      page.should have_content message
      page.should have_link "Next Message", message_project_path("en",@project,list,month.strftime("%b%Y"),1)
      page.should_not have_content "Previous Message"
      click_link "Next Message"
      click_link "Previous Message"
      current_path.should eq(the_path)
      click_link "Back"
      current_path.should eq(messages_of_month_project_path("en",@project,list,month.strftime("%b%Y")))
    end

  end

  describe 'IRC Channels' do

    before(:each) do
      Dj.any_instance.stub(:fork)
      irc_channels = ["#testing_test","#testing_test2"]
      @project = FactoryGirl.create(:empty_project, irc_channels: irc_channels.join("\n"))
      GeneralObjects.should_receive(:restart_irc_bot)
      GeneralObjects.should_receive(:restart_irc_bot)
      FactoryGirl.create(:kde_community_project)
      delayed_job_work
    end

    after(:each) do
      locale = "en"
      FileUtils.rm_rf("public/#{project_path(locale,@project)}")
    end

    it 'sees report' do
      visit project_path("en",@project)
      click_link "IRC Channels Report"
      page.status_code.should be(200)
    end

  end

  describe 'Forums' do

    before(:each) do
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_posts_response("integration"), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=0&submit=Search&sv=1").to_return(:status => 200, :body => test_forums_resolved_response("integration"), :headers => {})
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=100&feed_style=COMPACT&feed_type=RSS2.0&sd=d&sf=firstpost&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_threads_response(3), :headers => {})
      forum_ids = ["1","53"]
      Project.prepare_forum_posts_data
      @project = FactoryGirl.create(:empty_project, forum_ids: forum_ids.join("\n"))
      delayed_job_work
    end

    after(:each) do
      locale = "en"
      FileUtils.rm_rf("public/#{project_path(locale,@project)}")
    end

    it 'sees report' do
      visit project_path("en",@project)
      click_link "Forums Report"
      page.status_code.should be(200)
    end

    it 'sees topics list' do
      stub_request(:get, "https://forum.kde.org/search.php?countlimit=10&feed_style=COMPACT&feed_type=RSS2.0&fid%5B0%5D=1&fid%5B1%5D=53&sd=d&sf=titleonly&st=0&start=0&submit=Search").to_return(:status => 200, :body => test_forums_threads_response("latest"), :headers => {})
      visit project_path("en",@project)
      click_link "Forums Topics list"
      page.status_code.should be(200)
      page.should have_content "Forums Topics for"
      page.should have_content "Backup tool for kde"
      page.should have_content "Lagy preview - performance"
    end

  end

  describe 'Facebook' do

    before(:each) do
      stub_facebook
      facebook_pages = ["test1"]
      @project = FactoryGirl.create(:empty_project, facebook_pages: facebook_pages.join("\n"))
      delayed_job_work
    end

    after(:each) do
      locale = "en"
      FileUtils.rm_rf("public/#{project_path(locale,@project)}")
    end

    it 'sees report' do
      visit project_path("en",@project)
      click_link "Facebook Pages Report"
      page.status_code.should be(200)
    end

  end

  describe 'KDE Community Project' do

    before(:each) do
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Product.get_selectable_products").to_return(:status => 200, :body => fetch_selectable_products_ids)
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Product.get&params=%5B%7B%22include_fields%22:%5B%22name%22%5D,%22ids%22:%5B%22347%22,%20%22512%22,%20%22338%22,%20%22433%22%5D%7D%5D").to_return(:status => 200, :body => fetch_selectable_products_names)
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22Active%22,%22abakus%22,%22adept%22,%22aki%22%5D,%22include_fields%22:%5B%22creation_time%22,%22status%22,%22priority%22,%22assigned_to%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_report_test_response(1), :headers => {})
      stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22Active%22,%22abakus%22,%22adept%22,%22aki%22%5D,%22include_fields%22:%5B%22creation_time%22,%22last_change_time%22,%22status%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_line_graph_report_test_response(1))
      @project = FactoryGirl.create(:empty_project, title: "KDE Community")
      Project.initial_all_kde_preparation
      delayed_job_work
    end

    it 'sees KDE Community in Projects page' do
      visit projects_path("en")
      page.should have_link "KDE Community", project_path("en",@project)
    end

    it 'sees a link to KDE Community in Home page' do
      visit root_path("en")
      page.should have_link "KDE Community", project_path("en",@project)
    end

    it 'sees proper entries in side menu of KDE Community' do
      visit project_path("en", @project)
      page.should_not have_content "Project Report"
      page.should have_link "Bugs Report", bugs_report_project_path("en",@project)
      page.should_not have_content "Bugs List"
      page.should have_link "Commits Report", commits_report_project_path("en",@project)
      page.should_not have_content "Commits List"
      page.should have_link "Mailing lists Report", mailing_lists_report_project_path("en",@project)
      page.should_not have_content "Messages List"
      page.should have_link "IRC Channels Report", irc_channels_report_project_path("en",@project)
    end

    it 'sees bugs report' do
      visit bugs_report_project_path("en",@project)
      page.status_code.should be(200)
      click_link "Back"
      current_path.should eq(project_path("en",@project))
    end

    it 'sees commits report' do
      visit commits_report_project_path("en",@project)
      page.status_code.should be(200)
      click_link "Back"
      current_path.should eq(project_path("en",@project))
    end

    it 'sees mailing lists report' do
      visit mailing_lists_report_project_path("en",@project)
      page.status_code.should be(200)
      click_link "Back"
      current_path.should eq(project_path("en",@project))
    end

  end

end
