#!/bin/env ruby
# encoding: utf-8

require 'spec_helper'

describe ReportMailer do

  describe 'commits mails' do

    context 'master branch commits with english letters' do

      before(:all) do
        @the_mail = File.open('spec/factories/commits/mailers/main_test.mbox').read
      end

      it 'sends new commits mails to the appropriate parser function' do
        ReportMailer.any_instance.should_receive(:update_commits).with(Mail.new(@the_mail))
        ReportMailer.any_instance.should_not_receive(:new_bug).with(Mail.new(@the_mail))
        ReportMailer.any_instance.should_not_receive(:update_bug).with(Mail.new(@the_mail))
        ReportMailer.any_instance.should_not_receive(:update_mailing_list).with(Mail.new(@the_mail))
        ReportMailer.receive(@the_mail)
      end

      it 'parses new commits mails correctly' do
        repo = "plasma-workspace.git"
        Project.should_receive(:update_commits_data).with(repo)
        ReportMailer.receive(@the_mail)
      end

    end

    it 'discards new commits made to branches other than master' do
      the_mail = File.open('spec/factories/commits/mailers/test_not_master.mbox').read
      Project.should_not_receive(:update_commits_data)
      ReportMailer.receive(the_mail)
    end

  end

  describe 'bugs mails' do

    it 'parses changing bugs mails with multiple applicable changes' do
      the_mail = File.open('spec/factories/bugs/mailers/bug_main_test.mbox').read
      data = [["Status","UNCONFIRMED","RESOLVED"],["Priority","NOR","HI"]]
      bug_id = 336913
      Project.should_receive(:update_existing_bug).with("systemsettings",data,bug_id.to_s)
      ReportMailer.receive(the_mail)
    end

  end

end