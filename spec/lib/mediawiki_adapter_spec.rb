require 'spec_helper'

describe MediawikiAdapter do

  it 'gets a wiki page content' do
    stub_request(:post, "https://community.kde.org/api.php").with(:body => {"action"=>"parse", "format"=>"json", "page"=>"Test page", "prop"=>"text"}).to_return(:status => 200, :body => test_wikis_response("content",1), :headers => {})
    res = MediawikiAdapter.get_page_content "community", "Test page"
    expect(res).to eq(test_wikis_data("content",1,"html"))
  end

end