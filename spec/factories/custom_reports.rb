FactoryGirl.define do
  factory :custom_report do |f|
    f.title "Test report"
    f.description "Test description"

    factory :commits_custom_report do |f|
      f.generate_commits_report true
    end
  end
end
