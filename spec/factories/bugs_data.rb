FactoryGirl.define do
  factory :test_bugs_data_1, class: BugsData do |f|
    f.assigned_to [["aa@aaa.com", 3], ["aaa@aa.com", 6]]
    f.classification [["Unclassified", 9]]
    f.component [["general", 9]]
    f.creator [["xx@xx.com", 4], ["xxx@xx.com", 5]]
    f.net_open_line [1,2,3,6]
    f.op_sys [["Android 3.x", 1], ["All", 1], ["IRIX", 1], ["other", 2], ["Linux", 4]]
    f.open_by_priority [["NOR", 6]]
    f.platform [["Mandriva RPMs", 1], ["Other", 1], ["Compiled Sources", 2], ["unspecified", 5]]
    f.priority [["NOR", 9]]
    f.product [["abakus", 3], ["aki", 6]]
    f.reported_line [2,2,2,3]
    f.resolved_by_assigned_to [["aaa@aa.com", 3]]
    f.resolved_line [1,1,1,0]
    f.still_open_line [1,1,1,3]
    f.average_close_time_line [0,0,2,0]
    f.resolved_after_a_year_line [0,0,0,0]
    f.severity [["normal", 4], ["wishlist", 5]]
    f.start_date 4.days.ago.at_beginning_of_day
    f.status [["UNCONFIRMED", 2], ["RESOLVED", 3], ["CONFIRMED", 4]]
    f.total_created_line [2,4,6,9]
    f.total_number 9
    f.total_average_close_time 5
    f.version [["0.0.2", 3], ["unspecified", 6]]
  end
end