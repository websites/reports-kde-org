FactoryGirl.define do 
  
  factory :developer, :class => User do |f|
    f.sequence(:login) { |n| "test_developer#{n}" }
    f.password "test1234"
    f.password_confirmation "test1234"
    after(:create) { |user| user.add_role "developer" }
  end
  
  factory :super_developer, :class => User do |f|
    f.sequence(:login) { |n| "test_super_developer#{n}" }
    f.password "test1234"
    f.password_confirmation "test1234"
    after(:create) { |user| user.add_role "super_developer" }
  end
  
  factory :editor, :class => User do |f|
    f.sequence(:login) { |n| "test_editor#{n}" }
    f.password "test1234"
    f.password_confirmation "test1234"
    after(:create) { |user| user.add_role "editor" }
  end
  
  factory :super_editor, :class => User do |f|
    f.sequence(:login) { |n| "test_super_editor#{n}" }
    f.password "test1234"
    f.password_confirmation "test1234"
    after(:create) { |user| user.add_role "super_editor" }
  end
  
  factory :admin, :class => User do |f|
    f.sequence(:login) { |n| "test_admin#{n}" }
    f.password "test1234"
    f.password_confirmation "test1234"
    after(:create) { |user| user.add_role "admin" }
  end
  
end