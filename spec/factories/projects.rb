FactoryGirl.define do
  factory :project do |f|
    f.title "Test_Project"
    f.bugzilla_products ["abakus","aki"]
    f.description "my test project"
    f.git_repos "calligra.git\nakonadi.git"
    f.irc_channels "#test1\n#test2"
    f.large_description "This is a good test project."
    f.mailing_lists "calligra-devel\nkde-doc-english"
    f.twitter_profile "kdecommunity"
    f.google_plus_profiles "105126786256705328374"
    f.forum_ids "154\n121"
    f.bugs_report_fields ["assigned_to","classification","component","creator","op_sys","platform","priority","product","severity","status","version"]
  end

  factory :empty_project, class: Project do |f|
    f.title "empty project"
  end

  factory :kde_community_project, class: Project do |f|
    f.title "KDE Community"
    f.mailing_messages_start_date Date.today
    f.irc_messages_start_date Date.today
  end

  factory :reply_project, class: Project do |f|
    f.title "reply project"
    f.mailing_lists "a.mbox\nb.mbox"
    f.irc_channels "#ch1\n#ch2"
    f.forum_ids "1\n2"
    #TODO: Add IRC and Forum
    f.mailing_messages_start_date Date.new(2015,6,15)
    f.irc_messages_start_date Date.new(2015,6,15)
    f.forum_posts_start_date Date.new(2015,6,15)
  end
end