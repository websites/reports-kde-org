FactoryGirl.define do 
  factory :news do |f|
    f.title "Test Title"
    f.content "Test Content"
    association :user, factory: :editor
  end  
end