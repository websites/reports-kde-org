require 'spec_helper'

describe ProjectsController do
  
  
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response.status).to eq(200)
    end
    
    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
    
    it "loads projects with no parents" do
      Project.should_receive(:roots)
      get :index
    end
    
  end
  
  describe "GET #show" do
    
    before(:each) do
      @project = FactoryGirl.create(:empty_project)     
    end
    
    it "renders the show template when the project has no children" do
      Project.any_instance.should_receive(:children).and_return([])
      get :show, id: @project
      expect(response).to render_template("show")
    end
    
    it "renders the index template when the project has children" do
      Project.any_instance.should_receive(:children).and_return([@project])
      get :show, id: @project
      expect(response).to render_template("index")
    end
  end
  
end
