module Helpers
  # Bugs part:
  def bugs_data_test num
    bugs_data = JSON.parse(File.new('spec/factories/bugs/bugs_data_test/'+num.to_s+'.json').read)
    bugs_data["start_date"] = 4.days.ago.at_beginning_of_day.to_i * 1000
    bugs_data
  end

  def bugs_list offset = 1, limit = 10
    all_bugs = JSON.parse(File.new('spec/factories/bugs/general/list_of_bugs.json').read)
    initial = '{"error":null,"id":"https://bugs.kde.org/","result":{"bugs":'
    final = '}}'
    return initial + all_bugs[offset-1...offset-1+limit].to_json + final
  end

  def bugs_list_ids offset = 1, limit = 10
    all_bugs = JSON.parse(File.new('spec/factories/bugs/general/list_of_bugs.json').read)
    res = Array.new
    all_bugs[offset-1...offset-1+limit].each do |bug|
      res << bug["id"]
    end
    return res
  end

  def bugs_list_array offset = 1, limit = 10
    all_bugs = JSON.parse(File.new('spec/factories/bugs/general/list_of_bugs.json').read)
    return all_bugs[offset-1...offset-1+limit]
  end

  def bugs_report_test_response num
    File.new('spec/factories/bugs/bugs_report_test_response/'+num.to_s+'.json').read
  end

  def bugs_test_report num
    JSON.parse(File.new('spec/factories/bugs/bugs_test_report/'+num.to_s+'.json').read)
  end

  def bugs_line_graph_report_test_response num
    File.new('spec/factories/bugs/bugs_line_graph_report_test_response/'+num.to_s+'.json').read
  end

  def bugs_line_graph_report_test_report num
    res = JSON.parse(File.new('spec/factories/bugs/bugs_line_graph_report_test_report/'+num.to_s+'.json').read)
    res["start_date"] = res["start_date"].to_date
    res
  end

  def generated_report_files
    res = Hash.new
    Dir.glob("spec/factories/bugs/generated_report_files/*.json") do |file|
      res[File.basename(file,".json")] = File.read(file)
    end
    res
  end

  def count_bugs_by_successful num
    JSON.parse(File.new('spec/factories/bugs/count_bugs_by_successful/'+num.to_s+'.json').read)
  end

  def count_bugs_by_result num, property
    JSON.parse(File.new('spec/factories/bugs/count_bugs_by_result_'+property+ '/' + num.to_s + '.json').read)
  end

  def fetch_bug_response bug_id
    File.new('spec/factories/bugs/fetch_bug_response/'+bug_id.to_s+'.json').read
  end

  def fetch_bug_result bug_id
    JSON.parse(File.new('spec/factories/bugs/fetch_bug_result/'+bug_id.to_s+'.json').read)
  end

  def fetch_comments_response bug_id
    File.new('spec/factories/bugs/fetch_comments/responses/'+bug_id.to_s+'.json').read
  end

  def fetch_comments_result bug_id
    JSON.parse(File.new('spec/factories/bugs/fetch_comments/results/'+bug_id.to_s+'.json').read)
  end

  def fetch_selectable_products_ids
    File.new('spec/factories/bugs/general/selectable_products_ids.json').read
  end

  def fetch_selectable_products_names
    File.new('spec/factories/bugs/general/selectable_products_names.json').read
  end

  def real_selectable_products_result
    JSON.parse(File.new('spec/factories/bugs/general/real_selectable_products_result.json').read)
  end

  # Used in integration tests to allow the generation of a dummy bugs report.
  def bugs_stubber
    stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22Active%22%5D,%22include_fields%22:%5B%22creation_time%22,%22status%22,%22priority%22,%22assigned_to%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_report_test_response(1), :headers => {})
    stub_request(:get, "https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=%5B%7B%22product%22:%5B%22Active%22%5D,%22include_fields%22:%5B%22creation_time%22,%22last_change_time%22,%22status%22%5D,%22limit%22:0,%22offset%22:0,%22creation_time%22:%222002-09-16T00:00:00Z%22%7D%5D").to_return(:status => 200, :body => bugs_line_graph_report_test_response(1))
  end

  # Commits part:

  def test_repo product
    return Git.bare('.', { :repository => 'spec/factories/commits/repositories/' + product ,:index => '.' })
  end

  #TODO: Does not support the case where a certain contributor has done commits in more than one of the project's repos on the same day.
  def test_real_commits_data repos
    res = Array.new
    repos.each do |repo|
      json_data = File.open("spec/factories/commits/commits_data/#{repo}.json").read
      res+= JSON.parse(json_data)
    end
    res
  end

  def test_real_number_of_commits repos
    res = 0
    repos.each do |repo|
      raw_data = File.open("spec/factories/commits/number_of_commits/#{repo}").read
      res+= raw_data.to_i
    end
    res
  end

  def commits_stubber
    Project.stub(:repo) do |product|
      test_repo(product)
    end
  end

  # Mailing Lists Part:

  def test_all_messages mlist
    return Mbox.open("spec/factories/mailing_lists/mbox_files/"+mlist)
  end

  def test_email num
    Mail.read("spec/factories/mailing_lists/test_emails/#{num.to_s}.mbox")
  end

  def test_email_data num
    json_data = File.open("spec/factories/mailing_lists/test_emails_data/#{num.to_s}.json").read
    JSON.parse(json_data)
  end

  #TODO: Does not support the case where a certain contributor has posted a message to more than one of the project's mailing lists on the same day.
  def test_real_mailing_messages_data mlists
    res = Array.new
    mlists.each do |mlist|
      json_data = File.open("spec/factories/mailing_lists/mailing_messages_data/#{mlist}.json").read
      res+= JSON.parse(json_data)
    end
    res
  end

  def test_real_mailing_messages_data_for_assignment mlist
    json_data = File.open("spec/factories/mailing_lists/mailing_messages_data/#{mlist}.json").read
    res = JSON.parse(json_data)
    res.map!{|i| MailingMessagesData.new(i)}
  end

  # IRC Channels Part:

  def test_real_irc_messages_data_for_assignment channel
    json_data = File.open("spec/factories/irc_channels/irc_messages_data/#{channel}.json").read
    res = JSON.parse(json_data)
    res.map!{|i| IrcMessagesData.new(i)}
  end

  def irc_test_messages num
    res = JSON.parse(File.open("spec/factories/irc_channels/test_messages/#{num}.json").read)
    res.each do |item|
      item[3] = item[3].to_time
    end
    return res
  end

  # Facebook Pages Part:

  def stub_facebook
    stub_request(:get, "https://graph.facebook.com/test1/posts?access_token=#{Tokens.facebook_token}&fields=likes.limit(1).summary(1),comments.fields(id).limit(1).summary(1),shares&limit=500").to_return(:status => 200, :body => facebook_pages_response(1), :headers => {})
    stub_request(:get, "https://graph.facebook.com/v1.0/214803165352237/posts?access_token=atoken&fields=likes.limit(1).summary(1),comments.limit(1).summary(1),shares&limit=25&until=123").to_return(:status => 200, :body => facebook_pages_response(1,2), :headers => {})
    stub_request(:get, "https://graph.facebook.com/v1.0/214803165352237/posts?access_token=atoken&fields=likes.limit(1).summary(1),comments.limit(1).summary(1),shares&limit=25&until=120").to_return(:status => 200, :body => facebook_pages_response(1,3), :headers => {})
    stub_request(:get, "https://graph.facebook.com/1_5/likes?access_token=#{Tokens.facebook_token}&limit=100").to_return(:status => 200, :body => facebook_likes_response(1,1), :headers => {})
    stub_request(:get, "https://graph.facebook.com/v1.0/6344818917_10152317455558918/likes?access_token=#{Tokens.facebook_token}&after=NjcxMDE2MDQ2MzE4NTIy&limit=100").to_return(:status => 200, :body => facebook_likes_response(1,2), :headers => {})
    stub_request(:get, "https://graph.facebook.com/1_4/likes?access_token=#{Tokens.facebook_token}&limit=100").to_return(:status => 200, :body => facebook_likes_response(1,3), :headers => {})
    stub_request(:get, "https://graph.facebook.com/1_3/likes?access_token=#{Tokens.facebook_token}&limit=100").to_return(:status => 200, :body => facebook_likes_response(1,4), :headers => {})
    stub_request(:get, "https://graph.facebook.com/1_3/comments?access_token=#{Tokens.facebook_token}&fields=created_time,from&limit=100").to_return(:status => 200, :body => facebook_comments_response(1,1), :headers => {})
    stub_request(:get, "https://graph.facebook.com/v1.0/6344818917_10152317455558918/comments?access_token=#{Tokens.facebook_token}&after=NA==&fields=from&limit=1").to_return(:status => 200, :body => facebook_comments_response(1,2), :headers => {})
  end

  def facebook_pages_response num,page=1
    File.open("spec/factories/facebook_pages/response/#{num}/#{page}.json").read
  end

  def facebook_likes_response num,like
    File.open("spec/factories/facebook_pages/response/#{num}/likes/#{like}.json").read
  end

  def facebook_comments_response num,comment
    File.open("spec/factories/facebook_pages/response/#{num}/comments/#{comment}.json").read
  end

  def test_real_facebook_messages_data type,num
    JSON.parse(File.open("spec/factories/facebook_pages/#{type}_data/#{num}.json").read)
  end

  def test_real_facebook_line_graphs type
    JSON.parse(File.open("spec/factories/facebook_pages/line_graphs/#{type}.json").read)
  end

  def test_real_facebook_pie_charts type
    JSON.parse(File.open("spec/factories/facebook_pages/pie_charts/#{type}.json").read)
  end

  # Twitter Profiles Part:

  def twitter_user_response user
    File.open("spec/factories/twitter_profiles/response/#{user}.json").read
  end

  def twitter_user_timeline_response num
    File.open("spec/factories/twitter_profiles/response/user_timeline/#{num}.json").read
  end

  def twitter_retweets_response num
    File.open("spec/factories/twitter_profiles/response/retweets/#{num}.json").read
  end

  def test_real_tweets_data num
    JSON.parse(File.open("spec/factories/twitter_profiles/tweets_data/#{num}.json").read)
  end

  def test_real_retweeters_data num
    JSON.parse(File.open("spec/factories/twitter_profiles/retweeters_data/#{num}.json").read)
  end

  def test_real_twitter_graph type
    JSON.parse(File.open("spec/factories/twitter_profiles/line_graphs/#{type}.json").read)
  end

  def test_real_twitter_pie_chart type
    JSON.parse(File.open("spec/factories/twitter_profiles/pie_charts/#{type}.json").read)
  end

  # Google Plus Part:

  def gplus_response type,num
    File.open("spec/factories/google_plus/response/#{type}/#{num}.json").read
  end

  def gplus_data type,num
    JSON.parse(File.open("spec/factories/google_plus/#{type}_data/#{num}.json").read)
  end

  def test_gplus_graph graph_type, drawn_type
    JSON.parse(File.open("spec/factories/google_plus/#{graph_type}/#{drawn_type}.json").read)
  end

  # Forums part:

  def test_forum_lists num
    JSON.parse(File.open("spec/factories/forums/lists/#{num}.json").read)
  end

  def test_forums_resolved_response num
    File.open("spec/factories/forums/response/resolved/#{num}.json").read
  end

  def test_forums_threads_response num
    File.open("spec/factories/forums/response/threads/#{num}.json").read
  end

  def test_forums_posts_response num
    File.open("spec/factories/forums/response/posts/#{num}.json").read
  end

  def test_forums_data num
    JSON.parse(File.open("spec/factories/forums/data/#{num}.json").read)
  end

  def test_real_forums_graph type
    JSON.parse(File.open("spec/factories/forums/line_graphs/#{type}.json").read)
  end

  def test_real_forums_chart type
    JSON.parse(File.open("spec/factories/forums/pie_charts/#{type}.json").read)
  end

  # Planetkde Part:

  def test_planet_response num
    File.open("spec/factories/planet/response/#{num}.json").read
  end

  def test_planet_data num
    JSON.parse(File.open("spec/factories/planet/data/#{num}.json").read)
  end

  # Dot Part:

  def test_dot_response num
    File.open("spec/factories/dot_posts/response/#{num}.json").read
  end

  def test_dot_data num
    JSON.parse(File.open("spec/factories/dot_posts/data/#{num}.json").read)
  end

  def test_real_dot_graph type
    JSON.parse(File.open("spec/factories/dot_posts/graphs/#{type}.json").read)
  end

  def test_real_dot_chart type,period
    JSON.parse(File.open("spec/factories/dot_posts/charts/#{type}/#{period}.json").read)
  end

  # Wikis Part:

  def test_wikis_response type, num
    File.read("spec/factories/wikis/response/#{type}/#{num}.json")
  end

  def test_wikis_data type, num, ext = "json"
    File.read("spec/factories/wikis/data/#{type}/#{num}.#{ext}")
  end
  
  # Build Part:
  
  def test_build type
    File.read("spec/factories/build/#{type}.json")    
  end

  # General:

  def test_types
    {"commits" => "commits", "mailing_messages" => "mailing_lists", "irc_messages" => "irc_channels"}
  end

  def test_real_pie_charts_data type
    types = test_types
    res = Hash.new
    Dir.glob("spec/factories/#{types[type]}/pie_charts/*.json") do |file|
      res[File.basename(file,".json")] = File.read(file)
    end
    res
  end

  def test_real_pie_charts_data_all_kde type
    types = test_types
    res = Hash.new
    Dir.glob("spec/factories/#{types[type]}/pie_charts/all/*.json") do |file|
      res[File.basename(file,".json")] = File.read(file)
    end
    res
  end

  def test_real_line_graphs_data type
    types = test_types
    res = Hash.new
    Dir.glob("spec/factories/#{types[type]}/line_graphs/*.json") do |file|
      res[File.basename(file,".json")] = File.read(file)
    end
    res
  end

  def test_real_data_for_assignment type,obj
    types = test_types
    objects = {"commits" => CommitsData, "mailing_messages" => MailingMessagesData, "irc_messages" => IrcMessagesData}
    json_data = File.open("spec/factories/#{types[type]}/#{type}_data/#{obj}.json").read
    res = JSON.parse(json_data)
    res.map!{|i| objects[type].new(i)}
  end

  def test_generated_files_for type
    res = Hash.new
    Dir.glob("spec/factories/#{type}/generated_files/*.json") do |file|
      res[File.basename(file,".json")] = File.read(file)
    end
    res
  end

  def test_correctness_of_generated_files_for path, type, res
    unique = Hash.new
    res.each do |key,val|
      test_data = JSON.parse(File.open("public/en/projects/#{path}/#{type}/#{key}.json").read)
      real_data = JSON.parse(val)
      if real_data.class == Hash
        real_data.each do |k,v|
          test_data[k].should match_array(v) unless k == "colors"
        end
        if real_data["colors"]
          test_data["data"].zip(test_data["colors"]).each do |d,c|
            if unique[d.first]
              c.should eq(unique[d.first])
            else
            unique[d.first] = c
            end
          end
        end
      else
        test_data.should eq(real_data)
      end
    end
  end

  def fill_all_blanks
    page.all(:fillable_field).each { |f| f.set("") }
  end

  # included is an array of items that the side bar should include. Be sure to make it include all items that should be there.
  def test_all_side_bar_items included=[]
    all_items = ["BUGS", "Bugs Report", "Bugs List", "COMMITS", "Commits Report", "Commits List", "Repository", "MAILING LISTS", "Mailing lists Report", "Messages list", "IRC CHANNELS", "IRC Channels Report", "SOCIAL NETWORKS", "Facebook Pages Report", "Twitter Report", "Google+ Report", "FORUMS", "Forums Report", "BLOGS", "Planet Report", "Dot Report"]
    included.each do |item|
      expect(page).to have_content item
    end
    unincluded = all_items - included
    unincluded.each do |item|
      expect(page).not_to have_content item
    end
  end

  def test_reply_data type, num
    types = {
      "mailing_messages" => "mailing_lists/mailing_messages_reply",
      "irc_messages" => "irc_channels/irc_messages_reply",
      "forum_posts" => "forums/forum_posts_reply"
    }
    JSON.parse(File.open("spec/factories/#{types[type]}/#{num}.json").read)
  end

  # Tests equality of data in json.
  def test_json_equality first,second
    JSON.parse(first)["data"].should match_array(JSON.parse(second)["data"])
  end
end