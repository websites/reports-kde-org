source 'https://rubygems.org'

gem 'rails', '~> 4.2.3'

# For MongoDB.
gem 'mongoid', '~> 4.0.2'
gem 'bson_ext'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

gem 'bootstrap-generators', '~> 2.3'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', :platforms => :ruby

gem 'devise_ldap_authenticatable', :github => "Alwahsh/devise_ldap_authenticatable"

#gem "cucumber-rails", "~> 1.3.1", :require => "false", :group => [:test]

gem 'web-console', '~> 2.1.2', :group => :development

group :development, :test do
  gem "rspec-rails", "~> 3.4.2"
  gem "factory_girl_rails", "~> 4.6.0"
  gem 'logstasher', '~> 0.6.2'
  gem 'byebug'
end

group :test do
  gem "capybara", "~> 2.4.4"
  gem "guard-rspec", "~> 4.6.4"
  gem "database_cleaner", "~> 1.4.0"
  gem "launchy", "~> 2.4.3"
  gem 'webmock', '~> 1.20.4'
  gem 'sinatra', '~> 1.4.5'
  gem "fakefs", '~> 0.5.2', :require => "fakefs/safe"
  gem 'timecop', '~> 0.7.3'
  gem 'ladle', '~> 1.0.0'
  gem 'simplecov', '~> 0.9.2'
end

group :production do
  gem "daemons", "~> 1.1.9"
  gem "memcache-client", "~> 1.8.5"
end

gem 'jquery-rails'

# For settings like API Keys.
gem 'settingslogic', '~> 2.0.9'

# For friendly URLs.
#gem "friendly_id", "~> 4.0.10.1"
gem 'mongoid-slug', '~> 4.0.0'
# To use ActiveModel has_secure_password.
gem 'bcrypt-ruby', '~> 3.0.0'
# To be able to have projects and sub projects.
#gem "acts_as_tree", "~> 1.3.0"
gem 'mongoid-tree', '~> 2.0.0'
# For authentication.
gem 'devise', '~> 3.4.1'
# For adding roles.
gem "rolify", "~> 3.2.0"
# For Authorization.
gem 'cancancan', '~> 1.8'
# For XML Parsing.
gem "nokogiri", "~> 1.6.1"
# For pagination.
gem "will_paginate", "~> 3.0.4"
gem "will_paginate-bootstrap", "~> 0.2.3"
gem 'will_paginate_mongoid', '~> 2.0.1'
# For thin.
gem 'thin'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
gem 'nprogress-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# For handling background jobs.
gem 'delayed_job', '~> 4.0.2'
gem 'delayed_job_mongoid', '~> 2.1.0'

# For git report:
gem "git", :github => "Alwahsh/ruby-git"

# For Syntax Highlighting.
gem "charlock_holmes_bundle_icu", "~> 0.6.9.2"
gem "github-linguist", :github => "Alwahsh/linguist"

# For mailing lists report:
gem "mbox", :github => "Alwahsh/ruby-mbox"
gem "mail", "~> 2.5.4"

# For WYSIWYG editor:
gem "ckeditor", "~> 4.0.4"

# For cron jobs:
gem 'whenever', :require => false

# For IRC Bot: These are just added to make sure that the server has these gems.
gem "cinch", "~> 2.0.5"
gem "rufus-scheduler", "~> 2.0.24"

# For Facebook statistics generation.
gem 'koala', '~> 1.10.0'

# For Twitter statistics generation.
gem 'twitter', '~> 5.11.0'
gem 'tweetstream', '~> 2.6.1'

# For Google+ statistics generation.
gem 'google_plus', :github => "Alwahsh/google_plus", :branch => "MyVersion"

# For Mediawiki statistics generation.
gem 'mediawiki_api', '~> 0.4.1'

# For openning links.
gem 'curb', '~> 0.8.6'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'

