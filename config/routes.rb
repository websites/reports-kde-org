Reports::Application.routes.draw do

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      get '/bug/:bug_id', to: 'projects#bug_info'
      get '/commit', to: 'projects#commit_info'
      get '/shortlog', to: 'projects#shortlog'
      resources :projects do
        get 'bugs_report', :on => :member
        get 'bugs_list', :on => :member
        get 'commits_report', :on => :member
        get 'commits_list', :on => :member
        get 'mailing_lists_report', :on => :member
        get 'messages_list', :on => :member
        get 'messages_of_month', :on => :member
        get 'message', :on => :member
        get 'project_report', :on => :member
        get 'irc_channels_report', :on => :member
      end
    end
  end


  # config/routes.rb
  scope "(:locale)" do
    get "admin/index"

    get "admin/", to: 'admin#index'

    get "admin/control_user"

    get '/bug/:bug_id', to: 'projects#bug_info'

    get '/commit', to: 'projects#commit_info'

    get '/shortlog', to: 'projects#shortlog'

    devise_for :users

    resources :custom_reports do
      Constants::TYPES_TO_REPORT_TITLES.values.each do |name|
        get "#{name}_report", :on => :member
      end
    end

    get 'kde/', to: 'kde#index', as: :kde
    get 'kde/commits_report', to: 'kde#commits_report', as: :kde_commits_report
    get 'kde/mailing_lists_report', to: 'kde#mailing_lists_report', as: :kde_mailing_lists_report
    get 'kde/irc_channels_report', to: 'kde#irc_channels_report', as: :kde_irc_channels_report

    resources :projects do
      get 'bugs_report', :on => :member
      get 'bugs_list', :on => :member
      get 'bug/:bug_id', :on => :member, :as => :bug, to: 'projects#bug_info'
      get 'repository/:product/:branch/:directory', :on => :member, to: 'projects#repository',as: :repo_path , product: /.+\.git/, directory: /.+/, branch: /[^\/]+/
      get 'repository/:product(/:branch)', :on => :member, to: 'projects#repository',as: :full_repository, product: /.+\.git/, branch: /[^\/]+/
      get 'repository', :on => :member
      get 'blob/:product/:sha/:blob', :on => :member, to: 'projects#show_blob', as: :blob , product: /.+\.git/, blob: /.+/
      get 'commits_report', :on => :member
      get 'commits_per_day', :on => :member
      get 'review_requests_report', :on => :member
      #get 'download/:format/:product:sha', :on => :member, to: 'projects#download_snapshot', as: :download_snapshot, product: /.+\.git/, format: /[^\/]+/
      get 'commits_list/:product', :on => :member , to: 'projects#commits_list', product: /.+\.git/
      get 'commits_list', :on => :member
      get 'commit/:product:commit_sha', to: 'projects#commit_info', :on => :member , as: :commit, product: /.+\.git/
      get 'product/:product/branch/*branch', to: 'projects#branch', on: :member, as: :branch, product: /.+\.git/, branch: /.+/
      get 'mailing_lists_report', :on => :member
      get 'mailing_messages_per_day', :on => :member
      get 'messages_list', :on => :member
      get 'messages_list/:mlist/:month/:index', on: :member, to: 'projects#message', as: :message, mlist: /.+\.mbox/
      get 'messages_list/:mlist/:month', on: :member, to: 'projects#messages_of_month', as: :messages_of_month, mlist: /.+\.mbox/
      get 'messages_list/:mlist', :on => :member , to: 'projects#messages_list', as: :messages_list_mbox, mlist: /.+\.mbox/
      #get 'messages_of_month', :on => :member
      get 'project_report', :on => :member
      get 'irc_channels_report', :on => :member
      get 'irc_messages_per_day', :on => :member
      get 'facebook_pages_report', :on => :member
      get 'twitter_report', :on => :member
      get 'google_plus_report', :on => :member
      get 'forums_report', :on => :member
      get 'forums_topics_list', :on => :member
      get 'planet_report', :on => :member
      get 'dot_report', :on => :member
      get 'wiki_report', :on => :member
      get 'builds_report', :on => :member
    end

    get "home/index"

    resources :news

    match '/:locale' => 'home#index', via: [:get, :post]
    root :to => 'home#index'
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products
  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
