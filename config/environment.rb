# Load the rails application
require File.expand_path('../application', __FILE__)

# Make encoding UTF-8
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

# Initialize the rails application
Reports::Application.initialize!
ActionMailer::Base.default :charset => 'utf-8'