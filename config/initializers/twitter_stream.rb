TweetStream.configure do |config|
  config.consumer_key       = Tokens.twitter.consumer_key
  config.consumer_secret    = Tokens.twitter.consumer_secret
  config.oauth_token        = Tokens.twitter.oauth_token
  config.oauth_token_secret = Tokens.twitter.oauth_token_secret
  config.auth_method        = :oauth
end