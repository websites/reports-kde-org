module Constants

  # Constant starting dates for some reports.
  DOT_START_DATE = Time.utc(2000,9,2)
  PLANET_START_DATE = Time.utc(2014,8,14)

  CUSTOM_REPORT_TYPES = [
    "commits",
    "mailing_messages",
    "irc_messages",
    "review_requests",
    "forum_posts",
    "build",
    "planet_posts",
    "dot_posts"
  ]
  WikiDataManager.explorable_sites.each do |site|
    CUSTOM_REPORT_TYPES << "#{site}_wiki"
  end
  
  TYPES_TO_PRODUCT_FIELDS = {
    "forum_posts" => "forum_ids",
    "build" => "build_jobs",
  }
  WikiDataManager.explorable_sites.each do |site|
    TYPES_TO_PRODUCT_FIELDS["#{site}_wiki"] = WikiDataManager.product_field_for(site)
  end
  
  TYPES_TO_DATA_MANAGERS = {
    "commits" => [CommitsDataManager],
    "mailing_messages" => [MailingListsDataManager],
    "irc_messages" => [IrcDataManager],
    "forum_posts" => [ForumDataManager],
    "review_requests" => [ReviewboardDataManager],
    "build" => [BuildDataManager],
    "planet_posts" => [PlanetDataManager],
    "dot_posts" => [DotDataManager]
  }
  WikiDataManager.explorable_sites.each do |site|
    TYPES_TO_DATA_MANAGERS["#{site}_wiki"] = [WikiDataManager, site]
  end
  
  TYPES_TO_REPORT_TITLES = {
    "commits" => "commits",
    "mailing_messages" => "mailing_lists",
    "irc_messages" => "irc_channels",
    "forum_posts" => "forums",
    "review_requests" => "review_requests",
    "build" => "builds",
    "planet_posts" => "planet",
    "dot_posts" => "dot"
  }
  WikiDataManager.explorable_sites.each do |site|
    TYPES_TO_REPORT_TITLES["#{site}_wiki"] = "wiki"
  end
end