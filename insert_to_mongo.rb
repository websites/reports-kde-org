module Delayed
  class PerformableMethod
    def initialize(object,method_name,args = nil)
      @object = object
      @method_name = method_name.to_sym
      @args = args if args
    end
  end
end

require 'mongo'
require 'yaml'
include Mongo

server = "localhost"
port = 27017
db_name = "reports_development"
collection_name = "delayed_backend_mongoid_jobs"


def insertion object,method,args = nil
  handler = Delayed::PerformableMethod.new(object,method,args)
  {
    priority: 0,
    attempts: 0,
    handler: handler.to_yaml,
    run_at: Time.now,
    locked_at: nil,
    locked_by: nil,
    failed_at: nil,
    last_error: nil,
    queue: nil,
    created_at: Time.now,
    updated_at: Time.now
  }
end

db = MongoClient.new(server,port).db(db_name)
$coll = db.collection collection_name