require 'cinch'

irc_pid = "tmp/pids/irc_bot.pid"

if (File.exist?(irc_pid))
  system "kill #{File.read(irc_pid)}"
end

File.open(irc_pid,'w') do |f|
  f << Process.pid
end

channels = $stdin.read.split(",")


class Project
end

require_relative 'insert_to_mongo'

bot = Cinch::Bot.new do
  configure do |c|
    c.server = "irc.freenode.org"
    c.channels = channels
    c.nick = "reports_bot_2"
  end

  on :message do |m|
    $coll.insert insertion(Project,"irc_message_update",[m.channel.to_s,m.user.nick.to_s,m.message,m.time])
  end
end

bot.start
