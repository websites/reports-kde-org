class CustomReportsController < ApplicationController
  # Add authorization using cancan.
  load_and_authorize_resource
  before_filter :set_report, except: [
    :new,
    :create,
    :index
  ]

  def new
    @custom_report = CustomReport.new
    @projects = Project.all
    @types = Constants::CUSTOM_REPORT_TYPES
  end

  def create
    @custom_report = CustomReport.new(custom_report_params)
    @custom_report.user = current_user
    respond_to do |format|
      if @custom_report.save
        format.html { redirect_to @custom_report, notice: 'Your report is queued. Please allow some time for it to be generated and refresh the page.'}
        format.json { render json: @custom_report, status: :created, location: @custom_report }
      else
        format.html { render action: "new" }
        format.json { render json: @custom_report.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    @custom_reports = CustomReport.all
  end

  def show
  end
  
  def destroy
    @report.destroy

    respond_to do |format|
      format.html { redirect_to custom_reports_path }
      format.json { head :no_content }
    end
  end

  #-------------------------------------------------------------------------------------------------------------

  def commits_report
    @start_date = ReportHelpers.pointStart_date @report.commits_start_date
    @path = @report.display_path_for "commits"
    @title = "commits_report"
    
    respond_to do |format|
      format.html {render "report_layout"}
    end
  end
  
  def mailing_lists_report
    @start_date = ReportHelpers.pointStart_date @report.mailing_messages_start_date
    @path = @report.display_path_for "mailing_messages"
    @title = "mailing_lists_report"
    
    respond_to do |format|
      format.html {render "report_layout"}
    end
  end
  
  def irc_channels_report
    @start_date = ReportHelpers.pointStart_date @report.irc_messages_start_date
    @path = @report.display_path_for "irc_messages"
    @title = "irc_channels_report"
    
    respond_to do |format|
      format.html {render "report_layout"}
    end
  end

  def forums_report
    @start_date = ReportHelpers.pointStart_date @report.forum_posts_start_date
    @path = @report.display_path_for "forum_posts"
    @title = "forums_report"

    respond_to do |format|
      format.html {render "report_layout"}
    end
  end

  def review_requests_report
    @start_date = ReportHelpers.pointStart_date @report.review_requests_start_date
    @path = @report.display_path_for "review_requests"
    @title = "review_requests_report"

    respond_to do |format|
      format.html {render "report_layout"}
    end
  end

  def builds_report
    @start_date = ReportHelpers.pointStart_date @report.build_start_date
    @drawables = BuildDataManager.drawables_for @report
    @path = @report.display_path_for "build"
    @title = "builds_report"

    respond_to do |format|
      format.html {render "report_layout"}
    end
  end

  def wiki_report
    @drawable_wikis = WikiDataManager.drawable_wikis_for @report
    @start_date = WikiDataManager.start_dates_for @report, @drawable_wikis
    @total_views = WikiDataManager.total_views_for @report, @drawable_wikis
    @total_revisions = WikiDataManager.total_revisions_for @report, @drawable_wikis
    @path = WikiDataManager.path_for @report, @drawable_wikis
    @title = "wiki_report"

    respond_to do |format|
      format.html {render "report_layout"}
    end
  end

  def planet_report
    @start_date = ReportHelpers.pointStart_date @report.planet_posts_start_date
    @path = @report.display_path_for "planet_posts"
    @title = "planet_report"

    respond_to do |format|
      format.html {render "report_layout"}
    end
  end
  
  def dot_report
    @start_date = ReportHelpers.pointStart_date @report.dot_posts_start_date
    @path = @report.display_path_for "dot_posts"
    @title = "dot_report"

    respond_to do |format|
      format.html {render "report_layout"}
    end
  end

  private

    def set_report
      @report = CustomReport.find(params[:id])
    end

    def custom_report_params
      permit = CustomReport.get_permitted_attributes
      params.require(:custom_report).permit(*permit)
    end
end
