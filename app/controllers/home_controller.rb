class HomeController < ApplicationController
  def index
    @news = PlanetPostsData.get_latest_posts(10)
    @projects = Project.desc(:id).limit(5)
  end
end
