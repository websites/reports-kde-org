def get_array_from_text unparsed
  return unparsed.split("\n").map{|s| s.chomp}
end

class ProjectsController < ApplicationController

  # Add authorization using cancan.
  load_and_authorize_resource

  before_filter :set_project, except: [
    :index,
    :new,
    :create,
    :message,
    :download_snapshot,
    :show_blob,
    :shortlog,
    :branch,
    :commit_info,
    :bug_info
  ]


  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.roots
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :inline => @projects.to_json(:only => [:id,:title,:description]), :layout => "json_layout.json.erb" }
      format.xml { render :inline => @projects.to_xml(:skip_instruct => true, :only => [:id,:title,:description]), :layout => "xml_layout.xml.erb" }
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @projects = @project.children
    respond_to do |format|
      if @projects.empty? || !@project.large_description.blank?
        # Requesting a project that has no sub projects gives its info.
        format.html # show.html.erb
        format.json { render  :inline => @project.to_json(:only => [:id,:title,:large_description]) , :layout => "json_layout.json.erb" }
        format.xml { render :inline => @project.to_xml(:skip_instruct => true, :only => [:id,:title,:large_description]) , :layout => "xml_layout.xml.erb" }
      else
        # Requesting a project that has sub projects gives a list of all of its sub projects.
        format.html { render "index" } # index.html.erb
        format.json { render :inline => @projects.to_json(:only => [:id,:title,:description]) , :layout => "json_layout.json.erb" }
        format.xml { render :inline => @projects.to_xml(:skip_instruct => true, :only => [:id,:title,:description]) , :layout => "xml_layout.xml.erb" }
      end
    end
  end

  # GET /projects/new
  # GET /projects/new.json
  def new
    @project = Project.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @project }
    end
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    params[:project][:bugs_report_fields] ||= []
    params[:project][:bugzilla_products].delete("")
    params[:project][:build_jobs].delete("")
    @project = Project.new(project_params)
    @project.users << current_user # if current_user # Uncomment this to allow project creation from non-users.
    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render json: @project, status: :created, location: @project }
      else
        format.html { render action: "new" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /projects/1
  # PUT /projects/1.json
  def update
    params[:project][:bugs_report_fields] ||= []
    params[:project][:bugzilla_products].delete("")

    respond_to do |format|
      if @project.update_attributes(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy

    respond_to do |format|
      format.html { redirect_to projects_url }
      format.json { head :no_content }
    end
  end

  # GET /projects/1/project_report
  def project_report
    @data = @project.project_report
    @bugs = @data["bugs"]["latest_bugs"] if @data["bugs"]
    @products_and_commits = @data["commits"]["latest_commits"] if @data["commits"]
    respond_to do |format|
      format.html # /projects/1/project_report
      format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # Wikis:

  def wiki_report
    @start_date = @project.wiki_start_date.to_time.to_i * 1000
    @drawable_wikis = @project.drawable_wikis
    @total_views = @project.total_wiki_views
    @total_revisions = @project.total_wiki_revisions

    respond_to do |format|
      format.html #community_wiki_report.html.erb
    end
  end

  # Builds:

  def builds_report
    @start_date = @project.build_start_date.to_time.to_i * 1000
    @drawables = BuildDataManager.drawables_for @project
    @path = @project.display_path_for "build"
    respond_to do |format|
      format.html #community_wiki_report.html.erb
    end
  end

  # Dot:

  def dot_report
    @start_date = Constants::DOT_START_DATE.to_i * 1000

    respond_to do |format|
      format.html #dot_report.html.erb
      #format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # Planet:

  def planet_report
    @start_date = Constants::PLANET_START_DATE.to_i * 1000

    respond_to do |format|
      format.html #planet_report.html.erb
      #format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # Forums:

  def forums_report
    @start_date = @project.forum_posts_start_date.to_time.to_i * 1000

    respond_to do |format|
      format.html #forums_report.html.erb
      #format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  def forums_topics_list
    @number_of_topics = Array.new(@project.number_of_forums_topics,nil)
    @number_of_pages = @number_of_topics.paginate(:page => params[:page] ,:per_page => params[:view] ||= 10 )
    @posts = @project.forum_topics(params[:page] ||= 1,params[:view].to_i ||= 10)

    respond_to do |format|
      format.html #forums_topics_list.html.erb
      format.json { render :inline => @posts.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # Google Plus:

  def google_plus_report
    @start_date = @project.google_plus_posts_start_date.to_time.to_i * 1000
    @show_pie_charts = GeneralObjects.show_social_media_pie_charts?

    respond_to do |format|
      format.html #forums_report.html.erb
      #format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # Twitter Profiles:

  def twitter_report
    @start_date = @project.twitter_tweets_start_date.to_time.to_i * 1000
    @show_pie_charts = GeneralObjects.show_social_media_pie_charts?

    respond_to do |format|
      format.html #twitter_report.html.erb
      #format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # Facebook Pages:

  # GET /projects/1/facebook_pages_report
  def facebook_pages_report
    @start_date = @project.facebook_posts_start_date.to_time.to_i * 1000
    @show_pie_charts = GeneralObjects.show_social_media_pie_charts?

    respond_to do |format|
      format.html #facebook_pages_report.html.erb
      #format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # IRC Channels:

  # GET /projects/1/irc_channels_report
  # GET /projects/1/irc_channels_report.json
  def irc_channels_report
    @start_date = @project.irc_messages_start_date.to_time.to_i * 1000

    respond_to do |format|
      format.html #irc_channels_report.html.erb
      #format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      #format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # START AJAX IRC CHANNELS REPORT
  def irc_messages_per_day
    @data = @project.get_objects_line_chart("irc_messages")
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def irc_messages_senders_per_day
    @data = @project.get_daily_doers_line_chart("irc_messages")
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def irc_messages_senders_per_month
    raw_data = project.get_monthly_doers_line_chart("irc_messages")
    first_date = project.irc_messages_start_date
    @data = Project.convert_raw_data_to_monthly_data raw_data, first_date
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def irc_messages_senders_per_year
    raw_data = project.get_yearly_doers_line_chart("irc_messages")
    first_date = project.irc_messages_start_date
    @data = Project.convert_raw_data_to_yearly_data raw_data, first_date
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  # END AJAX IRC CHANNELS REPORT

  # Mailing Lists:

  # GET /projects/1/mailing_lists_report
  # GET /projects/1/mailing_lists_report.json
  def mailing_lists_report
    @start_date = @project.mailing_messages_start_date.to_time.to_i * 1000

    respond_to do |format|
      format.html #mailing_lists_report.html.erb
      format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end


  # START AJAX MAILING LISTS REPORT
  def mailing_messages_per_day
    @data = @project.get_objects_line_chart("mailing_messages")
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def mailing_messages_senders_per_day
    @data = @project.get_daily_doers_line_chart("mailing_messages")
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def mailing_messages_senders_per_month
    raw_data = project.get_monthly_doers_line_chart("mailing_messages")
    first_date = project.mailing_messages_start_date
    @data = Project.convert_raw_data_to_monthly_data raw_data, first_date
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def mailing_messages_senders_per_year
    raw_data = project.get_yearly_doers_line_chart("mailing_messages")
    first_date = project.mailing_messages_start_date
    @data = Project.convert_raw_data_to_yearly_data raw_data, first_date
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  # END AJAX MAILING LISTS REPORT


  # GET /projects/1/messages_list
  # GET /projects/1/messages_list.json
  def messages_list
    @lists = params[:mlist] ||= @project.get_all_lists
    if @lists.class == String
      @lists = [@lists]
    end
    if @lists.length == 1
      @mlist = @lists[0]
      @list, @starting_month = @project.get_messages_list_index @mlist
      respond_to do |format|
        format.html #messages_list.html.erb
        format.json { render :file => "projects/json/messages_list.json.erb", :layout => "json_layout.json.erb" }
        format.xml { render :file => "projects/xml/messages_list.xml.erb", :layout => "xml_layout.xml.erb" }
      end
    else
      respond_to do |format|
        format.html { render "choose_list" }
        format.json { render :file => "projects/json/choose_list.json.erb", :layout => "json_layout.json.erb" }
        format.xml { render :file => "projects/xml/choose_list.xml.erb", :layout => "xml_layout.xml.erb" }
      end
    end
  end

  # GET /1/messages_of_month?mlist=xxx.mbox&month=Jul2013
  # GET /1/messages_of_month.json?mlist=xxx.mbox&month=Jul2013
  def messages_of_month
    @temp = Project.get_messages_list_by_subject params[:mlist],Date.strptime(params[:month],"%b%Y")
    if @temp.length == 0
      redirect_to messages_list_mbox_project_path , alert: t("no_messages_this_month") and return
    end
    @messages = @temp[0]
    @message_ids = @temp[1]
    respond_to do |format|
      format.html #messages_of_month.html.erb
      format.json { render :file => "projects/json/messages_of_month.json.erb", :layout => "json_layout.json.erb" }
      format.xml { render :file => "projects/xml/messages_of_month.xml.erb", :layout => "xml_layout.xml.erb" }
    end
  end

  # GET /1/message?mlist=xxx.mbox&index=1
  # GET /1/message.json?mlist=xxx.mbox&index=1
  def message
    @message = Project.get_message(params[:mlist],Date.strptime(params[:month],"%b%Y"),params[:index])
    @number_of_messages = Project.number_of_messages(params[:mlist],params[:month])
    respond_to do |format|
      format.html #message.html.erb
      format.json { render :file => "projects/json/message.json.erb", :layout => "json_layout.json.erb" }
      format.xml { render :file => "projects/xml/message.xml.erb", :layout => "xml_layout.xml.erb" }
    end
  end


  # Review Requests:

  # GET /projects/1/review_requests_report
  # GET /projects/1/review_requests_report.json
  def review_requests_report
    @start_date = @project.review_requests_start_date.to_time.to_i * 1000
    @average_close_time = ReviewRequestsData.get_total_average_close_time(@project).round(3).to_s

    respond_to do |format|
      format.html #review_requests_report.html.erb
      format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # Commits:

  # GET /projects/1/repository
  # GET /projects/1/repository.json
  def repository
    if (params[:product] || get_array_from_text(@project.git_repos).length==1)
      if params[:product]
        @product = params[:product]
      else
        @product = @project.git_repos.chomp
      end
      @directory = params[:directory]
      @branch = params[:branch] ||= "master"
      @files = Project.list_all_files @product, @branch, @directory
      if @files
        respond_to do |format|
          format.html #repository.html.erb
          format.json { render :inline => @files.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @files.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
        end
      else
        respond_to do |format|
          @branches = Project.list_branches @product
          format.html do
            flash.now[:error] = @project.title + " " + t('doesnt_have_a_branch_called') + " " + @branch + ". " + t('choose_another_one') + "."
            render 'choose_branch'
          end
          format.json { render :inline => @branches.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @branches.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
        end
      end
    else
      @products = get_array_from_text(@project.git_repos)
      respond_to do |format|
        format.html { render 'choose_product' }
        format.json { render :file => "projects/json/choose_product.json.erb", :layout => "json_layout.json.erb" }
        format.xml { render :file => "projects/xml/choose_product.xml.erb", :layout => "xml_layout.xml.erb" }
      end
    end
  end


  def download_snapshot
    send_file Project.download_snapshot(params[:sha],params[:format], params[:product]), filename: params[:product][0..-4] + params[:format]
  end

  def show_blob
    @product = params[:product]
    @blob = params[:blob]
    @commit_sha = params[:sha]
    @content = Project.show_blob_content @product, @blob, @commit_sha
    respond_to do |format|
      format.html #show_blob.html.erb
      format.json { render :inline => @content.to_json, :layout => "json_layout.json.erb" }
      format.xml { render :inline => @content.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # START AJAX COMMITS REPORT
  def commits_per_day
    @data = @project.get_objects_line_chart("commits")
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def commits_authors_per_day
    @data = @project.get_daily_doers_line_chart("commits")
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def commits_authors_per_month
    raw_data = project.get_monthly_doers_line_chart("commits")
    first_date = project.commits_start_date
    @data = Project.convert_raw_data_to_monthly_data raw_data, first_date
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  def commits_authors_per_year
    raw_data = project.get_yearly_doers_line_chart("commits")
    first_date = project.commits_start_date
    @data = Project.convert_raw_data_to_yearly_data raw_data, first_date
    respond_to do |format|
      format.json { render :inline => @data.to_json }
    end
  end

  # END AJAX COMMITS REPORT

  # GET /projects/1/commits_report
  # GET /projects/1/commits_report.json
  def commits_report
    @start_date = @project.commits_start_date.to_time.to_i * 1000

    respond_to do |format|
      format.html #commits_report.html.erb
      format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # GET /projects/1/commits_list
  # GET /projects/1/commits_list.json
  def commits_list
    if (params[:product] || get_array_from_text(@project.git_repos).length==1)
      if params[:product]
        @product = params[:product]
      else
        @product = @project.git_repos.chomp
      end
      @number_of_commits = Array.new(@project.number_of_commits,nil)
      @number_of_pages = @number_of_commits.paginate(:page => params[:page] ||= 1 ,:per_page => params[:view] ||= 10 )
      @commits = @project.product_list_of_commits(@product, params[:page].to_i , params[:view].to_i ||= 10)
      @branches = Project.repo(@product).branches
      respond_to do |format|
        format.html # commits_list.html.erb
        format.json { render :file => "projects/json/commits_list.json.erb", :layout => "json_layout.json.erb" }
        format.xml { render :file => "projects/xml/commits_list.xml.erb", :layout => "xml_layout.xml.erb" }
      end
    else
      @products = get_array_from_text(@project.git_repos)
      respond_to do |format|
        format.html { render 'choose_product' }
        format.json { render :file => "projects/json/choose_product.json.erb", :layout => "json_layout.json.erb" }
        format.xml { render :file => "projects/xml/choose_product.xml.erb", :layout => "xml_layout.xml.erb" }
      end
    end
  end

  # GET /shortlog/1
  # GET /shortlog/1.json
  def shortlog
    @product = params[:product]
    @commit_sha = params[:commit_sha]
    @g = Project.repo(@product)
    @number_of_commits = Array.new(Project.branch_number_of_commits(@product,@commit_sha),nil)
    @number_of_pages = @number_of_commits.paginate(:page => params[:page] ||= 1, :per_page => params[:view] ||= 10 )
    @commits = @g.gcommit(@commit_sha).log(params[:view].to_i ||= 10).skip(params[:view].to_i*(params[:page].to_i-1)).to_a
    @branches = @g.branches
    respond_to do |format|
      format.html # shortlog.html.erb
      format.json { render :file => "projects/json/shortlog.json.erb", :layout => "json_layout.json.erb" }
      format.xml { render :file => "projects/xml/shortlog.xml.erb", :layout => "xml_layout.xml.erb" }
    end
  end

  # GET /product/calligra.git/branch/master
  # GET /product/calligra.git/branch/master.json
  def branch
    @product = params[:product]
    @branch = params[:branch]
    @g = Project.repo(@product)
    @number_of_commits = Array.new(@g.number_of_commits(@branch),nil)
    @number_of_pages = @number_of_commits.paginate(:page => params[:page] ||= 1, :per_page => params[:view] ||= 10 )
    @commits = @g.branch(@branch).gcommit.log(params[:view].to_i ||= 10).skip(params[:view].to_i*(params[:page].to_i-1)).to_a
    @branches = @g.branches
    respond_to do |format|
      format.html { render 'shortlog' } # shortlog.html.erb
      format.json { render :file => "projects/json/shortlog.json.erb", :layout => "json_layout.json.erb" }
      format.xml { render :file => "projects/xml/shortlog.xml.erb", :layout => "xml_layout.xml.erb" }
    end
  end

  # GET /commit?product=product&commit_sha=commit_sha
  # GET /commit.json?product=product&commit_sha=commit_sha
  def commit_info
    g = Project.repo(params[:product])
    @commit = g.gcommit(params[:commit_sha])
    @product = params[:product]
    @project = Project.find(params[:id])

    respond_to do |format|
      format.html #commit_info.html.erb
      format.json { render :file => "projects/json/commit_info.json.erb", :layout => "json_layout.json.erb" }
      format.xml { render :file => "projects/xml/commit_info.xml.erb", :layout => "xml_layout.xml.erb" }
    end
  end

  # --------------------------------------------------------------------------------------------------------------------------

  # Bugs:

  # GET /projects/1/bugs_report
  # GET /projects/1/bugs_report.json
  def bugs_report
    @start_date = @project.bugs_data["start_date"].to_time.to_i * 1000

    respond_to do |format|
      format.html #bugs_report.html.erb
      format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
      format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end

  # GET /projects/1/bugs_list
  # GET /projects/1/bugs_list.json
  def bugs_list
    @number_of_bugs = Array.new(@project.number_of_bugs,nil)
    @number_of_pages = @number_of_bugs.paginate(:page => params[:page] ,:per_page => params[:view] ||= 10 )
    @bugs = @project.list_of_bugs(params[:page] ||= 1,params[:view].to_i ||= 10)
    respond_to do |format|
      format.html # bugs_list.html.erb
      format.json { render :inline => @bugs.to_json, :layout => "json_layout.json.erb" }
      format.xml { render :inline => @bugs.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
    end
  end


  # GET /bug/1
  # GET /bug/1.json
  def bug_info
    @bug_info = Project.fetch_bug(params[:bug_id])
    @comments = Project.fetch_comments(params[:bug_id])
    respond_to do |format|
      format.html # bug_info.html.erb
      format.json { render :file => "projects/json/bug_info.json.erb", :layout => "json_layout.json.erb" }
      format.xml { render :file => "projects/xml/bug_info.xml.erb", :layout => "xml_layout.xml.erb" }
    end
  end

  private

    def project_params
      params.require(:project).permit(:parent_id, :title, :description, :community_wiki_description_page, :large_description,:git_repos, :mailing_lists, :irc_channels, :facebook_pages, :twitter_profile, :google_plus_profiles, :forum_ids, :techbase_wiki_pages, :community_wiki_pages, :userbase_wiki_pages, :bugzilla_products => [], :bugs_report_fields => [], :build_jobs => [])
    end

    def set_project
      @project = Project.find(params[:id])
    end

end
