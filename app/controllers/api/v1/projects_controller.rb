module Api
  module V1
    class ProjectsController < ApplicationController
      
      # Add authorization using cancan.
      load_and_authorize_resource
      # GET /projects
      # GET /projects.json
      def index
        @projects = Project.roots
    
        respond_to do |format|
          format.json { render :inline => @projects.to_json(:only => [:id,:title,:description]), :layout => "json_layout.json.erb" }
          format.xml { render :inline => @projects.to_xml(:skip_instruct => true, :only => [:id,:title,:description]), :layout => "xml_layout.xml.erb" }
        end
      end
    
      # GET /projects/1
      # GET /projects/1.json
      def show
        @project = Project.find_by_id(params[:id])
        @projects = @project.children
        respond_to do |format|
          if @projects.empty?
            # Requesting a project that has no sub projects gives its info.
            format.json { render  :inline => @project.to_json(:only => [:id,:title,:large_description]) , :layout => "json_layout.json.erb" } 
            format.xml { render :inline => @project.to_xml(:skip_instruct => true, :only => [:id,:title,:large_description]) , :layout => "xml_layout.xml.erb" }
          else 
            format.json { render :inline => @projects.to_json(:only => [:id,:title,:description]) , :layout => "json_layout.json.erb" }
            format.xml { render :inline => @projects.to_xml(:skip_instruct => true, :only => [:id,:title,:description]) , :layout => "xml_layout.xml.erb" }
          end
        end
      end
    
      # GET /projects/new
      # GET /projects/new.json
      def new
        @project = Project.new
    
        respond_to do |format|
          format.html # new.html.erb
          format.json { render json: @project }
        end
      end
    
      # GET /projects/1/edit
      def edit
        @project = Project.find(params[:id])
      end
    
      # POST /projects
      # POST /projects.json
      def create
        params[:project][:bugs_report_fields] ||= []  
        @project = Project.new(params[:project])
        @project.users << current_user
        respond_to do |format|
          if @project.save
            format.html { redirect_to @project, notice: 'Project was successfully created.' }
            format.json { render json: @project, status: :created, location: @project }
          else
            format.html { render action: "new" }
            format.json { render json: @project.errors, status: :unprocessable_entity }
          end
        end
      end
    
      # PUT /projects/1
      # PUT /projects/1.json
      def update
        params[:project][:bugs_report_fields] ||= []  
        @project = Project.find(params[:id])
    
        respond_to do |format|
          if @project.update_attributes(params[:project])
            format.html { redirect_to @project, notice: 'Project was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @project.errors, status: :unprocessable_entity }
          end
        end
      end
    
      # DELETE /projects/1
      # DELETE /projects/1.json
      def destroy
        @project = Project.find(params[:id])
        @project.destroy
    
        respond_to do |format|
          format.html { redirect_to projects_url }
          format.json { head :no_content }
        end
      end
      
      # GET /projects/1/project_report
      def project_report
        @project = Project.find(params[:id])
        @data = @project.project_report
        respond_to do |format|
          format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
        end
      end
      
      
      # IRC Channels:
  
      # GET /projects/1/irc_channels_report
      # GET /projects/1/irc_channels_report.json
      def irc_channels_report
        @project = Project.find(params[:id])
        @data = @project.get_irc_report
                
        respond_to do |format|
          format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
        end
      end
      
      # Mailing Lists:
      
      # GET /projects/1/mailing_lists_report
      # GET /projects/1/mailing_lists_report.json
      def mailing_lists_report
        @project = Project.find(params[:id])
        @data = @project.get_lists_report
        
        respond_to do |format|
          format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" }
        end
      end
      
      # GET /projects/1/messages_list
      # GET /projects/1/messages_list.json
      def messages_list
        @project = Project.find(params[:id])
        @lists = params[:list] ||= @project.get_all_lists
        if @lists.class == String
          @lists = [@lists]
        end
        if @lists.length == 1
          @mlist = @lists[0]
          @list = @project.get_messages_list_index @mlist
          @starting_month = @list.shift.beginning_of_month
          respond_to do |format|
            format.json { render :file => "projects/json/messages_list.json.erb", :layout => "json_layout.json.erb" }
            format.xml { render :file => "projects/xml/messages_list.xml.erb", :layout => "xml_layout.xml.erb" }
          end
        else
          respond_to do |format|
            format.json { render :file => "projects/json/choose_list.json.erb", :layout => "json_layout.json.erb" }
            format.xml { render :file => "projects/xml/choose_list.xml.erb", :layout => "xml_layout.xml.erb" }
          end
        end
      end
      
      # GET /1/messages_of_month?mlist=xxx.mbox&month=Jul2013
      # GET /1/messages_of_month.json?mlist=xxx.mbox&month=Jul2013
      def messages_of_month
        @temp = Project.get_messages_list_by_subject params[:mlist],Date.strptime(params[:month],"%b%Y")
        @messages = @temp[0]
        @message_ids = @temp[1]
        @project = Project.find(params[:id])
        respond_to do |format|
          format.json { render :file => "projects/json/messages_of_month.json.erb", :layout => "json_layout.json.erb" }
          format.xml { render :file => "projects/xml/messages_of_month.xml.erb", :layout => "xml_layout.xml.erb" }
        end
      end
      
      # GET /1/message?mlist=xxx.mbox&index=1
      # GET /1/message.json?mlist=xxx.mbox&index=1
      def message
        @message = Project.get_message(params[:mlist],Date.strptime(params[:month],"%b%Y"),params[:index])
        @number_of_messages = Project.number_of_messages(params[:mlist],params[:month])
        respond_to do |format|
          format.json { render :file => "projects/json/message.json.erb", :layout => "json_layout.json.erb" }
          format.xml { render :file => "projects/xml/message.xml.erb", :layout => "xml_layout.xml.erb" }
        end
      end
      
      # Commits:
      
      # GET /projects/1/commits_report
      # GET /projects/1/commits_report.json
      def commits_report
        @project = Project.find(params[:id])
        @data = @project.get_commits_report
        
        respond_to do |format|
          format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" } 
        end
      end
      
      # GET /projects/1/commits_list
      # GET /projects/1/commits_list.json
      def commits_list
        def get_array_from_text unparsed
          return unparsed.split("\n").map{|s| s.chomp}
        end
        @project = Project.find(params[:id])
        if (params[:product] || get_array_from_text(@project.git_repos).length==1)
          if params[:product]
            @product = params[:product]
          else
            @product = @project.git_repos.chomp
          end
          @number_of_commits = Array.new(@project.number_of_commits(@product),nil)
          @number_of_pages = @number_of_commits.paginate(:page => params[:page] ||= 1 ,:per_page => params[:view] ||= 10 )
          @commits = @project.product_list_of_commits(@product, params[:page].to_i , params[:view].to_i ||= 10)   
          @branches = @project.repo(@product).branches
          respond_to do |format|
            format.json { render :file => "projects/json/commits_list.json.erb", :layout => "json_layout.json.erb" }
            format.xml { render :file => "projects/xml/commits_list.xml.erb", :layout => "xml_layout.xml.erb" }
          end
        else
          @products = get_array_from_text(@project.git_repos)
          respond_to do |format|
            format.json { render :file => "projects/json/choose_product.json.erb", :layout => "json_layout.json.erb" }
            format.xml { render :file => "projects/xml/choose_product.xml.erb", :layout => "xml_layout.xml.erb" }
          end
        end
      end
      
      # GET /shortlog/1
      # GET /shortlog/1.json
      def shortlog
        @product = params[:product]
        @commit_sha = params[:commit_sha]
        @g = Project.repo(@product)
        @number_of_commits = Array.new(Project.branch_number_of_commits(@product,@commit_sha),nil)
        @number_of_pages = @number_of_commits.paginate(:page => params[:page] ||= 1, :per_page => params[:view] ||= 10 )
        @commits = @g.gcommit(@commit_sha).log(params[:view].to_i ||= 10).skip(params[:view].to_i*(params[:page].to_i-1)).to_a
        @branches = @g.branches
        respond_to do |format|
          format.json { render :file => "projects/json/shortlog.json.erb", :layout => "json_layout.json.erb" }
          format.xml { render :file => "projects/xml/shortlog.xml.erb", :layout => "xml_layout.xml.erb" }
        end
      end
      
      # GET /commit?product=product&commit_sha=commit_sha
      # GET /commit.json?product=product&commit_sha=commit_sha
      def commit_info
        g = Project.repo(params[:product])
        @commit = g.gcommit(params[:commit_sha])
        
        respond_to do |format|
          format.json { render :file => "projects/json/commit_info.json.erb", :layout => "json_layout.json.erb" }
          format.xml { render :file => "projects/xml/commit_info.xml.erb", :layout => "xml_layout.xml.erb" }
        end
      end
      
      # --------------------------------------------------------------------------------------------------------------------------
      
      # Bugs:
      
      # GET /projects/1/bugs_report
      # GET /projects/1/bugs_report.json
      def bugs_report
        @project = Project.find(params[:id])
        @data = @project.get_bugs_report
        
        respond_to do |format|
          format.json { render :inline => @data.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @data.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" } 
        end
      end
      
      # GET /projects/1/bugs_list
      # GET /projects/1/bugs_list.json
      def bugs_list
        @project = Project.find(params[:id])
        @number_of_bugs = Array.new(@project.number_of_bugs,nil)
        @number_of_pages = @number_of_bugs.paginate(:page => params[:page] ,:per_page => params[:view] ||= 10 )
        @bugs = @project.list_of_bugs(params[:page],params[:view].to_i ||= 10)
        respond_to do |format|
          format.json { render :inline => @bugs.to_json, :layout => "json_layout.json.erb" }
          format.xml { render :inline => @bugs.to_xml(:skip_instruct => true), :layout => "xml_layout.xml.erb" } 
        end
      end
      
      
      # GET /bug/1
      # GET /bug/1.json
      def bug_info
        @bug_info = Project.fetch_bug(params[:bug_id])
        @comments = Project.fetch_comments(params[:bug_id])
        respond_to do |format|
          format.json { render :file => "projects/json/bug_info.json.erb", :layout => "json_layout.json.erb" }
          format.xml { render :file => "projects/xml/bug_info.xml.erb", :layout => "xml_layout.xml.erb" }
        end
      end
    end
  end
end