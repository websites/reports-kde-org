class Dj < Struct.new(:project_id, :changes)
  def perform
    project = Project.find(project_id)
    project.update_large_description_with_wiki_content
    project.prepare_bugs_report unless project.bugzilla_products.length == 0 || (!changes["bugzilla_products"] && !changes["bugs_report_fields"])
    project.bugs_data.destroy if (project.bugzilla_products.length == 0 and project.bugs_data)
    #project.fetch_project_repos unless project.git_repos.length == 0 # Disabled for new server.
    project.prepare_commits_data unless project.git_repos.length == 0 || !changes["git_repos"]
    project.update_attributes(commits_start_date: nil, commits_last_updated_time: nil) if (project.git_repos.length == 0 and project.title != "KDE Community")
    unless project.mailing_lists.length == 0 || !changes["mailing_lists"]
      project.fetch_mbox_file
      project.prepare_mailing_messages_data
    else
      project.update_attributes(mailing_messages_start_date: nil) if (project.mailing_lists.length == 0 and project.title != "KDE Community")
    end
    if changes["irc_channels"]
      unless project.irc_channels.length == 0
        project.prepare_irc_messages_data
      else
        project.update_attributes(irc_messages_start_date: nil) unless project.title == "KDE Community"
      end
      GeneralObjects.restart_irc_bot
    end
    unless project.facebook_pages.length == 0 || !changes["facebook_pages"]
      project.prepare_facebook_posts_data
      project.prepare_facebook_posts_files
    else
      project.update_attributes(facebook_posts_start_date: nil) if project.facebook_pages.length == 0
    end
    unless project.twitter_profile.length == 0 || !changes["twitter_profile"]
      project.prepare_twitter_data
      project.prepare_twitter_files
      GeneralObjects.restart_twitter_stream
    else
      project.update_attributes(twitter_tweets_start_date: nil) if project.twitter_profile.length == 0
    end
    unless project.google_plus_profiles.length == 0 || !changes["google_plus_profiles"]
      project.prepare_google_plus_data
      project.prepare_google_plus_files
    else
      project.update_attributes(google_plus_posts_start_date: nil) if project.google_plus_profiles.length == 0
    end
    if changes["forum_ids"]
      project.update_attributes(forum_posts_start_date: nil) if (project.forum_ids.length == 0 and project.title != "KDE Community")
      unless project.forum_ids.length == 0
        project.prepare_forum_posts_start_date
        project.prepare_forum_posts_files(project.title == "KDE Community")
      end
    end
    ReviewRequestsData.prepare_data project if changes["reviewboard_repository"]
    WikiDataManager.prepare_project_revisions_report project unless (changes.keys & WikiDataManager.fields).empty?
    BuildDataManager.prepare_report project if changes["build_jobs"]
  end
end
