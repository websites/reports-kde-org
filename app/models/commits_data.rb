class CommitsData
  include Mongoid::Document

  field :contributor
  field :repo
  field :day, type: Date
  field :number, type: Integer, default: 0
  field :insertions, type: Integer, default: 0
  field :deletions, type: Integer, default: 0

  belongs_to :project, index: true

  index repo: 1
  index contributor: 1
  index day: 1

  def self.entries_for object
    attributes = object.get_special_attributes_for "commits"
    res = object.class.choose_from_entries all, ["contributor"], [attributes.first]
    res = res.in(project_id: attributes.last.map{|p| p.id}) unless attributes.last.empty?
    res
  end

end