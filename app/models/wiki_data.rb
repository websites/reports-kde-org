class WikiData
  include Mongoid::Document

  # Returns a MediawikiApi::Client initialized to the specified site.
  def self.get_client site
    MediawikiApi::Client.new "https://#{site}.kde.org/api.php"
  end

  # Given plain HTML body content of a page, converts it to HTML that can be shown in the site directly.
  def self.convert_to_showable site, text
    ng = Nokogiri::HTML(text)
    ng.css("span.editsection").remove # Remove all edit links.
    ng.css("a").each do |link|
      link["href"] = ("https://#{site}.kde.org" + link["href"]) if (link["href"][0] == '/')
      link["target"] = "_blank"
    end
    ng.to_s
  end

  # Returns the content of a wiki page in html after making it suitable for direct display.
  def self.get_page_content site, page
    request = get_client(site).action :parse, page: page, prop: 'text', token_type: false
    return '' unless request.success?
    self.convert_to_showable site,request.data["text"]["*"]
  end

end