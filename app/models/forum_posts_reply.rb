class ForumPostsReply
  include Mongoid::Document

  field :container, type: Integer # The forum id that contains the entry.
  field :thread_id, type: Integer
  field :time, type: Time
  field :replier
  field :first_reply_period, type: Float #in hours.

  index container: 1
  index({ container: 1, thread_id: 1 }, {unique: true})


  def self.entries_for object
    attributes = object.get_attributes_for "forum_posts"
    object.class.choose_from_entries ne(first_reply_period: nil), ["replier","container"], attributes
  end

  # Generates all appropriate records for the forums.
  # No need to call this if a new setup as the reply data is setup while setting the posts data.
  # This is implemented because in the production version, posts data had already been generated.
  def self.prepare_forum_posts_reply start_year = 2015
    entries = ["threads","posts"]
    params = ["sf=firstpost&",""]
    entries.zip(params).each do |entry,param|
      num = 0
      while num < 20000
        link = "https://forum.kde.org/search.php?#{param}sd=d&st=0&feed_type=RSS2.0&feed_style=COMPACT&countlimit=100&start=#{num}&submit=Search"
        posts = RSS::Parser.parse(open_link(link),false,false).items
        done = false
        posts.each do |post|
          if post.pubDate.year < start_year
            done = true
            break
          end
          ForumPostsReply.process_post post, entry
        end
        break if done
        num+= 100
      end
    end
  end

  def self.process_post post, type
    case type
    when "threads"
      self.process_thread post
    when "posts"
      self.process_reply post
    end
  end

  # Called when a new thread is posted to update the table.
  def self.process_thread post
    fid, tid = get_forum_post_info_from_link post.link
    self.create(container: fid.to_i, thread_id: tid.to_i, time: post.pubDate)
  end

  # Called when a new reply is posted to update the table.
  #TODO: It would be better to loop through the posts in reverse order
  # so as to have first_reply_period set correctly in the first update.
  def self.process_reply post
    fid, tid = get_forum_post_info_from_link post.link
    entry = self.where(container: fid.to_i, thread_id: tid.to_i).first
    return unless entry
    new_reply_period = post.pubDate.minus_with_coercion(entry.time)/1.hour
    return if new_reply_period == 0
    return if entry.first_reply_period && entry.first_reply_period < new_reply_period
    entry.first_reply_period = new_reply_period
    entry.replier = post.author.force_encoding("UTF-8")
    entry.save
  end
end