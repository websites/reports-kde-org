class FacebookPostsData
  include Mongoid::Document
  
  field :post_id
  field :day, type: Date
  field :likes_count, type: Integer, default: 0 # Total likes for posts written on that day.
  field :comments_count, type: Integer, default: 0 # Total comments for posts written on that day.
  field :shares_count, type: Integer, default: 0 # Total number of shares for posts written on that day.
  
  belongs_to :project, index: true

  index day: 1
  
end