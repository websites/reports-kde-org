class MailingMessagesReply
  include Mongoid::Document

  field :container # The mailing list that contains the entry.
  field :message_id
  field :time, type: Time
  field :replier
  field :first_reply_period, type: Float #in hours

  index container: 1
  index({ container: 1, message_id: 1 }, {unique: true})


  # Generates all appropriate records for the given list.
  # The list mbox file should be already downloaded and ready.
  def self.generate_for mlist
    self.where(container: mlist).destroy_all
    messages = Project.all_messages mlist
    entries = Hash.new(nil)
    messages.each do |message|
      message = Mail.new(message)
      self.process_message mlist, message
    end
  end

  # Handles a new reply for an entry.
  # Sets the first_reply_period if no previous reply.
  def add_reply message
    self.first_reply_period ||= (message.date.to_time.minus_with_coercion(self.time)/1.hour)
    self.replier = message.sender
    save
  end

  # Called when a new message is received to update the table.
  def self.process_message mlist, message
    if message.references
      parent = message.references
      parent = parent.first if parent.class == Array
      entry = self.where(container: mlist, message_id: parent)
      entry.first.add_reply(message) if entry.exists?
    else
      message_id = message.message_id
      return unless message_id
      entry = self.new(container: mlist, message_id: message_id, time: message.date)
      entry.save if entry.valid?
    end
  end
  
  def self.entries_for object
    attributes = object.get_attributes_for "mailing_messages"
    object.class.choose_from_entries ne(first_reply_period: nil), ["replier","container"], attributes
  end
end