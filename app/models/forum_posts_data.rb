class ForumPostsData
  include Mongoid::Document

  field :contributor  
  field :day, type: Date
  field :fid, type: Integer
  field :posts, type: Integer, default: 0 # Number of posts written by that contributor that day(including threads started).
  field :threads, type: Integer, default: 0 # Number of threads started by that contributor that day.
  field :resolved, type: Integer, default: 0 # Number of threads started by that contributor on that day that were resolved.
  
  index contributor: 1
  index day: 1
  
  def self.entries_for object
    attributes = object.get_attributes_for "forum_posts"
    object.class.choose_from_entries all, ["contributor","fid"], attributes
  end
  
end