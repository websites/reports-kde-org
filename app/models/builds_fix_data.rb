class BuildsFixData
  include Mongoid::Document

  field :container # The job that contains the entry.
  field :time, type: Time
  field :number, type: Integer, default: 0
  field :fixer
  field :fix_period, type: Float #in hours.

  index container: 1
  index fixer: 1
  
  def self.process_entry container, status, time, contributor
    if status == "SUCCESS"
      process_successful container, time, contributor
    else
      process_unsuccessful container, time
    end
    nil
  end
  
  def self.process_successful container, time, contributor
    entry = first_unfixed_entry container, time
    return unless entry
    entry.fixer = contributor || "rebuild"
    entry.fix_period = time.minus_with_coercion(entry.time)/1.hour
    entry.save
  end
  
  def self.process_unsuccessful container, time
    entry = first_unfixed_entry container, time
    if entry
      entry.number+= 1
      entry.save
    else
      create(container: container, time: time, number: 1)
    end
    nil
  end
  
  def self.first_unfixed_entry container, time
    where(container: container, :time.lte => time, fixer: nil).first
  end
  
  def self.entries_for object
    attributes = object.get_attributes_for "build"
    CustomReport.choose_from_entries ne(fixer: nil), ["fixer", "container"], attributes
  end
end