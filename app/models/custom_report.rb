class CustomReport
  include Mongoid::Document

  field :title
  field :description, default: ""
  Constants::CUSTOM_REPORT_TYPES.each do |type|
    field "#{type}_contributors".to_sym, type: Array, default: Array.new
    field "#{type}_products".to_sym, type: Array, default: Array.new
    field "generate_#{type}_report".to_sym, type: Boolean, default: false
    field "#{type}_start_date".to_sym, type: Date
  end
  field :generated, type: Boolean, default: false
  field :drawables, type: Hash, default: Hash.new

  validates_presence_of :title

  belongs_to :user
  has_and_belongs_to_many :projects, inverse_of: nil

  after_create :do_jobs

  def do_jobs
    Delayed::Job.enqueue(CustomDj.new(id))
  end

  def self.get_permitted_attributes
    res = [:title, :description, {:project_ids => []}]
    Constants::CUSTOM_REPORT_TYPES.each do |type|
      res << {"#{type}_contributors".to_sym => []}
      res << {"#{type}_products".to_sym => []}
      res << "generate_#{type}_report"
    end
    res
  end

  # Path to be used while fetching json for end users.
  def display_path_for type
    Rails.application.routes.url_helpers.custom_report_path("en",self) + "/#{type}/"
  end

  # Path to be used while creating the json files within the app.
  def path_for type
    locale = "en"
    path = "public"
    path += Rails.application.routes.url_helpers.custom_report_path(locale,self)
    path += "/#{type}/"
    FileUtils.mkdir_p(path)
    path
  end

  def set_start_date type, starting_date
    self["#{type}_start_date"] = starting_date
    self.save
  end

  def set_drawables type, data
    drawables[type] = Hash.new
    data.each do |key,value|
      new_key = key.partition("_").first
      if value.class == Hash
        drawables[type][new_key] = true
      else
        next if value.length == 0
        drawables[type][new_key] = false
      end
    end
    save
  end

  def is_included? type
    self["generate_#{type}_report"]    
  end

  # Returns the custom report path to be used in files generation.
  # It guarantees that the returned path exists.
  def custom_report_path type
    locale = "en"
    path = "public"
    path += Rails.application.routes.url_helpers.custom_report_path(locale,self)
    path += "/#{type}/"
    FileUtils.mkdir_p(path)
    path
  end

  def prepare_reports
    Constants::CUSTOM_REPORT_TYPES.each do |type|
      if is_included?(type)
        data_manager = Constants::TYPES_TO_DATA_MANAGERS[type].dup
        data_manager.shift.prepare_report self, *data_manager
      end 
    end
    self.generated = true
    self.save
  end

  def get_projects_from_titles text
    titles = get_array_from_text(text)
    res = Array.new
    titles.each do |title|
      res << Project.find_by(title: title) rescue nil
    end
    res
  end
  
  # Returns an array of 2 elements.
  # First represents an array of contributors to include in the report.
  # Second represents an array of products to include in the report.
  # An empty array in any of the 2 elements means that there's no constraint on that attribtue.
  def get_attributes_for type
    [
      self["#{type}_contributors"],
      get_products_for(type)
    ]
  end

  def get_special_attributes_for type
    [
      self["#{type}_contributors"],
      self.projects
    ]
  end

  def get_products_for type
    return self["#{type}_products"] unless self["#{type}_products"].empty?
    get_products_from_projects type 
  end
  
  def get_products_from_projects type
    res = Array.new
    self.projects.each do |project|
      res&= project.get_products_of type
    end
    res
  end

  # Returns an array that contains the wikis that should be drawn for this project.
  def drawable_wikis
    res = Array.new
    WikiDataManager.explorable_sites.each do |site|
      res << site unless self["#{site}_wiki_pages"].empty? and self[]
    end
    res
  end

  # Replaces empty fields with ALL.
  def replace_empty_with_all type
    self["#{type}_contributors"] = "ALL" if self["#{type}_contributors"].empty?
    self.projects = Project.all if self.projects.empty?
  end

  # Given list of candidate entries, names of fields to choose with and attributes to constraint with,
  # it returns the chosen entries.
  def self.choose_from_entries entries, fields, attributes
    fields.zip(attributes).each do |field, attribute|
      entries = entries.in(field.to_sym => attribute) unless attribute.empty?
    end
    entries 
  end

  # Returns all entries for a specific type with the given requirements.
  def get_entries_for type, data_class, contributors_name = "contributor", projects_name = "project"
    replace_empty_with_all type
    entries = data_class.all
    entries = entries.in(contributors_name.to_sym => get_array_from_text(self["#{type}_contributors"])) unless self["#{type}_contributors"] == "ALL"
    entries = entries.in(projects_name.to_sym => self.projects)
    entries
  end

  # files is a hash with keys representing file names and values represeting
  # the chart to be drawn.
  def prepare_report_files type, files
    path = custom_report_path type    
    files.each do |file_name, chart|
      File.write(path+"#{file_name}.json",chart.to_json)
    end
  end

  #TODO: CommitsData would be better using repos rather than projects.
  def prepare_commits_report
    entries = CommitsData.entries_for self
    return if entries.empty?
    self.commits_start_date = entries.min(:day)
    files = Hash.new
    files["objects"] = Timeline.get_timeline_for entries, "number"
    files["lines"] = construct_multiline_graph entries, ["insertions","deletions"]
    prepare_report_files "commits", files
    self.drawables["commits"] = true
    self.save
  end

  def prepare_mailing_messages_report
    entries = MailingMessagesData.entries_for self
    return if entries.empty?
    self.mailing_messages_start_date = entries.min(:day)
    files = Hash.new
    files["objects"] = Timeline.get_timeline_for entries, "number"
    prepare_report_files "mailing_messages", files
    self.drawables["mailing_messages"] = true
    self.save
  end

  def prepare_irc_messages_report
    entries = IrcMessagesData.entries_for self
    return if entries.empty?
    self.irc_messages_start_date = entries.min(:day)
    files = Hash.new
    files["objects"] = Timeline.get_timeline_for entries, "number"
    prepare_report_files "irc_messages", files
    self.drawables["irc_messages"] = true
    self.save
  end

  def prepare_forum_posts_report
    ForumDataManager.prepare_report self
  end

  def prepare_wiki_report
    entries = get_entries_for 
  end

end
