class News
  include Mongoid::Document
  resourcify
  belongs_to :user
  field :title
  field :content
  field :created_at, type: Date, default: Date.today
  
  validates :title , :presence => true
  validates :content , :presence => true
  #self.per_page = 5
end
