class FacebookDoersData
  include Mongoid::Document
  
  field :doer
  field :day, type: Date
  field :comments, type: Integer, default: 0
  field :likes, type: Integer, default: 0
  
  belongs_to :project, index: true
    
  index doer: 1
  index day: 1
  
end