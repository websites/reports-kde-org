class IrcMessagesReply
  include Mongoid::Document

  field :container # The irc channel that contains the entry.
  field :asker
  field :time, type: Time
  field :replier
  field :first_reply_period, type: Float #in hours

  index container: 1
  index time: 1
  index({ container: 1, asker: 1 })

  # Handles a new reply for an entry.
  # Sets the first_reply_period if no previous reply.
  def add_reply message_time, sender
    return if first_reply_period
    self.first_reply_period = message_time.minus_with_coercion(self.time)/1.hour
    self.replier = sender
    save
  end

  # Called when a new message is received to update the table.
  def self.process_message channel, sender, message, time
    ind = message.index(":")
    if ind && ind < 17 && !message[0,ind].index(" ")
      # This is a reply
      asker = message[0,ind]
      entry = get_entry_for(channel, asker)
      entry.last.add_reply(time, sender) if entry.exists?
    else
      # This is a potential question
      asker = sender
      entry = get_entry_for(channel, asker)
      return if entry.exists? && (!entry.first.first_reply_period || time.minus_with_coercion(entry.first.time) < 10.minutes)
      return if self.where(container: channel, replier: asker, :time.gte => (time - 10.minutes)).exists?
      self.create(container: channel, asker: sender, time: time)
    end
  end
  
  def self.entries_for object
    attributes = object.get_attributes_for "irc_messages"
    object.class.choose_from_entries ne(first_reply_period: nil), ["replier","container"], attributes
  end

  def self.get_entry_for container, asker
    self.where(asker: asker, container: container, :time.gte => 1.day.ago)
  end
end