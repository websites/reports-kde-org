class TwitterTweetsData
  include Mongoid::Document

  field :tweet_id  
  field :day, type: Date
  field :favorite_count, type: Integer, default: 0 # Total favorites for that tweet.
  field :retweet_count, type: Integer, default: 0 # Total retweets for that tweet.
  field :is_tweet, type: Boolean, default: true # Specifies whether the item is a tweet by the page or not(not = retweet).
  
  belongs_to :project, index: true

  index day: 1
  
end