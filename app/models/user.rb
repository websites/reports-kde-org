class User
  include Mongoid::Document
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :ldap_authenticatable, :rememberable, :trackable

  field :login
  field :email
  field :message

  # Rememberable.
  field :remember_created_at, :type => Time
  field :remember_token

  # Trackable.
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  # Adding a many to many relationship between projects and users.
  has_and_belongs_to_many :projects

  # Adding a one to many relationship between users and news.
  has_many :news

  has_many :custom_reports

  # Assign a role according to the group.
  before_save :assign_role

  # Add a "undecided" role to all new users. Not needed after identity authentication.
  after_create :assign_undecided_role

  # Assign the suitable role for the user after he is authenticated.
  def assign_role
    ldap_connect = Devise::LDAP::Adapter.ldap_connect(login)
    if ldap_connect.in_group?("sysadmins")
      self.add_role "admin" unless self.has_role?("admin")
    elsif ldap_connect.in_group?("developers")
      self.add_role "super_developer" unless self.has_role?("super_developer")
    end
  end


  def assign_undecided_role
    self.add_role "undecided"
  end

  def remove_all_roles
    Role.all.each do |role|
      self.remove_role role.name
    end
  end



end
