class DotPostsData
  include Mongoid::Document

  field :contributor
  field :category
  field :day, type: Date
  field :posts, type: Integer, default: 0 # Number of posts written by that contributor that day.
  field :comments_count, type: Integer, default: 0 # Number of comments that this contributor's posts on that day got.
  
  index contributor: 1
  index day: 1
  
  def self.entries_for object
    attributes = object.get_attributes_for "dot_posts"
    CustomReport.choose_from_entries all, ["contributor", "category"], attributes
  end
end