class PlanetPostsData
  include Mongoid::Document

  field :contributor
  field :day, type: Date
  field :number, type: Integer, default: 0 # Number of posts written by that contributor that day.

  index contributor: 1
  index day: 1

  def self.get_latest_posts num = nil
    link = "http://planetkde.org/rss20.xml"
    posts = RSS::Parser.parse(open_link(link),false,false).items
    return posts unless num
    posts[0,num]
  end
  
  def self.entries_for object
    attributes = object.get_attributes_for "planet_posts"
    CustomReport.choose_from_entries all, ["contributor"], [attributes.first]
  end

end