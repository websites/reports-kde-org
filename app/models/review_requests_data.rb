class ReviewRequestsData
  include Mongoid::Document

  field :contributor
  field :day, type: Date
  field :number, type: Integer, default: 0
  field :number_submitted, type: Integer, default: 0
  field :number_discarded, type: Integer, default: 0
  field :ship_it_count, type: Integer, default: 0
  field :bugs_closed_count, type: Integer, default: 0
  field :days_to_close, type: Integer, default: 0 # Holds the total number of days taken to close all closed review requests closed in under a year.
  field :closed_after_a_year, type: Integer, default: 0 # Holds the number of review requests openned that day and were closed in more than a year.
  field :total_days_to_close, type: Integer, default: 0 # Holds the total number of days taken to close all closed review requests

  belongs_to :project, index: true

  index contributor: 1
  index day: 1


  # Reviews Report:

  def self.entries_for object
    attributes = object.get_special_attributes_for "review_requests"
    res = CustomReport.choose_from_entries all, ["contributor"], [attributes.first]
    res = res.in(project_id: attributes.last.map{|p| p.id}) unless attributes.last.empty?
    res
  end

  def self.reviewboard_interface link
    http = Curl.get(URI::encode(link)) do|h|
      h.headers['Accept'] = "application/json"
    end
    JSON.parse http.body_str
  end

  def self.get_id_for repository
    link = "https://git.reviewboard.kde.org/api/repositories/?name=#{repository}"
    response = self.reviewboard_interface link
    response["repositories"].first["id"]
  end

  def self.prepare_data project,begin_date = nil
    link = "https://git.reviewboard.kde.org/api/review-requests/?max-results=200&status=all"
    if begin_date
      project.review_requests_data.gte(day: begin_date).destroy_all
      link+= "&time-added-from=#{begin_date.to_s}"
    else
      project.review_requests_data.destroy_all
    end
    return if project.reviewboard_repository.length == 0
    repo_id = self.get_id_for project.reviewboard_repository
    link+= "&repository=#{repo_id}"
    start_date = Date.today
    while true
      response = self.reviewboard_interface link
      review_requests = response["review_requests"]
      review_requests.each do |rr|
        date = rr["time_added"].to_date
        data = project.review_requests_data.find_or_create_by(
          contributor: rr["links"]["submitter"]["title"],
          day: date
        )
        data.number+= 1
        data.number_submitted+= 1 if rr["status"] == "submitted"
        data.number_discarded+= 1 if rr["status"] == "discarded"
        data.ship_it_count+= rr["ship_it_count"]
        data.bugs_closed_count+= rr["bugs_closed"].length
        unless rr["status"] == "pending"
          cur_days_to_close = (rr["last_updated"].to_date - rr["time_added"].to_date).to_i
          data.total_days_to_close+= cur_days_to_close
          if (cur_days_to_close > 365)
            data.closed_after_a_year+= 1
          else
            data.days_to_close+= cur_days_to_close
          end
        end
        data.save
        start_date = date if date < start_date
      end
      break unless response["links"]["next"]
      link = response["links"]["next"]["href"]
    end
    project.review_requests_start_date||= start_date
    the_all = Project.find_by(title: "KDE Community")
    the_all.review_requests_start_date = start_date if start_date < the_all.review_requests_start_date
    the_all.save
    project.save
    self.prepare_files project
  end

  # Prepares files needed to show the Review Requests report.
  def self.prepare_files project
    all = true if project.title == "KDE Community"
    return unless ReviewRequestsData.first
    return unless all or project.review_requests_data.first
    all_lines = Hash.new
    all_lines["objects"] = project.get_multiple_objects_line_charts "review_requests",all
    all_lines["objects"]["number_created"] = all_lines["objects"].delete("number")
    all_lines["average_close_time"] = self.get_average_close_time_line project,all
    all_lines["closed_after_a_year"] = self.get_number_closed_after_a_year_line project,all
    all_lines["doers_daily"] = project.get_daily_doers_line_chart "review_requests", all
    all_lines["doers_monthly"] = project.get_monthly_doers_line_chart "review_requests", all
    all_lines["doers_yearly"] = project.get_yearly_doers_line_chart "review_requests", all
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,project)
    project_path += "/review_requests/"
    FileUtils.mkdir_p(project_path)
    all_lines.each do |key,value|
      File.open(project_path+key+".json",'w') do |f|
        f << value.to_json
      end
    end
    project.prepare_pie_charts "review_requests", all
  end

  def self.get_average_close_time_line project,all=false
    entries = project.get_appropriate_entries "review_requests",all
    map = %Q{
      function() {
        emit(this.day,this.number_submitted+this.number_discarded-this.closed_after_a_year);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values);
      };
    }
    closed_count_data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    closed_count_data = closed_count_data.sort_by { |entry| entry["_id"] }
    return if closed_count_data.length == 0
    map = %Q{
      function() {
        emit(this.day,this.days_to_close);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values);
      };
    }
    time_to_close_data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    time_to_close_data = time_to_close_data.sort_by { |entry| entry["_id"] }
    res = Array.new
    prev_date = closed_count_data.first["_id"].to_date
    prev_date-= 1
    closed_count_data.zip(time_to_close_data).each do |c,t|
      cur_date = c["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res += Array.new(num_zeros,0)
      if (c["value"] == 0)
        res << 0
      else
        res << (t["value"].to_f / c["value"].to_f)
      end
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros =  (cur_date - prev_date).to_i
    res += Array.new(num_zeros,0)
  end

  def self.get_number_closed_after_a_year_line project, all
    entries = project.get_appropriate_entries "review_requests",all
    map = %Q{
      function() {
        emit(this.day,this.closed_after_a_year);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values);
      };
    }
    num_closed_after_a_year = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    num_closed_after_a_year = num_closed_after_a_year.sort_by { |entry| entry["_id"] }
    res = Array.new
    prev_date = num_closed_after_a_year.first["_id"].to_date
    prev_date-= 1
    num_closed_after_a_year.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res += Array.new(num_zeros,0)
      res << entry["value"]
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros =  (cur_date - prev_date).to_i
    res += Array.new(num_zeros,0)
  end

  def self.get_total_average_close_time project
    map = %Q{
      function() {
        emit(1,{count: this.number_submitted+this.number_discarded, time: this.total_days_to_close});
      };
    }
    reduce = %Q{
      function(key,values) {
        var result = { count: 0, time: 0};
        values.forEach(function(value) {
          result.count += value.count;
          result.time += value.time;
        });
        return result;
      };
    }
    if project.title == "KDE Community"
      data = ReviewRequestsData.all
    else
      data = project.review_requests_data
    end
    nums = data.map_reduce(map, reduce).out(replace: "map-reduce").entries.first["value"]
    return nums["time"].to_f / nums["count"] unless nums["count"] == 0
    return 0
  end

  def self.periodic_update begin_date = 1.month.ago
    Project.each do |p|
      self.prepare_data(p,begin_date) unless p.reviewboard_repository.length == 0
    end
    self.prepare_files Project.find_by(title: "KDE Community")
  end
end