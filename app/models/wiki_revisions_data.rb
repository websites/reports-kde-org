class WikiRevisionsData
  include Mongoid::Document

  field :contributor
  field :site #TODO: Might be better to convert this to an integer or enum.
  field :page
  field :day, type: Date
  field :number, type: Integer, default: 0 # Number of revisions.

  # Adds to the databse all data about revisions of the given page.
  def self.add_revisions_data site, page_title, date = nil
    MediawikiAdapter.loop_revisions(site, page_title) do |revision|
      day = revision["timestamp"].to_date
      return if date && day < date
      entry = find_or_create_by(
        site: site,
        page: page_title,
        contributor: revision["user"],
        day: day
      )
      entry.number+= 1
      entry.save
    end
  end

  #TODO: rename to entries_for after getting rid of the other one.
  def self.get_entries_for object, site
    attributes = object.get_attributes_for "#{site}_wiki"
    res = object.class.choose_from_entries where(site: site), ["contributor"], [attributes.first]
    pages = attributes.last.map { |page| MediawikiAdapter.convert_to_wiki_regexp(page) }
    res = res.in(page: pages) unless pages.empty?
    res
  end

  # Returns all database entries relevant to this project
  def self.entries_for project, site, start_date = nil
    query = where(site: site)
    query = query.where(:day.gte => start_date) if start_date
    return query if project.all_kde_project?
    pages = Array.new
    get_array_from_text(project["#{site}_wiki_pages"]).each do |page|
      pages << MediawikiAdapter.convert_to_wiki_regexp(page)
    end
    query.in(page: pages)
  end
end