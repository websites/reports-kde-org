class TwitterDoersData
  include Mongoid::Document
  
  field :doer
  field :day, type: Date
  field :retweets, type: Integer, default: 0
  
  belongs_to :project, index: true
    
  index doer: 1
  index day: 1
  
end