class WikiPagesData
  include Mongoid::Document

  field :site #TODO: Might be better to convert this to an integer or enum.
  field :page
  field :views, type: Integer, default: 0

  def self.add_page_data site, page_title
    info = MediawikiAdapter.get_page_info site, page_title
    entry = find_or_create_by(site: site, page: page_title)
    entry.views = info["counter"]
    entry.save
  end
  
  #TODO: rename to entries_for after getting rid of the other one.
  def self.get_entries_for object, site
    attributes = object.get_attributes_for "#{site}_wiki"
    res = where(site: site)
    pages = attributes.last.map { |page| MediawikiAdapter.convert_to_wiki_regexp(page) }
    res = res.in(page: pages) unless pages.empty?
    res 
  end
  
  # Returns all database entries relevant to this project
  def self.entries_for project, site
    query = where(site: site)
    return query if project.all_kde_project?
    pages = Array.new
    get_array_from_text(project["#{site}_wiki_pages"]).each do |page|
      pages << MediawikiAdapter.convert_to_wiki_regexp(page)
    end
    query.in(page: pages)
  end
end