class Role
  include Mongoid::Document
  has_and_belongs_to_many :users #, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true
  field :name
  scopify
end
