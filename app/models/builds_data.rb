class BuildsData
  include Mongoid::Document

  field :contributor
  field :status
  field :job
  field :day, type: Date
  field :number, type: Integer, default: 0

  index job: 1
  index status: 1
  index contributor: 1
  index day: 1
  
  def self.entries_for object, status
    attributes = object.get_attributes_for "build"
    CustomReport.choose_from_entries where(status: status), ["contributor","job"], attributes
  end
  
  def self.with_contributors
    ne(contributor: nil)
  end
end