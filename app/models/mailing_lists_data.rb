class MailingListsData
  include Mongoid::Document
  
  field :list
  field :number_of_messages, type: Integer, default: 0
  field :starting_month, type: Date, default: Date.today
    
  # Increments the number of messages by 1. Should be called when a new message is sent to this mailing list.
  def self.increment_number_of_messages mlist
    mlist_data = MailingListsData.find_or_create_by(list: mlist)
    mlist_data.number_of_messages+= 1
    mlist_data.save
  end
  
  # Adds a new message to an mbox archive.
  def self.add_message_to_mbox mbox,month,email
    add_line = File.exist?("mbox_files/" + mbox + "/" + month + ".mbox")
    f = File.open("mbox_files/" + mbox + "/" + month + ".mbox","a")
    f << "\n" if add_line
    f << "From " + email.from.first.to_s.encode('utf-8', :invalid => :replace, :undef => :replace) + " " + email.date.strftime("%a %b %d %H:%M:%S %Y") + "\n"
    f << email.to_s.encode('utf-8', :invalid => :replace, :undef => :replace)
    f.close
  end
  
  # Splits the mbox file into several mbox files, one per month. 
  def self.split_mbox_file mlist
    the_list =  Project.all_messages mlist
    month = MailingListsData.determine_beginning_month the_list
    begin
    Dir.mkdir("mbox_files/" + mlist[0...-5] ) unless File.exists?("mbox_files" + mlist[0...-5])
    rescue
    end
    MailingListsData.make_all_months_mbox_files mlist
  end
  
  # Determine the beginning month in the given list.
  def self.determine_beginning_month the_list
    begin_month = Date.today  
    the_list.each({:headers_only => true}) do |message|
      current_date = message.ruby_date
      if current_date < begin_month
        begin_month = current_date
      end
    end
    return begin_month.beginning_of_month
  end
  
  # splits messages into separate months mbox files.
  def self.make_all_months_mbox_files mlist
    files = Hash.new()
    the_list = Project.all_messages mlist
    counter = -1
    the_list.each_raw_message do |message|
      temp_file = StringIO.new(message)
      the_date = Mbox::Mail.parse(temp_file,{:headers_only => true, :separator => Mbox.separator}).ruby_date.strftime("%b%Y")
      temp_file.close
      files[the_date] = File.open('mbox_files/' + mlist[0...-5] + '/' + the_date + '.mbox', 'wb') unless files[the_date]
      files[the_date] << message # Change if not working.
      counter += 1
    end
    files.each do |k,v|
      v.close
    end
  end
  
end