class BugsData
  include Mongoid::Document

  field :assigned_to, type: Array, default: []
  field :classification, type: Array, default: []
  field :component, type: Array, default: []
  field :creator, type: Array, default: []
  field :net_open_line, type: Array, default: []
  field :op_sys, type: Array, default: []
  field :open_by_priority, type: Array, default: []
  field :platform, type: Array, default: []
  field :priority, type: Array, default: []
  field :product, type: Array, default: []
  field :reported_line, type: Array, default: []
  field :resolved_by_assigned_to, type: Array, default: []
  field :resolved_line, type: Array, default: []
  field :still_open_line, type: Array, default: []
  field :average_close_time_line, type: Array, default: []
  field :resolved_after_a_year_line, type: Array, default: []
  field :severity, type: Array, default: []
  field :start_date, type: DateTime
  field :status, type: Array, default: []
  field :total_created_line, type: Array, default: []
  field :total_number, type: Integer
  field :total_average_close_time, type: Float, default: 0
  field :version, type: Array, default: []

  embedded_in :project

end
