class GooglePlusPostsData
  include Mongoid::Document
  
  field :post_id
  field :day, type: Date
  field :plusones_count, type: Integer, default: 0 # Total plusones for posts written on that day.
  field :replies_count, type: Integer, default: 0 # Total comments for posts written on that day.
  field :reshares_count, type: Integer, default: 0 # Total number of reshares for posts written on that day.
  
  belongs_to :project, index: true

  index day: 1
  
end