#require 'open-uri' # Needed for fetching data from external URLs
require 'mbox' # Needed for parsing mbox archives.
require 'mail' # Needed for parsing messages.
require 'rss' # Needed for parsing rss.

# --------------------------------------------------------------------------------------------------------------------------
# General Functions:
# These are functions that are used generally in other functions.

# Opens a link and returns its response
def open_link link
  Curl.get(URI::encode(link)).body_str
end

# Given a text, it splits the text into an array. Used for getting an array out of text areas that hold one item per line
def get_array_from_text unparsed
  return unparsed.split("\n").map{|s| s.chomp}
end

# Given a text, it splits the text into an array and returns the size of that array.
def get_length_of_text_array unparsed
  return unparsed.split("\n").length
end

# Given a Ruby array, it transforms it into a string. This is used to represent the array in JSON format.
def convert_array_to_string_array to_convert
  to_convert.to_json
end

def period_to_adj period
  return "daily" if period == "day"
  period + "ly"
end

# Given a time object, it returns the string representation in the format accepted by Bugzilla.
def get_time_as_string time
  return time.strftime("%Y-%m-%dT%H:%M:%SZ")
end

# Receives an array of pairs and converts it into a Hash.
def hashify_array the_array
  res = Hash.new(0)
  the_array.each do |pair|
    res[pair[0]] = pair[1]
  end
  return res
end

# Returns an array of hashes adjusted to be shown in the general project report page.
def hashify_for_general_report the_array, first_name, last_name
  res = Array.new()
  the_array.each do |pair|
    res << {first_name => pair[0], last_name => pair[1]}
  end
  return res
end

# Returns an array of chosen colors to be displayed in pie charts.
def all_colors
  return ["#4684ee", "#dc3912", "#ff9900", "#008000", "#666666",
    "#4942cc", "#cb4ac5", "#d6ae00", "#336699", "#dd4477",
    "#aaaa11", "#66aa00", "#888888", "#994499", "#dd5511",
    "#22aa99", "#999999", "#705770", "#109618", "#a32929"]
end

def javascript_map_filler_function items
  res = "var res = [];
  "
  items.each do |item|
    res+= "if (this.#{item} > 0) {
      res.push(1);
    } else {
      res.push(0);
    }
    "
  end
  return res
end

def doers_reduce
  %Q{
      function(key,values) {
        var res = [];
        var len = values[0].length;
        for (var i = 0; i < len; i++) {
          var found_one = false;
          for (var j = 0; j < values.length; j++) {
            if (values[j][i] > 0) {
              found_one = true;
              break;
            }
          }
          if (found_one) {
            res.push(1);
          }
          else {
            res.push(0);
          }
        }
        return res.toString();
      };
    }
end

def report_has_reply? type
  ["mailing_messages","irc_messages"].include? type
end

# Returns the class that holds the replies data for type.
def reply_class_for type
  case type
  when "mailing_messages"
    return MailingMessagesReply
  when "irc_messages"
    return IrcMessagesReply
  when "forum_posts"
    return ForumPostsReply
  end
end

# ----------------------------------------------------------------------------------------------------------------------------

# Bugzilla Functions:
# These are functions that are used to interact with Bugzilla.



# Given the parameters, it fetches the list of bugs matching the given parameters.
def search_bugzilla include_fields=nil,limit=0,offset=0,status = nil, creation_time = nil, last_change_time = nil,products = bugzilla_products
  # include_fields: An array of fields that should be fetched.
  # limit, offset: Used to display lists of bugs.
  # status: An array of bugs status to search for.
  creation_time ||= Date.new(2002,9,16)
  full_string = 'https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&params=[{"product":' + convert_array_to_string_array(products)
  full_string += ',"include_fields":' + convert_array_to_string_array(include_fields) if include_fields
  full_string += ',"status":' + convert_array_to_string_array(status) if status
  full_string += ',"limit":' + limit.to_s + ',"offset":'+offset.to_s
  full_string += ',"creation_time":"'+ get_time_as_string(creation_time) +'"' if creation_time
  full_string += ',"last_change_time":"'+ get_time_as_string(last_change_time) +'"' if last_change_time
  full_string += '}]'
  unparsed = open_link full_string
  parsed = ActiveSupport::JSON.decode(unparsed)
  return parsed["result"]["bugs"] if parsed["result"]
end

# Returns an array of status which cause the bug to be considered as closed in Bugzilla.
def closed_bugs_status
  return ["RESOLVED","NEEDSINFO","CLOSED"]
end

# ----------------------------------------------------------------------------------------------------------------------------

# Mailing Lists Functions:

# Returns an Mbox object containing messages of the specified month archive.
def open_month_archive mlist,month
  begin
    return Mbox.open('mbox_files/' + mlist[0...-5] + '/' + month.strftime("%b%Y") + '.mbox') unless month.class == String
    return Mbox.open('mbox_files/' + mlist[0...-5] + '/' + month + '.mbox')
  rescue
    return nil
  end
end

# Returns an array of Mail objects(headers_only) in the Mbox object.
def get_messages_array the_list
  res = Array.new
  the_list.each do |message|
        res << message
  end
  return res
end

# IRC Functions

# Updates the data in the cache to update the number of messages for the sender.
def irc_update_pie_data cache_key,sender
  the_hash = Rails.cache.read(cache_key)
  the_hash[sender] += 1
  Rails.cache.write(cache_key,the_hash)
end

def shift_then_zero cache_key
  the_array = eval(Rails.cache.read(cache_key))
  the_array.shift
  the_array << 0
  Rails.cache.write(cache_key,the_array.inspect)
end

# Forums Functions

# Returns the fid given the link of the post.
def get_fid_from_link post_link
  post_link.match(/https:\/\/forum.kde.org\/viewtopic.php\?f=(\d+)/)[1]
end

# Returns the fid and tid given the link of the post.
def get_forum_post_info_from_link post_link
  post_link.match(/https:\/\/forum.kde.org\/viewtopic.php\?f=(\d+)&t=(\d+)/)[1,2]
end

# ----------------------------------------------------------------------------------------------------------------------------

class Project
  include Mongoid::Document
  include Mongoid::Tree # To have subprojects.
  include Mongoid::Slug # Friendly ID.
  resourcify

  field :title
  field :description, default: ""
  field :large_description, default: ""
  field :community_wiki_description_page, default: ""
  field :bugzilla_products, type: Array, default: []
  field :bugs_report_fields, type: Array, default: []
  field :git_repos, default: ""
  field :pko_repository_link
  field :repos_last_sha, type: Array, default: []
  field :commits_start_date, type: Date
  field :commits_last_updated_time, type: DateTime
  field :number_of_commits, type: Integer, default: 0
  field :reviewboard_repository, default: ""
  field :review_requests_start_date, type: Date
  field :mailing_lists, default: ""
  field :mailing_messages_start_date, type: Date
  field :number_of_mailing_messages, type: Integer, default: 0
  field :irc_channels, default: ""
  field :irc_messages_start_date, type: Date
  field :number_of_irc_messages, type: Integer, default: 0
  field :facebook_pages, default: ""
  field :facebook_posts_start_date, type: Date
  field :twitter_profile, default: ""
  field :twitter_profile_id, type: Integer
  field :twitter_tweets_start_date, type: Date
  field :google_plus_profiles, default: ""
  field :google_plus_posts_start_date, type: Date
  field :forum_ids, default: ""
  field :forum_posts_start_date, type: Date
  WikiDataManager.fields.each do |field|
    field field.to_sym, default: ""
  end
  field :wiki_start_date, type: Date
  field :build_jobs, type: Array, default: []
  field :build_start_date, type: Date
  field :drawables, type: Hash, default: Hash.new
  field :path

  embeds_one :bugs_data, class_name: 'BugsData'
  has_many :commits_data, class_name: 'CommitsData'
  has_many :review_requests_data, class_name: 'ReviewRequestsData'
  has_many :mailing_messages_data, class_name: 'MailingMessagesData'
  has_many :irc_messages_data, class_name: 'IrcMessagesData'
  has_many :facebook_posts_data, class_name: "FacebookPostsData"
  has_many :facebook_doers_data, class_name: "FacebookDoersData"
  has_many :twitter_tweets_data, class_name: "TwitterTweetsData"
  has_many :twitter_doers_data, class_name: "TwitterDoersData"
  has_many :google_plus_posts_data, class_name: "GooglePlusPostsData"
  has_many :google_plus_doers_data, class_name: "GooglePlusDoersData"

  # The project must have a title.
  validates :title, presence: true

  # Adding a many to many relationship between projects and users.
  has_and_belongs_to_many :users

  # After creating the project, fetch needed data to produce graphical reports and save them in the cache.
  after_save :do_jobs

  # Friendly URLs. (Replaced by mongoid-slug)
  # extend FriendlyId
  # friendly_id :path
  slug :path

  before_save :compute_path

  def compute_path
    self.path = ancestors.reverse.push(self).collect{|p| p.title.downcase}.join("-")
    self.build_slug
  end

  # Call the custom job to prepare reports.
  def do_jobs
    job_callers = ["community_wiki_description_page","bugzilla_products","bugs_report_fields","git_repos","mailing_lists","irc_channels","facebook_pages","twitter_profile","google_plus_profiles","forum_ids","reviewboard_repository","build_jobs"]
    Delayed::Job.enqueue(Dj.new(id,changes)) unless (changes.keys & job_callers).empty?
  end

# ----------------------------------------------------------------------------------------------------------------------------

  # General:

  # Uses the value in community_wiki_description_page to generate a large description.
  def update_large_description_with_wiki_content
    return if community_wiki_description_page.empty?
    self.large_description = MediawikiAdapter.get_page_content "community", community_wiki_description_page
    save
  end

  # Returns an array of possible pie charts.
  def self.pie_charts_periods
    PieChart.pie_charts_periods
  end

  def self.pie_charts_dates
    PieChart.pie_charts_dates
  end

  def get_products_of type
    case type
    when "bugs"
      return bugzilla_products
    when "build"
      return build_jobs
    else
      return [] unless Constants::TYPES_TO_PRODUCT_FIELDS[type] # Dot needs this.
      return get_array_from_text(self[Constants::TYPES_TO_PRODUCT_FIELDS[type]])
    end
  end

  def get_attributes_for type
    return [[],[]] if all_kde_project?
    field = self[Constants::TYPES_TO_PRODUCT_FIELDS[type]]
    if field.class == String
      return [[],get_array_from_text(field)]
    else
      return [[],field]
    end
  end

  # Path to be used while creating the json files within the app.
  def path_for type
    path = "public"
    path += display_path_for type
    FileUtils.mkdir_p(path)
    path
  end

  def display_path_for type
    Rails.application.routes.url_helpers.project_path("en",self) + "/#{type}/"
  end

  def set_start_date type, date
    self["#{type}_start_date"] = date
    save
  end

  def set_drawables type, data
    drawables[type] = Hash.new
    data.each do |key,value|
      new_key = key.partition("_").first
      if value.class == Hash
        drawables[type][new_key] = true
      else
        next if value.length == 0
        drawables[type][new_key] = false
      end
    end
    save
  end

  # Project List Generators:

  # These are functions that are used to generate projects from xml file (projects.kde.org/kde_projects.xml)


  # Helper that generates a sub project while parsing kde_projects.xml
  def self.sub_projects_generator modul,sparent
    projects = modul.xpath("project")
    projects.each do |proj|
      title = proj.xpath("name").text
      ssp = sparent.children.find_or_create_by(title: title)
      ssp.large_description = ssp.description = proj.xpath("description").text
      if proj.xpath("repo").size != 0
        ssp.git_repos = proj.xpath("repo//url")[0].text[22..-1] + ".git" # Could be better if with regular expressions.
        ssp.pko_repository_link = proj.xpath("repo//web")[0].text
      end
      ssp.save
      unless proj.xpath("project").size == 0
        Project.sub_projects_generator proj, ssp
      end
    end
  end

  # Generates all projects from kde_projects.xml.
  def self.generate_all_projects
    content = Nokogiri::XML(open_link('http://projects.kde.org/kde_projects.xml'))
    components = content.xpath("//component")
    debug = ""
    components.each do |component|
      title = component.xpath("name").text
      p = Project.find_or_create_by(title: title)
      p.large_description = p.description = component.xpath("description").text
      if component.xpath("repo").size != 0
        p.git_repos = component.xpath("repo//url")[0].text[22..-1] + ".git" # Could be better if with regular expressions.
        p.pko_repository_link = component.xpath("repo//web")[0].text
      end
      p.save
      parent = p.id
      modules = component.xpath("module")
      modules.each do |modul|
        title = modul.xpath("name").text
        sp = p.children.find_or_create_by(title: title)
        sp.large_description = sp.description = modul.xpath("description").text
        #sp.parent_id = parent
        if modul.xpath("repo").size != 0
          sp.git_repos = modul.xpath("repo//url")[0].text[22..-1] + ".git" # Could be better if with regular expressions.
          sp.pko_repository_link = modul.xpath("repo//web")[0].text
        end
        sp.save
        unless modul.xpath("project").size == 0
          sparent = sp
          Project.sub_projects_generator modul, sparent
        end
      end
    end
    return debug
  end

  def self.reviewboard_periodic_update
    ReviewRequestsData.periodic_update
  end

  # These are functions which are used to generate and view general project report.

  # Returns a hash containing data needed to generate a general project report.
  def project_report
    res = Hash.new()
    res["bugs"] = bugs_general_report if bugs_data
    res["commits"] = commits_general_report if commits_start_date
    res["lists"] = lists_general_report if mailing_messages_start_date
    return res
  end

  # Returns a hash used to be used for drawing line graphs.
  def monthly_graph_hash
    res = Hash.new()
    temp = 1.year.ago.mon
    for i in 0...13
      res[(temp+i)%13] = 0 unless (temp+i)%13 == 0
    end
    return res
  end

  # Returns a complex hash to be used to generate pie charts where each user has a unique color for all charts.
  def self.pie_chart_colors data,unique_colors = Hash.new(nil)
    PieChart.colored_pie_charts data, unique_colors
  end

  def self.pie_chart_colors_multiple data
    res = Hash.new
    unique_colors = Hash.new(nil)
    data.each do |key,value|
      res[key] = Project.pie_chart_colors value,unique_colors
    end
    res
  end

  def self.start_date_for_all type
    start_date = Date.today
    Project.all.each do |d|
      this_date = d["#{type}_start_date"]
      next unless this_date
      start_date = this_date if this_date < start_date
    end
    start_date
  end

  # Sets starting dates for the All KDE project to the earliest date in each category.
  def set_all_kde_start_dates
    self.commits_start_date = Project.start_date_for_all "commits"
    self.mailing_messages_start_date = Project.start_date_for_all "mailing_messages"
    self.irc_messages_start_date = Project.start_date_for_all "irc_messages"
    self.review_requests_start_date = Project.start_date_for_all "review_requests"
  end

  def self.all_kde_project
    Project.find_or_create_by(title: "KDE Community")
  end

  def all_kde_project?
    title == "KDE Community"
  end

  def self.initial_all_kde_preparation
    p = self.all_kde_project
    p.bugzilla_products = Project.list_selectable_products
    p.set_all_kde_start_dates
    p.save
    Project.prepare_all_kde_files "commits"
    Project.prepare_all_kde_files "mailing_messages"
    Project.prepare_all_kde_files "irc_messages"
    ReviewRequestsData.prepare_files p
  end

  # Returns the project path to be used in files generation.
  # It guarantees that the returned path exists.
  def project_files_path type
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/#{type}/"
    FileUtils.mkdir_p(project_path)
    project_path
  end

  # Prepares all files needed to generate report for certain type for a project.
  def prepare_report_files type, do_all = true
    prepare_pie_charts type
    prepare_line_graphs type
    Project.prepare_all_kde_files type if do_all
    return nil
  end

  # Prepares all files needed to generate report for KDE Community project.
  def self.prepare_all_kde_files type
    p = Project.find_or_create_by(title: "KDE Community")
    p.prepare_pie_charts type, true
    p.prepare_line_graphs type, true
    p.commits_last_updated_time = Time.now
    p.save
    return nil
  end

  # prepares the data needed to show the type pie charts.
  def prepare_pie_charts type, all = false, special_function = nil, suffix = nil
    periods = Project.pie_charts_periods
    dates = Project.pie_charts_dates
    res = Hash.new
    if special_function
      periods.each do |period|
        res[period] = improved_pie_chart(self.send(special_function.to_sym,period,all))
      end
    else
      periods.each do |period|
        res[period] = improved_pie_chart(get_doers_chart(type,dates[period],all))
      end
    end
    colors = Project.pie_chart_colors res
    add_pie_cache_data type,res,colors, suffix
    return nil
  end

  # Adds to the public folder files needed to show the type pie charts.
  def add_pie_cache_data type,res,colors,suffix = nil
    self.drawables[type] = Hash.new(nil) unless suffix
    self.save!
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/#{type}/"
    FileUtils.mkdir_p(project_path)
    res.each do |key,value|
      new_key = key.partition("_").first
      new_key+= "_#{suffix}" if suffix
      if value.class == Hash
        File.open(project_path+"#{new_key}_all.json",'w') do |f|
          temp = Hash.new
          temp["data"] = value["all"]
          temp["colors"] = colors[key]["all"]
          f << temp.to_json
        end
        File.open(project_path+"#{new_key}_top_10.json",'w') do |f|
          temp = Hash.new
          temp["data"] = value["top_10"]
          temp["colors"] = colors[key]["top_10"]
          f << temp.to_json
        end
        self.drawables[type][new_key] = true
        self.save!
      else
        return if value.length == 0
        File.open(project_path+"#{new_key}.json",'w') do |f|
          temp = Hash.new
          temp["data"] = value
          temp["colors"] = colors[key]
          f << temp.to_json
        end
        self.drawables[type][new_key] = false
        self.save!
      end
    end
    self.save!
    return nil
  end

  # Adds the data needed to show the type line graphs to the public folder.
  def prepare_line_graphs type, all = false
    graphs = [get_objects_line_chart(type,all),get_daily_doers_line_chart(type,all),get_monthly_doers_line_chart(type,all),get_yearly_doers_line_chart(type,all)]
    types = ["objects","doers_daily","doers_monthly","doers_yearly"]
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/#{type}/"
    FileUtils.mkdir_p(project_path)
    graphs.zip(types).each do |graph,graph_type|
      File.open(project_path+"#{graph_type}.json",'w') do |f|
        f << graph.to_json
      end
    end
    prepare_average_reply_file(project_path, type) if report_has_reply? type
    return nil
  end

  def prepare_average_reply_file project_path, type
    File.open(project_path + "average_time_to_first_reply.json",'w') do |f|
      f << get_average_reply_line(type).to_json
    end
  end

  def add_line_graph_files type, all_lines
    project_path = project_files_path type
    FileUtils.mkdir_p(project_path)
    all_lines.each do |key,value|
      File.open(project_path+key+".json",'w') do |f|
        f << value.to_json
      end
    end
    return nil
  end

  # Prepares the files needed to show pie charts when they're multiple.
  def prepare_multiple_pie_charts type, project_path
    self.drawables[type] = Hash.new(nil)
    self.save!
    objects = get_hash_of_requested type
    data = Hash.new
    function = "get_#{type}_doers_chart"
    objects.each do |obj|
      periods = Project.pie_charts_periods
      data[obj] = Hash.new
      periods.each do |period|
        data[obj][period] = improved_pie_chart(self.send(function, period, obj, false))
      end
    end
    colors = Project.pie_chart_colors_multiple data
    res = Hash.new
    data.each do |k,v|
      v.each do |key,value|
        next if value.length == 0
        new_key = key.to_s + "_" + k
        if value.class == Hash
          File.open(project_path+new_key+"_all.json",'w') do |f|
            res["data"] = value["all"]
            res["colors"] = colors[k][key]["all"]
            f << res.to_json
          end
          File.open(project_path+new_key+"_top_10.json",'w') do |f|
            res["data"] = value["top_10"]
            res["colors"] = colors[k][key]["top_10"]
            f << res.to_json
          end
          self.drawables[type][new_key] = true
          self.save!
        else
          File.open(project_path+new_key+".json",'w') do |f|
            res["data"] = value
            res["colors"] = colors[k][key]
            f << res.to_json
          end
          self.drawables[type][new_key] = false
          self.save!
        end
      end
    end
    return nil
  end

   # Adds the data needed to show the bugs report the public folder.
  def prepare_bugs_report_files
    self.drawables["bugs"] = Hash.new(nil)
    self.save!
    data = get_all_bugs_data.attributes.dup.except!("total_number","_id","created_at","updated_at","start_date","total_average_close_time")
    data["bugs_by_status"] = data.delete "status" if data["status"] # There's a problem with using status in HighCharts.
    all_lines = Hash.new
    bugs_line = {
      "Reported Bugs" => data.delete("reported_line"),
      "Still Open Bugs" => data.delete("still_open_line"),
      "Resolved Bugs" => data.delete("resolved_line")
    }
    all_lines["rep_res"] = bugs_line
    all_lines["average_close_time"] = data.delete "average_close_time_line"
    all_lines["resolved_after_a_year"] = data.delete "resolved_after_a_year_line"
    all_lines["net_open"] = data.delete "net_open_line"
    #all_lines["still_open"] = data.delete "still_open_line"
    all_lines["total_created"] = data.delete "total_created_line"
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/bugs/"
    FileUtils.mkdir_p(project_path)
    all_lines.each do |key,value|
      File.open(project_path+key+".json",'w') do |f|
        f << value.to_json
      end
    end
    data.each do |key,value|
      next if value.length == 0
      improved = improved_pie_chart value
      if improved.class == Hash
        File.open(project_path+key+"_all.json",'w') do |f|
          f << improved["all"].to_json
        end
        File.open(project_path+key+"_top_10.json",'w') do |f|
          f << improved["top_10"].to_json
        end
        self.drawables["bugs"][key] = true
        self.save!
      else
        File.open(project_path+key+".json",'w') do |f|
          f << improved.to_json
        end
        self.drawables["bugs"][key] = false
        self.save!
      end
    end
    return nil
  end

  # Given a Ruby array, it transforms it into a string. This is used to represent the array in JSON format.
  def self.convert_array_to_string_array to_convert
    begin
      return "["+"\"#{to_convert.html_safe.join('","')}\""+"]"
    rescue
      return []
    end
  end

  def improved_pie_chart original
    lower_max_number = 15 #TODO: This can be made specific setting per project.
    upper_max_number = 100
    the_length = original.length
    return original if the_length < lower_max_number
    res = Hash.new()
    res["all"] = original.dup unless the_length > upper_max_number
    res["top_10"] = original.pop(10)
    others = 0
    original.each do |item|
      others+= item[1]
    end
    res["top_10"] << ["others",others]
    return res unless the_length > upper_max_number
    res["top_10"]
  end

  # Appends a zero to the end of the array in the given cache_key value. Used in regular updates.
  def append_zero cache_key, num = 0
    old = Rails.cache.read(cache_key+id.to_s)
    #TODO: All should be strings later.
    if old.class == String
      old = eval(old)
      is_s = true
    end
    if num == 0
      old << 0
    else
      old << old[-1]
    end
    if is_s
      Rails.cache.write(cache_key+id.to_s,old.inspect)
    else
      Rails.cache.write(cache_key+id.to_s,old)
    end
  end

  # Increments the last element of the array in the given cache_key value. Used when a new thing is added.
  def increment_last cache_key
    old = Rails.cache.read(cache_key+id.to_s)
    old[-1]+= 1
    Rails.cache.write(cache_key+id.to_s,old)
  end

  def increment_last_db data,key
    old = data[key]
    old[-1]+= 1
    data[key] = old
  end

  # Increments the last element of the array in the given cache_key value. Used when a new thing is added.
  def decrement_last cache_key
    old = Rails.cache.read(cache_key+id.to_s)
    old[-1]-= 1
    Rails.cache.write(cache_key+id.to_s,old)
  end

  def decrement_last_db data,key
    old = eval(data[key])
    old[-1]-= 1
    data[key] = old.inspect
  end

  # This function runs daily. It generates up-to-date reports.
  def self.daily_update
    Project.ne(:title => "KDE Community").each do |project|
      begin
        project.update_bugs_line_graphs_daily unless project.bugzilla_products.length == 0
        unless project.git_repos.length == 0
          project.prepare_report_files("commits", false)
          project.prepare_lines_of_code_files
        end
        ReviewRequestsData.prepare_files project unless project.reviewboard_repository.length == 0
        project.prepare_report_files("mailing_messages", false) unless project.mailing_lists.length == 0
        project.prepare_report_files("irc_messages", false) unless project.irc_channels.length == 0
      rescue
        File.open("failing/daily/"+Time.now.strftime("%d%m%H%M")+"#{project.title}.error", 'wb') do |file|
          file << $!.backtrace
        end
      end
    end
    ["commits","mailing_messages","irc_messages"].each do |type|
      Project.prepare_all_kde_files type
    end
    all_kde = Project.all_kde_project
    all_kde.update_bugs_line_graphs_daily
    all_kde.prepare_lines_of_code_files
    WikiDataManager.daily_update
    BuildDataManager.daily_update
  end

  # Monthly data can't be represented as simple as daily ones because the time between data entries is not constant.
  # We need to specify the time for each entry.
  def self.convert_raw_data_to_monthly_data raw_data, first_date
    return unless raw_data
    res = "["
    start_date = first_date
    i = -1 # Javascript month 0 = January
    raw_data.each do |entry|
      res+= "[Date.UTC(#{start_date.year},#{start_date.month+i}, 1),#{entry}],"
      i+= 1
    end
    res+= "]"
  end

  # Yearly data can't be represented as simple as daily ones because the time between data entries is not constant.
  # We need to specify the time for each entry.
  def self.convert_raw_data_to_yearly_data raw_data, first_date
    return unless raw_data
    res = "["
    start_date = first_date
    i = 0
    raw_data.each do |entry|
      res+= "[Date.UTC(#{start_date.year + i}, 0, 1),#{entry}],"
      i+= 1
    end
    res+= "]"
  end

  def get_appropriate_entries type, all = false
    case type
    when "commits"
      return commits_data unless all
      return CommitsData.all
    when "review_requests"
      return review_requests_data unless all
      return ReviewRequestsData.all
    when "mailing_messages"
      return mailing_messages_data unless all
      return MailingMessagesData.all
    when "irc_messages"
      return irc_messages_data unless all
      return IrcMessagesData.all
    when "planet_posts"
      return PlanetPostsData.all
    when "dot_posts"
      return DotPostsData.all
    when "facebook_posts-objects"
      return facebook_posts_data unless all
      return FacebookPostsData.all
    when "facebook_posts"
      return facebook_doers_data unless all
      return FacebookDoersData.all
    when "twitter_tweets-tweets"
      return twitter_tweets_data.where(:is_tweet => true) unless all
      return TwitterTweetsData.where(:is_tweet => true)
    when "twitter_tweets-retweets"
      return twitter_tweets_data.where(:is_tweet => false) unless all
      return TwitterTweetsData.where(:is_tweet => false)
    when "twitter_tweets"
      return twitter_doers_data unless all
      return TwitterDoersData.all
    when "google_plus_posts-objects"
      return google_plus_posts_data unless all
      return GooglePlusPostsData.all
    when "google_plus_posts"
      return google_plus_doers_data unless all
      return GooglePlusDoersData.all
    when "forum_posts"
      return forum_posts_data unless all
      return ForumPostsData.all
    end
  end

  # Returns the value of the start date for the report corresponding
  # to the given element.
  def get_start_date_for type
    case type
    when "commits"
      return commits_start_date
    when "review_requests"
      return review_requests_start_date
    when "facebook_posts-objects", "facebook_posts"
      return facebook_posts_start_date
    when "twitter_tweets-tweets", "twitter_tweets-retweets", "twitter_tweets"
      return twitter_tweets_start_date
    when "google_plus_posts-objects", "google_plus_posts"
      return google_plus_posts_start_date
    when "forum_posts"
      return forum_posts_start_date
    when "mailing_messages"
      return mailing_messages_start_date
    when "irc_messages"
      return irc_messages_start_date
    end
  end

  # Returns the entries to search through for the given element in the project.
  def get_explorable_list_for type
    case type
    when "mailing_messages"
      return mailing_lists
    when "irc_messages"
      return irc_channels
    when "forum_posts"
      return forum_ids
    end
  end

  def get_hash_of_requested type
    res = {"facebook_posts" => ["likes","comments"],
           "twitter_tweets" => ["retweets"],
           "google_plus_posts" => ["plusones","replies","reshares"],
           "review_requests" => ["created","submitted","discarded"]
    }
    res[type]
  end

  def get_hash_of_doers type
    res = {
      "facebook_posts" => ["likers","commenters"],
      "google_plus_posts" => ["plusoners","repliers","resharers"]
    }
    res[type]
  end

  # Returns an array with zeros from the start date of the type.
  # Used to handle the case where there are no entries yet(can happen in IRC for example).
  def empty_entries_array type, start_date = nil
    start_date ||= get_start_date_for(type)
    return Array.new((Date.today - start_date).to_i + 1,0)
  end

  # Returns the objects line chart for the project.
  def get_objects_line_chart type, all = false, object = "number", start_date = nil
    entries = get_appropriate_entries type,all
    return empty_entries_array(type, start_date) if entries.empty?
    if object.class == Fixnum
      map = %Q{
        function() {
          emit(this.day,#{object});
        };
      }
    else
      map = %Q{
        function() {
          emit(this.day,this.#{object});
        };
      }
    end
    reduce = %Q{
      function(key,values) {
        return Array.sum(values);
      };
    }
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    return if data.length == 0
    res = Array.new
    # This is needed for the first iteration of the loop to work correctly.
    if start_date
      prev_date = start_date.to_date
    else
      prev_date = data.first["_id"].to_date
    end
    prev_date-= 1
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res += Array.new(num_zeros,0)
      res << entry["value"].to_i
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros =  (cur_date - prev_date).to_i
    res += Array.new(num_zeros,0)
  end

  # Returns the objects line chart for the project.
  def get_multiple_objects_line_charts type, all = false, start_date = nil
    # type is the key. Value is a hash, its keys represent objects being counted, values represent the values in DB entry to count. true means add 1 for each entry. false means count its value from column.
    lines = {
      "facebook_posts-objects" => {"posts" => true,"likes_count" => false,"comments_count" => false,"shares_count" => false},
      "twitter_tweets-tweets" => {"tweets" => true,"retweet_count" => false,"favorite_count" => false},
      "twitter_tweets-retweets" => {"retweets" => true,"retweet_count" => false,"favorite_count" => false},
      "google_plus_posts-objects" => {"posts" => true, "plusones_count" => false, "replies_count" => false, "reshares_count" => false},
      "forum_posts" => {"threads" => false, "posts" => false, "resolved" => false},
      "dot_posts" => {"posts" => false, "comments_count" => false},
      "review_requests" => {"number" => false, "number_submitted" => false, "number_discarded" => false}
    }
    res = Hash.new
    lines[type].each do |key,value|
      res[key] = get_objects_line_chart type, all, (value ? 1 : key), start_date
    end
    return res
  end

  # Returns the daily doers line chart.
  def get_daily_doers_line_chart type, all = false
    entries = get_appropriate_entries type,all
    return empty_entries_array(type) if entries.empty?
    map = %Q{
      function() {
        emit(this.day,this.contributor);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.unique(values).join(',');
      };
    }
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    return if data.length == 0
    res = Array.new
    prev_date = data.first["_id"].to_date - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res += Array.new(num_zeros,0)
      res << entry["value"].split(',').uniq.length
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros = (cur_date - prev_date).to_i
    res += Array.new(num_zeros,0)
  end

  # Returns the monthly doers line chart.
  def get_monthly_doers_line_chart type, all = false
    entries = get_appropriate_entries type,all
    return empty_entries_array(type) if entries.empty?
    map = %Q{
      function() {
        date = this.day;
        date = new Date(date.getFullYear(),date.getMonth(),2);
        emit(date,this.contributor);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.unique(values).join(',');
      };
    }
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    return if data.length == 0
    res = Array.new
    prev_date = data.first["_id"].to_date - 1.month # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date.year*12 + cur_date.month) - (prev_date.year*12 + prev_date.month) - 1
      res += Array.new(num_zeros,0)
      res << entry["value"].split(',').uniq.length
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros = (cur_date.year*12 + cur_date.month) - (prev_date.year*12 + prev_date.month)
    res += Array.new(num_zeros,0)
  end

  # Returns the yearly doers line chart.
  def get_yearly_doers_line_chart type, all = false
    entries = get_appropriate_entries type,all
    return empty_entries_array(type) if entries.empty?
    map = %Q{
      function() {
        date = this.day;
        date = new Date(date.getFullYear(),1,1);
        emit(date,this.contributor);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.unique(values).join(',');
      };
    }
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    return if data.length == 0
    res = Array.new
    prev_date = data.first["_id"].to_date.year - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date.year
      num_zeros = cur_date - prev_date - 1
      if num_zeros < 0
        return entry
      end
      res += Array.new(num_zeros,0)
      res << entry["value"].split(',').uniq.length
      prev_date = cur_date
    end
    cur_date = Date.today.year
    num_zeros = cur_date - prev_date
    res += Array.new(num_zeros,0)
  end


  def get_daily_doers_single_line_chart type,counting=nil, all = false
    entries = get_appropriate_entries type,all
    return empty_entries_array(type) if entries.empty?
    map = %Q{
      function() {
        if (this.#{counting} > 0) {
          emit(this.day,this.doer);
        }
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.unique(values).join(',');
      };
    }
    return [] if entries.empty?
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    return if data.length == 0
    res = Array.new
    prev_date = self["#{type}_start_date"].to_date - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res += Array.new(num_zeros,0)
      res << entry["value"].split(',').uniq.length
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros = (cur_date - prev_date).to_i
    res += Array.new(num_zeros,0)
  end


  # Returns the monthly doers line chart.
  def get_monthly_doers_single_line_chart type,counting=nil, all = false
    entries = get_appropriate_entries type,all
    return empty_entries_array(type) if entries.empty?
    map = %Q{
      function() {
        date = this.day;
        date = new Date(date.getFullYear(),date.getMonth(),2);
        if (this.#{counting} > 0) {
          emit(date,this.doer);
        }
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.unique(values).join(',');
      };
    }
    return [] if entries.empty?
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    return if data.length == 0
    res = Array.new
    prev_date = self["#{type}_start_date"].to_date - 1.month # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date.year*12 + cur_date.month) - (prev_date.year*12 + prev_date.month) - 1
      res += Array.new(num_zeros,0)
      res << entry["value"].split(',').uniq.length
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros = (cur_date.year*12 + cur_date.month) - (prev_date.year*12 + prev_date.month)
    res += Array.new(num_zeros,0)
  end

  def get_yearly_doers_single_line_chart type, counting=nil, all = false
    entries = get_appropriate_entries type,all
    return empty_entries_array(type) if entries.empty?
    map = %Q{
      function() {
        date = this.day;
        date = new Date(date.getFullYear(),1,1);
        if (this.#{counting} > 0) {
          emit(date,this.doer);
        }
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.unique(values).join(',');
      };
    }
    return [] if entries.empty?
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    return if data.length == 0
    res = Array.new
    prev_date = self["#{type}_start_date"].year - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date.year
      num_zeros = cur_date - prev_date - 1
      if num_zeros < 0
        return entry
      end
      res += Array.new(num_zeros,0)
      res << entry["value"].split(',').uniq.length
      prev_date = cur_date
    end
    cur_date = Date.today.year
    num_zeros = cur_date - prev_date
    res += Array.new(num_zeros,0)
  end

  # Returns the daily doers multiple lines chart.
  def get_daily_doers_multiple_lines_chart type, all = false
    entries = get_appropriate_entries type,all
    requested = get_hash_of_requested type
    doers = get_hash_of_doers type
    map = %Q{
      function() {
        #{javascript_map_filler_function(requested)}
        emit(this.day,res);
      };
    }
    reduce = %Q{
      function(key,values) {
        var len = values[0].length;
        var res = values[0];
        for (var i = 1; i < values.length; i++) {
          for (var j = 0; j < len; j++) {
              res[j]+= values[i][j];
          };
        };
        return res.toString();
      };
    }
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    return if data.length == 0
    res = Hash.new
    doers.each do |i|
      res[i] = Array.new
    end
    prev_date = (self["#{type}_start_date"]).to_date - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res.update(res){|k,v| v+= Array.new(num_zeros,0)}
      if entry["value"].class == String
        the_array = eval("[#{entry["value"]}]")
        res.update(res){|k,v| v << the_array[doers.index(k)].to_i}
      else
        res.update(res){|k,v| v << (entry["value"][doers.index(k)]).to_i}
      end
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros = (cur_date - prev_date).to_i
    res.update(res){|k,v| v+= Array.new(num_zeros,0)}
    return res
  end

  # Returns the monthly doers multiple lines chart.
  def get_monthly_doers_multiple_lines_chart type, all = false
    entries = get_appropriate_entries type,all
    requested = get_hash_of_requested type
    doers = get_hash_of_doers type
    map = %Q{
      function() {
        date = this.day;
        date = new Date(date.getFullYear(),date.getMonth(),2);
        #{javascript_map_filler_function(requested)}
        emit([date,this.doer],res);
      };
    }
    reduce = doers_reduce
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    return if data.length == 0
    res = Hash.new
    doers.each do |i|
      res[i] = Array.new
    end
    prev_date = (self["#{type}_start_date"]).to_date - 1.month # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"][0].to_date
      num_zeros = (cur_date.year*12 + cur_date.month) - (prev_date.year*12 + prev_date.month) - 1
      if num_zeros == -1
        if entry["value"].class == String
          the_array = eval("[#{entry["value"]}]")
          res.update(res){|k,v| v[0...-1] + [v[-1]+= the_array[doers.index(k)].to_i]}
        else
          res.update(res){|k,v| v[0...-1] + [v[-1]+= (entry["value"][doers.index(k)]).to_i]}
        end
      else
        res.update(res){|k,v| v+= Array.new(num_zeros,0)}
        if entry["value"].class == String
          the_array = eval("[#{entry["value"]}]")
          res.update(res){|k,v| v << the_array[doers.index(k)].to_i}
        else
          res.update(res){|k,v| v << (entry["value"][doers.index(k)]).to_i}
        end
      end
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros = (cur_date.year*12 + cur_date.month) - (prev_date.year*12 + prev_date.month)
    res.update(res){|k,v| v+= Array.new(num_zeros,0)}
    return res
  end

  # Returns the yearly doers multiple lines chart.
  def get_yearly_doers_multiple_lines_chart type, all = false
    entries = get_appropriate_entries type,all
    requested = get_hash_of_requested type
    doers = get_hash_of_doers type
    map = %Q{
      function() {
        date = this.day;
        date = new Date(date.getFullYear(),1,1);
        #{javascript_map_filler_function(requested)}
        emit([date,this.doer],res);
      };
    }
    reduce = doers_reduce
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    return if data.length == 0
    res = Hash.new
    doers.each do |i|
      res[i] = Array.new
    end
    prev_date = (self["#{type}_start_date"]).to_date.year - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"][0].to_date.year
      num_zeros = cur_date - prev_date - 1
      if num_zeros == -1
        if entry["value"].class == String
          the_array = eval("[#{entry["value"]}]")
          res.update(res){|k,v| v[0...-1] + [v[-1]+= the_array[doers.index(k)].to_i]}
        else
          res.update(res){|k,v| v[0...-1] + [v[-1]+= (entry["value"][doers.index(k)]).to_i]}
        end
      else
        res.update(res){|k,v| v+= Array.new(num_zeros,0)}
        if entry["value"].class == String
          the_array = eval("[#{entry["value"]}]")
          res.update(res){|k,v| v << the_array[doers.index(k)].to_i}
        else
          res.update(res){|k,v| v << (entry["value"][doers.index(k)]).to_i}
        end
      end
      prev_date = cur_date
    end
    cur_date = Date.today.year
    num_zeros = cur_date - prev_date
    res.update(res){|k,v| v+= Array.new(num_zeros,0)}
  end

  # Returns a doers lines chart using the above functions.
  def get_doers_lines type,period, all = false
    requested = get_hash_of_requested type
    doers = get_hash_of_doers type
    function = "get_#{period}_doers_single_line_chart"
    return self.send(function.to_sym,type,requested.first) if requested.length == 1
    res = Hash.new
    doers.zip(requested).each do |doer,counting|
      res[doer] = self.send(function,type,counting)
    end
    res
  end

  # Returns the pie chart that represents the number of contributions for each author in this type for the project.
  def get_doers_chart type, start_date = nil, all = false,contributor = "contributor", number = "number"
    entries = get_appropriate_entries type,all
    return [] if entries.empty?
    map = %Q{
      function() {
        emit(this.#{contributor},this.#{number});
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values);
      };
    }
    unless start_date
      data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    else
      data = entries.where(:day.gte => start_date).map_reduce(map, reduce).out(replace: "map-reduce").entries
    end
    return [] if data.length == 0
    data.sort_by! {|item| item["value"]}
    res = Array.new
    data.each do |entry|
      res << [entry["_id"],entry["value"].to_i] unless entry["value"] == 0
    end
    res
  end

  # Returns the average time in to first reply line.
  # type is the model to operate on, for e.g: MailingMessagesReply.
  # included is an array of
  def get_average_reply_line type
    type_class = reply_class_for type
    entries = type_class.all
    included = get_array_from_text(get_explorable_list_for(type))
    entries = entries.in(container: included) if included.size > 0
    entries = entries.ne(first_reply_period: nil)
    return empty_entries_array(type) if entries.empty?
    map = %Q{
      function() {
        var day = new Date(Date.UTC(this.time.getFullYear(),this.time.getMonth(),this.time.getDate()));
        emit(day,this.first_reply_period);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values)/values.length;
      };
    }
    data = entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
    data = data.sort_by { |entry| entry["_id"] }
    res = Array.new
    prev_date = get_start_date_for(type) - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res += Array.new(num_zeros,0)
      if entry["value"].class == BSON::Undefined # Nothing was replied to on that day.
        res << 0
      else
        res << entry["value"].to_f
      end
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros = (cur_date - prev_date)
    res += Array.new(num_zeros,0)
  end

# ----------------------------------------------------------------------------------------------------------------------------

  # Wiki Report:

  def set_wiki_start_date start_date
    self.wiki_start_date = start_date
    save
  end

  # Returns an array that contains the wikis that should be drawn for this project.
  def drawable_wikis
    return WikiDataManager.explorable_sites if all_kde_project?
    res = Array.new
    WikiDataManager.explorable_sites.each do |site|
      res << site unless self["#{site}_wiki_pages"].empty?
    end
    res
  end

  # Prepares the wiki report for all projects
  def self.prepare_wiki_report
    Project.each do |p|
      WikiDataManager.prepare_project_revisions_report p
    end
  end

  # Returns a hash with the total number of views for each of the sites.
  def total_wiki_views
    res = Hash.new
    WikiDataManager.explorable_sites.each do |site|
      res[site] = WikiPagesData.entries_for(self, site).sum(:views)
    end
    res
  end

  # Returns a hash with the total number of revisions for each of the sites.
  def total_wiki_revisions
    res = Hash.new
    WikiDataManager.explorable_sites.each do |site|
      res[site] = WikiRevisionsData.entries_for(self, site).sum(:number)
    end
    res
  end

# ----------------------------------------------------------------------------------------------------------------------------


  # Dot Report(dot.kde.org):

  def self.prepare_dot_data start_date = nil
    is_done = false
    data = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    num = 0
    while true
      link = "https://dot.kde.org/all-content.json?page=#{num}"
      items = ActiveSupport::JSON.decode(open_link(link))["nodes"]
      break if items.length == 0
      items.each do |node|
        item = node["node"]
        day = Time.at(item["Post timestamp"].to_i).utc.to_date
        if start_date
          if day < start_date
            is_done = true
            break
          end
        end
        category = item["Dot Categories"] ||= "Not Categorized"
        entry = data[{contributor: item["Author"], day: day, category: category}]
        entry["posts"]+= 1
        entry["comments_count"]+= item["Comment count"].to_i
      end
      num+= 1
      break if is_done
    end
    if start_date
      DotPostsData.where(:day.gte => start_date).destroy_all
    else
      DotPostsData.destroy_all
    end
    data.each do |key,value|
      entry = DotPostsData.create(key.merge(value))
    end
    nil
  end

  def self.update_dot_report start_date = 2.weeks.ago
    begin
      self.prepare_dot_data start_date
      Project.find_by(title: "KDE Community").prepare_dot_posts_files
    rescue
      File.open('failing/dot/'+Time.now.strftime("%d%m%H%M")+'.error', 'wb') do |file|
        file << $!.backtrace
      end
    end
    nil
  end

  def get_dot_doers_chart period, all = false
    dates = Project.pie_charts_dates
    get_doers_chart("dot_posts", dates[period], all, "contributor", "posts")
  end

  def get_dot_categories_chart period, all = false
    dates = Project.pie_charts_dates
    get_doers_chart("dot_posts", dates[period], all, "category", "posts")
  end

  # Prepares files needed to show the Dot report.
  def prepare_dot_posts_files
    all_lines = Hash.new
    all_lines["objects"] = get_multiple_objects_line_charts "dot_posts"
    all_lines["doers_daily"] = get_daily_doers_line_chart "dot_posts"
    all_lines["doers_monthly"] = get_monthly_doers_line_chart "dot_posts"
    all_lines["doers_yearly"] = get_yearly_doers_line_chart "dot_posts"
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/dot_posts/"
    FileUtils.mkdir_p(project_path)
    all_lines.each do |key,value|
      File.open(project_path+key+".json",'w') do |f|
        f << value.to_json
      end
    end
    self.drawables["dot_posts"] = Hash.new(nil)
    prepare_pie_charts "dot_posts", false, "get_dot_doers_chart", "posters"
    prepare_pie_charts "dot_posts", false, "get_dot_categories_chart", "categories"
  end

  # Planet Report(planetkde.org):

  # Adds all data needed to display a report about planetkde in the DB.
  def self.update_planet_report last_date = 1.day.ago.to_date
    begin
      PlanetPostsData.where(:day.gte => last_date).destroy_all
      link = "http://planetkde.org/rss20.xml"
      posts = RSS::Parser.parse(open_link(link),false,false).items
      posts.each do |post|
        day = post.pubDate.utc.to_date
        break if day < last_date
        contributor = post.author.force_encoding("UTF-8")
        entry = PlanetPostsData.find_or_create_by(day: day, contributor: contributor)
        entry["number"]+= 1
        entry.save
      end
      self.prepare_all_kde_files "planet_posts"
    rescue
      File.open('failing/planet/'+Time.now.strftime("%d%m%H%M")+'.error', 'wb') do |file|
        file << $!.backtrace
      end
    end
  end

  # Forum posts Report:

  # Returns the Forum posts data for this project
  def forum_posts_data
    ForumPostsData.where(fid: {'$in' => get_array_from_text(forum_ids)})
  end

  # Returns the Forums ids in the correct format to put in the link to fetch data.
  def get_forum_ids_for_link
    res = ""
    items = get_array_from_text(forum_ids)
    items.each do |item|
      res += "&fid[]=" + item.to_s
    end
    res
  end

  # Returns an array of forum ids for this project.
  def forum_ids_array
    get_array_from_text self.forum_ids
  end

  # Prepares all data needed to generate forum posts report for the project.
  def self.prepare_forum_posts_data
    res = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    entries = ["threads","posts","resolved"]
    params = ["sf=firstpost&","","sv=1&sf=firstpost&"]
    first = true
    entries.zip(params).each do |entry,param|
      num = 0
      while num < 20000
        link = "https://forum.kde.org/search.php?#{param}sd=d&st=0&feed_type=RSS2.0&feed_style=COMPACT&countlimit=100&start=#{num}&submit=Search"
        posts = RSS::Parser.parse(open_link(link),false,false).items
        done = false
        posts.each do |post|
          day = post.pubDate.utc
          if entry == "posts" && first
            GeneralObjects.set_last_updated "forum", day
            first = false
          end
          day = day.to_date
          if day.year < 2014
            done = true
            break
          end
          contributor = post.author.force_encoding("UTF-8")
          fid = get_fid_from_link post.link
          res[[day,contributor,fid]][entry]+= 1
          ForumPostsReply.process_post post, entry
        end
        break if done
        num+= 100
      end
    end
    ForumPostsData.destroy_all
    res.each do |key,value|
      entry = ForumPostsData.new(value)
      entry.day = key[0]
      entry.contributor = key[1]
      entry.fid = key[2]
      entry.save
    end
    ForumDataManager.prepare_choices_list
  end

  def self.clear_forum_data start_date
    ForumPostsData.where(:day.gte => start_date).destroy_all
    ForumPostsReply.where(:day.gte => start_date).destroy_all
  end

  def self.update_forum_posts_data start_date = 2.days.ago.to_date
    begin
      clear_forum_data start_date
      entries = ["threads","posts","resolved"]
      params = ["sf=firstpost&","","sv=1&sf=firstpost&"]
      affected = Array.new
      entries.zip(params).each do |entry,param|
        num = 0
        while num < 20000
          link = "https://forum.kde.org/search.php?#{param}sd=d&st=0&feed_type=RSS2.0&feed_style=COMPACT&countlimit=100&start=#{num}&submit=Search"
          posts = RSS::Parser.parse(open_link(link),false,false).items
          done = false
          posts.each do |post|
            day = post.pubDate.utc.to_date
            if day < start_date
              done = true
              break
            end
            contributor = post.author.force_encoding("UTF-8")
            fid = get_fid_from_link post.link
            affected << fid
            db_entry = ForumPostsData.find_or_create_by(day: day, contributor: contributor, fid: fid)
            db_entry[entry]+= 1
            db_entry.save
            ForumPostsReply.process_post post, entry
          end
          break if done
          num+= 100
        end
      end
      affected = affected.uniq
      Project.ne(:forum_ids => "").each do |project|
        project.prepare_forum_posts_files unless (affected & project.forum_ids_array).empty?
      end
      Project.find_by(title: "KDE Community").prepare_forum_posts_files(true) unless affected.empty?
      ForumDataManager.prepare_choices_list
    rescue
      File.open('failing/forums/'+Time.now.strftime("%d%m%H%M")+'.error', 'wb') do |file|
        file << $!.backtrace
      end
    end
  end

  def self.update_forum_resolved start_from = 1.month.ago
    begin
      affected = Array.new()
      res = Hash.new { |hash,key| hash[key] = Hash.new(0) }
      num = 0
      while num < 20000
        link = "https://forum.kde.org/search.php?sv=1&sf=firstpost&sd=d&st=0&feed_type=RSS2.0&feed_style=COMPACT&countlimit=100&start=#{num}&submit=Search"
        posts = RSS::Parser.parse(open_link(link),false,false).items
        done = false
        posts.each do |post|
          day = post.pubDate.utc
          if day <= start_from
            done = true
            break
          end
          day = day.to_date
          contributor = post.author.force_encoding("UTF-8")
          fid = get_fid_from_link post.link
          affected << fid
          entry = res[{day: day,contributor: contributor,fid: fid}]
          entry["resolved"]+= 1
        end
        break if done
        num+= 100
      end
      res.each do |key,value|
        entry = ForumPostsData.find_or_create_by(key)
        entry.update_attributes(value)
      end
      Project.ne(:forum_id => "").each do |project|
        project.prepare_forum_posts_files unless (affected & project.forum_ids_array).empty?
      end
      Project.find_by(title: "KDE Community").prepare_forum_posts_files(true) unless affected.empty?
    rescue
      open('failing/forum/'+Time.now.strftime("%d%m%H%M")+'.resolvers-error', 'wb') do |file|
        file << $!.backtrace
      end
    end
  end

  # Prepares all data needed to generate forum posts report for the project.
  # TODO: Remove this after the above works.
  def self.prepare_forum_topics_data
    res = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    resolved_topics = Hash.new(false)
    num = 0
    while num < 20000
      link = "https://forum.kde.org/search.php?sv=1&sd=a&st=0&sf=titleonly&feed_type=RSS2.0&feed_style=COMPACT&countlimit=100&start=#{num}&submit=Search"
      begin
        resolved_posts = RSS::Parser.parse(open_link(link),false,false).items
      rescue OpenURI::HTTPError
        break
      end
      resolved_posts.each do |post|
        tid = post.link.match(/https:\/\/forum.kde.org\/viewtopic.php\?f=\d+&t=(\d+)/)[1]
        resolved_topics[tid] = true
      end
      num+= 100
    end
    num = 0
    while num < 20000
      link = "https://forum.kde.org/search.php?sd=a&st=0&sf=titleonly&feed_type=RSS2.0&feed_style=COMPACT&countlimit=100&start=#{num}&submit=Search"
      begin
        posts = RSS::Parser.parse(open_link(link), false, false).items
      rescue OpenURI::HTTPError
        break
      end
      posts.each do |post|
        contributor = post.author
        day = post.pubDate.utc.to_date
        temp = post.link.match(/https:\/\/forum.kde.org\/viewtopic.php\?f=(\d+)&t=(\d+)/)
        fid = temp[1]
        tid = temp[2]
        entry = res[[day,contributor,fid]]
        topics[tid] = contributor
        entry["threads"]+= 1
        entry["resolved"]+= 1 if resolved_topics[tid]
      end
      num+= 100
    end
  end

  def prepare_forum_posts_start_date
    fids = get_array_from_text(forum_ids)
    self.forum_posts_start_date = ForumPostsData.where(fid: {'$in'=> fids }).min(:day)
    self.save
  end

  def get_forum_doers_chart period, all = false
    dates = Project.pie_charts_dates
    get_doers_chart("forum_posts", dates[period], all, "contributor", "posts")
  end

  # Prepares all files needed to display forums report.
  def prepare_forum_posts_files all = false
    all_lines = Hash.new
    all_lines["objects"] = get_multiple_objects_line_charts "forum_posts", all
    all_lines["doers_daily"] = get_daily_doers_line_chart "forum_posts", all
    all_lines["doers_monthly"] = get_monthly_doers_line_chart "forum_posts", all
    all_lines["doers_yearly"] = get_yearly_doers_line_chart "forum_posts", all
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/forum_posts/"
    FileUtils.mkdir_p(project_path)
    all_lines.each do |key,value|
      File.open(project_path+key+".json",'w') do |f|
        f << value.to_json
      end
    end
    prepare_average_reply_file project_path, "forum_posts"
    prepare_pie_charts "forum_posts", all, "get_forum_doers_chart"
  end

  # Returns the number of topics in the forums that belong to the project.
  def number_of_forums_topics
    ForumPostsData.where(fid: {'$in' => get_array_from_text(forum_ids)}).sum(:threads)
  end

  # Returns an array of the latest forum topics.
  def forum_topics page = 1, limit = 10
    link = "https://forum.kde.org/search.php?sd=d&st=0&sf=titleonly&feed_type=RSS2.0&feed_style=COMPACT&countlimit=#{limit}&start=#{limit*(page.to_i-1)}&submit=Search"
    link+= get_forum_ids_for_link if self.forum_ids.length != 0
    RSS::Parser.parse(open_link(link),false,false).items
  end

  # Google+ Report:

  # Prepares all data needed to display a Google+ Report.
  # previous_posts_interactions is the number of posts before start_from to check for interactions whose dates are after the date of start_from.
  def prepare_google_plus_data start_from = nil, previous_posts_interactions = 5
    ids = get_array_from_text(google_plus_profiles)
    self.google_plus_posts_data.destroy_all unless start_from
    doers_data = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    start_date = Date.today
    ids.each do |id|
      items = GooglePlus::Activity.for_person(id, :max_results => 100 )
      is_done = false
      items.each do |item|
        unless is_done
          day = item.published.to_time.utc.to_date
          if start_from
            is_done = true if day < start_from
          end
          object = item.object
          entry = self.google_plus_posts_data.find_or_create_by(post_id: item.id)
          entry.update_attributes(
            day: day,
            replies_count: object.replies.total_items,
            plusones_count: object.plusoners.total_items,
            reshares_count: object.resharers.total_items
          )
          item.plusoners.each do |doer|
            doers_data[[day,doer.display_name]]["plusones"]+= 1
          end
          item.resharers.each do |doer|
            doers_data[[day,doer.display_name]]["reshares"]+= 1
          end
          start_date = day
        end
        comments = item.list_comments
        comments.each do |comment|
          c_day = comment.published.to_time.utc.to_date
          if start_from
            break if c_day < start_from
          end
          author = comment.actor.display_name
          doers_data[[c_day,author]]["replies"]+= 1
        end
      end
    end
    self.google_plus_doers_data.destroy_all unless start_from
    doers_data.each do |key,value|
      entry = self.google_plus_doers_data.find_or_create_by(day: key.first, doer: key.last)
      entry.update_attributes(value)
    end
    self.google_plus_posts_start_date = start_date unless start_from
    self.save
  end

  def self.update_google_plus_report start_from = 2.weeks.ago, interactions_number = 5
    begin
      Project.ne(:google_plus_profiles => "").each do |p|
        p.prepare_google_plus_data start_from, interactions_number
        p.prepare_google_plus_files
      end
    rescue
      open('failing/google_plus/'+Time.now.strftime("%d%m%H%M")+'.error', 'wb') do |file|
        file << $!.backtrace
      end
    end
  end

  def get_google_plus_posts_doers_chart period, object, all = false
    dates = Project.pie_charts_dates
    get_doers_chart("google_plus_posts",dates[period],all,"doer",object)
  end

  # Prepares files needed to show the Google Plus report.
  def prepare_google_plus_files
    all_lines = Hash.new
    all_lines["objects"] = get_multiple_objects_line_charts "google_plus_posts-objects"
    all_lines["replies"] = get_objects_line_chart "google_plus_posts", false, "replies", google_plus_posts_start_date
    all_lines["doers_daily"] = get_doers_lines "google_plus_posts", "daily"
    all_lines["doers_monthly"] = get_doers_lines "google_plus_posts", "monthly"
    all_lines["doers_yearly"] = get_doers_lines "google_plus_posts", "yearly"
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/google_plus_posts/"
    FileUtils.mkdir_p(project_path)
    all_lines.each do |key,value|
      File.open(project_path+key+".json",'w') do |f|
        f << value.to_json
      end
    end
    prepare_multiple_pie_charts "google_plus_posts", project_path
  end

  # Twitter Report:

  # Returns a client that can be used to access Twitter API.
  def twitter_client stream = false
    config = {
      :consumer_key    => Tokens.twitter.consumer_key,
      :consumer_secret => Tokens.twitter.consumer_secret,
      :bearer_token => Tokens.twitter.bearer_token
    }
    return Twitter::REST::Client.new(config) unless stream
    return Twitter::Streaming::Client.new(config)
  end

  # Prepares all twitter data needed to produce a twitter report for the project's page.
  def prepare_twitter_data
    prepare_twitter_profile_id
    prepare_tweets_data
    prepare_retweeters_data
  end

  # Adds the twitter profile id to the DB to allow subscribing to it.
  def prepare_twitter_profile_id
    client = twitter_client
    id = client.user(self.twitter_profile).id
    self.twitter_profile_id = id
    self.save
  end

  # Prepares all tweets data needed to produce tweets graph for the project's page.
  def prepare_tweets_data start_from = nil
    client = twitter_client
    options = {count: 200}
    twitter_tweets_data.destroy_all unless start_from
    res_tweets = Hash.new
    tweets = client.user_timeline(twitter_profile,options)
    start_date = Date.today
    is_done = false
    while tweets.length > 0
      tweets.each do |tweet|
        is_tweet = true
        if tweet.retweeted_status?
          favorite_count = tweet.retweeted_status.favorite_count
          is_tweet = false
        else
          favorite_count = tweet.favorite_count
        end
        day = tweet.created_at.dup.utc.to_date
        if start_from
          if day < start_from
            is_done = true
            break
          end
        end
        entry = self.twitter_tweets_data.find_or_create_by(tweet_id: tweet.id)
        entry.update_attributes(
          day: day,
          favorite_count: favorite_count,
          retweet_count: tweet.retweet_count,
          is_tweet: is_tweet
        )
      end
      break if is_done
      options[:max_id] = tweets.last.id-1
      start_date = tweets.last.created_at.dup.to_date
      tweets = client.user_timeline(twitter_profile,options)
    end
    unless start_from
      self.twitter_tweets_start_date = start_date
      self.save
    end
  end

  def self.handle_new_tweet tweet
    begin
      Project.where(twitter_profile_id: tweet.user.id).each do |project|
        project.twitter_tweets_data << TwitterTweetsData.new(
                                                            tweet_id: tweet.id,
                                                            day: tweet.created_at.dup.utc.to_date,
                                                            is_tweet: !tweet.retweet?
                                                            )
        project.prepare_twitter_tweets_files
      end

      if tweet.retweet?
        original = tweet.retweeted_tweet
        TwitterTweetsData.where(tweet_id: original.id).each do |entry|
          entry.retweet_count = original.retweet_count
          entry.favorite_count = original.favorite_count
          entry.save
          entry.project.prepare_twitter_tweets_files
        end

        Project.where(twitter_profile_id: original.user.id).each do |project|
          entry = project.twitter_doers_data.find_or_create_by(doer: tweet.user.name, day: tweet.created_at.dup.utc.to_date)
          entry.retweets+= 1
          entry.save
          project.prepare_twitter_doers_files
        end
      end
    rescue
      File.open("failing_twitter/"+ Time.now.strftime("%d%m%H%M") + ".tweet","wb") do |file|
        file << tweet.id.to_s
      end
      File.open("failing_twitter/"+ Time.now.strftime("%d%m%H%M") + ".error","wb") do |file|
        file << $i.message.to_s
      end
    end
  end

  def self.update_twitter_report
    begin
      Project.ne(:twitter_profile => "").each do |p|
        p.prepare_tweets_data 2.weeks.ago
        p.prepare_twitter_files
      end
    rescue
      open('failing/twitter/'+Time.now.strftime("%d%m%H%M")+'.cron-error', 'wb') do |file|
        file << $!.backtrace
      end
    end
  end

  # Prepares retweeters data to be able to display doers charts
  def prepare_retweeters_data start_from = nil, safety_period = 1.month
    client = twitter_client
    res = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    reqs = 0
    all_ids = twitter_tweets_data.where(:is_tweet => true)
    all_ids = all_ids.where(:day.gte => start_from-safety_period) if start_from
    all_ids = all_ids.map { |item| item.tweet_id }
    all_ids.each do |tweet|
      max_attempts = 10
      num_attempts = 0
      reqs+= 1
      begin
        num_attempts += 1
        items = client.retweets(tweet)
      rescue Twitter::Error::TooManyRequests => error
        if num_attempts <= max_attempts
          puts error.rate_limit.reset_in
          sleep error.rate_limit.reset_in
          retry
        else
          raise
        end
      #TODO: Remove this rescue.
      rescue
        puts $!.backtrace
        retry
      end
      items.each do |item|
        day = item.created_at.dup.utc.to_date
        break if start_from && day < start_from
        res[day][item.user.name]+= 1
      end
      if reqs == 4
        sleep(1.minute)
        reqs = 0
      end
    end
    if start_from
      twitter_doers_data.where(:day.gte => start_from).destroy_all
    else
      twitter_doers_data.destroy_all
    end
    res.each do |day,val|
      val.each do |doer,retweets|
        twitter_doers_data << TwitterDoersData.new(day: day, doer: doer, retweets: retweets)
      end
    end
  end

  def get_twitter_doers_chart period, all = false
    dates = Project.pie_charts_dates
    get_doers_chart("twitter_tweets",dates[period],all,"doer","retweets")
  end

  # Prepares files needed to show the Twitter report.
  def prepare_twitter_files
    all_lines = Hash.new
    prepare_twitter_tweets_files
    prepare_twitter_doers_files
    prepare_pie_charts "twitter_tweets",false,"get_twitter_doers_chart"
  end

  # Prepares files needed to show the tweets line graph.
  def prepare_twitter_tweets_files all = false
    all_lines = Hash.new
    all_lines["tweets"] = get_multiple_objects_line_charts "twitter_tweets-tweets"
    all_lines["retweets-page"] = get_multiple_objects_line_charts "twitter_tweets-retweets",all,self.twitter_tweets_start_date
    add_line_graph_files "twitter_tweets", all_lines
  end

  # Prepares files needed to show the doers line graph.
  def prepare_twitter_doers_files
    all_lines = Hash.new
    all_lines["retweets-users"] = get_objects_line_chart "twitter_tweets", false, "retweets", self.twitter_tweets_start_date
    all_lines["doers_daily"] = get_doers_lines "twitter_tweets", "daily"
    all_lines["doers_monthly"] = get_doers_lines "twitter_tweets", "monthly"
    all_lines["doers_yearly"] = get_doers_lines "twitter_tweets", "yearly"
    add_line_graph_files "twitter_tweets", all_lines
  end

  def self.update_twitters_report
    Project.ne(:twitter_profile => "").each do |p|
      p.prepare_tweets_data 2.weeks.ago
      p.prepare_twitter_tweets_files
    end
  end

  # Facebook Report:

  # Returns a token that can be used to access Facebook API.
  def facebook_app_token
    Tokens.facebook_token
  end

  # Returns an array of Facebook pages.
  def get_facebook_pages
    get_array_from_text(facebook_pages)
  end

  # Prepares data needed to produce Facebook posts data.
  # previous_comments_posts is the number of posts before the last checked one whose comments should be checked for comments with dates after start_from.
  def prepare_facebook_posts_data start_from = nil, previous_comments_posts = 5
    doers_data = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    pages = get_facebook_pages
    graph = Koala::Facebook::API.new(facebook_app_token)
    starting_date = DateTime.now
    types = ["posts"]
    fields = ["likes.limit(1).summary(1),comments.fields(id).limit(1).summary(1),shares"]
    types.zip(fields).each do |type,field|
      pages.each do |page|
        is_done = false
        feed = graph.get_object("#{page}/#{type}", :fields => field, :limit => 500)
        while(feed.length != 0)
          feed.each do |f|
            unless is_done
              this_date = Time.parse(f["created_time"]).utc.to_date
              if start_from
                is_done = true if this_date < start_from
              end
              entry = self.facebook_posts_data.find_or_create_by(post_id: f["id"])
              entry.shares_count = f["shares"]["count"] if f["shares"]
              entry.day = this_date
              entry.save
              starting_date = this_date if this_date < starting_date
              if f["likes"]
                entry.likes_count = f["likes"]["summary"]["total_count"]
                likes_feed = graph.get_object("#{f["id"]}/likes", :limit => 100)
                while(likes_feed)
                  likes_feed.each do |c|
                    doer = c["name"]
                    doers_data[{doer: doer, day: this_date}]["likes"]+= 1
                  end
                  likes_feed = likes_feed.next_page
                end
              end
            else
              previous_comments_posts-= 1
            end
            if f["comments"]
              entry.comments_count = f["comments"]["summary"]["total_count"] unless is_done
              comments_feed = graph.get_object("#{f["id"]}/comments", :fields => "created_time,from", :limit => 100)
              is_done_comments = false
              while(comments_feed)
                comments_feed.each do |c|
                  this_date = Date.parse(c["created_time"])
                  if start_from
                    if this_date < start_from
                      is_done_comments = true
                      break
                    end
                  end
                  doer = c["from"]["name"]
                  doers_data[{doer: doer, day: this_date}]["comments"]+= 1
                end
                break if is_done_comments
                comments_feed = comments_feed.next_page
              end
            end
            entry.save unless is_done
          end
          break if is_done and previous_comments_posts <= 0
          feed = feed.next_page
	        break unless feed
        end
      end
    end
    unless start_from
      self.facebook_doers_data.destroy_all
    else
      self.facebook_doers_data.where(:day.gte => start_from).destroy_all
    end
    doers_data.each do |key,properties|
      entry = self.facebook_doers_data.find_or_create_by(key)
      entry.update_attributes(properties)
    end
    unless start_from
      self.facebook_posts_start_date = starting_date
      self.save
    end
  end

  def self.update_facebook_report start_from = 2.weeks.ago, num_comments = 5
    begin
      Project.ne(:facebook_pages => "").each do |p|
        p.prepare_facebook_posts_data start_from, num_comments
        p.prepare_facebook_posts_files
      end
    rescue
      open('failing/facebook/'+Time.now.strftime("%d%m%H%M")+'.error', 'wb') do |file|
        file << $!.backtrace
      end
    end
  end

  # TODO: Remove this... Not used anymore.
  # Returns the objects line chart for the project.
  def get_facebook_posts_line_charts all = false
    map = %Q{
      function() {
        emit(this.day,[this.posts,this.likes_count,this.comments_count,this.shares_count]);
      };
    }
    #TODO: This is actually not needed as the operation is always map only.
    reduce = %Q{
      function(key,values) {
        var res = values[0];
        var len = 4;
        for(var i = 1; i < values.length; i++) {
          for (var j = 0; j < len; j++) {
            res[j]+= values[i][j];
          };
        };
      };
    }
    unless all
      data = facebook_posts_data.map_reduce(map,"").out(replace: "map-reduce").entries
    else
      data = FacebookPostsData.all.map_reduce(map, "").out(replace: "map-reduce").entries
    end
    types = ["posts","likes_count","comments_count","shares_count"]
    return if data.length == 0
    res = Hash.new
    types.each do |i|
      res[i] = Array.new
    end
    prev_date = data.first["_id"].to_date - 1 # This is needed for the first iteration of the loop to work correctly.
    data.each do |entry|
      cur_date = entry["_id"].to_date
      num_zeros = (cur_date - prev_date).to_i - 1
      res.update(res){|k,v| v+= Array.new(num_zeros,0)}
      res.update(res){|k,v| v << (entry["value"][types.index(k)]).to_i}
      prev_date = cur_date
    end
    cur_date = Date.today
    num_zeros =  (cur_date - prev_date).to_i
    res.update(res){|k,v| v+= Array.new(num_zeros,0)}
  end

  def get_facebook_posts_doers_chart period, object, all = false
    dates = Project.pie_charts_dates
    get_doers_chart("facebook_posts",dates[period],all,"doer",object)
  end

  # Prepares files needed to display the facebook pages report.
  def prepare_facebook_posts_files
    self.drawables["facebook_posts"] = Hash.new(nil)
    self.save!
    all_lines = Hash.new
    all_lines["posts"] = get_multiple_objects_line_charts "facebook_posts-objects"
    all_lines["comments"] = get_objects_line_chart("facebook_posts",false,"comments",self.facebook_posts_start_date)
    all_lines["doers_daily"] = get_doers_lines "facebook_posts", "daily"
    all_lines["doers_monthly"] = get_doers_lines "facebook_posts", "monthly"
    all_lines["doers_yearly"] = get_doers_lines "facebook_posts", "yearly"
    locale = "en"
    project_path = "public"
    project_path += Rails.application.routes.url_helpers.project_path(locale,self)
    project_path += "/facebook_posts/"
    FileUtils.mkdir_p(project_path)
    all_lines.each do |key,value|
      File.open(project_path+key+".json",'w') do |f|
        f << value.to_json
      end
    end
    objects = ["likes","comments"]
    prepare_multiple_pie_charts "facebook_posts", project_path
  end

# ----------------------------------------------------------------------------------------------------------------------------

  # Project Repository:

  # List all folders and files for a certain branch or folder in a branch.
  def self.list_all_files product,branch = "master", directory
    g = Project.repo product
    unless (g.branches.map {|a| a.to_s}).include? branch
      return nil
    end
    if directory
      res = g.ls_tree(branch + ":" + directory)
    else
      res = g.ls_tree(branch)
    end
    res["blob"].each do |file,value|
      unless directory
        last_commit = g.log(1).path(file).first
      else
        last_commit = g.log(1).path(directory + "/" + file).first
      end
      res["blob"][file] = [g.gblob(value[:sha]).size,last_commit]
    end
    res["tree"].keys.each do |folder|
      unless directory
        res["tree"][folder] = g.log(1).path(folder).first
      else
        res["tree"][folder] = g.log(1).path(directory + "/" + folder).first
      end
    end
    return res
  end


  # Shows the content of a blob.
  def self.show_blob_content product, blob,commit_sha = "master"
    g = Project.repo product
    return g.gblob(commit_sha+":"+blob).contents
  end

# ----------------------------------------------------------------------------------------------------------------------------

  # IRC Channels:


  def prepare_irc_messages_data
    self.irc_messages_data.destroy_all
    self.irc_messages_start_date = Date.today
    save
    prepare_report_files "irc_messages"
  end

  # Adds to cache data needed to generate IRC Channels Report.
  def prepare_irc_report
    Rails.cache.write("irc_start_date_for"+id.to_s,Date.today.to_time.to_i*1000)
    Rails.cache.write("irc_all_time_messages_for"+id.to_s,[0])
    Rails.cache.write("irc_senders_per_day_for"+id.to_s,"[0]")
    Rails.cache.write("irc_senders_per_month_for"+id.to_s,"[0]")
    Rails.cache.write("irc_senders_per_year_for"+id.to_s,"[0]")
    Rails.cache.write("irc_this_year_messages_per_sender_for"+id.to_s,Hash.new(0))
    Rails.cache.write("irc_this_month_messages_per_sender_for"+id.to_s,Hash.new(0))
    Rails.cache.write("irc_this_week_messages_per_sender_for"+id.to_s,Hash.new(0))
    Rails.cache.write("irc_today_messages_per_sender_for"+id.to_s,Hash.new(0))
  end

  # Returns a hash that contains data needed to display IRC Channels Report.
  def get_irc_report
    res = Hash.new
    res["this_year_messages_per_sender"] = improved_pie_chart(get_doers_chart("irc_messages",Date.today.at_beginning_of_year))
    res["this_month_messages_per_sender"] = improved_pie_chart(get_doers_chart("irc_messages",Date.today.at_beginning_of_month))
    res["this_week_messages_per_sender"] = improved_pie_chart(get_doers_chart("irc_messages",Date.today.at_beginning_of_week))
    res["today_messages_per_sender"] = improved_pie_chart(get_doers_chart("irc_messages",Date.today))
    res["first_date"] = self.irc_messages_start_date.to_time.to_i * 1000
    return res
  end

  # Returns an array of IRC channels or the project.
  def get_irc_channels
    get_array_from_text irc_channels
  end

  # Returns an array of IRC Channels to be used in terminal.
  def get_irc_channels_terminal
    res = ""
    the_array = get_array_from_text irc_channels
    the_array.each do |channel|
      res += channel += ","
    end
    return res[0...-1]
  end

  # Updates the IRC Channels Report when a message is received.
  def self.irc_message_update channel, sender, message, time
    IrcMessagesReply.process_message channel, sender, message, time
    affected = Project.where({:irc_channels => /#{channel}/})
    affected.each do |project|
      project.add_new_irc_message_data sender
      project.prepare_report_files "irc_messages"
    end
  end

  # Adds a new irc_message_data entry or updates existing for the arrival of a new message.
  def add_new_irc_message_data sender
    old = irc_messages_data.find_or_create_by({contributor: sender, day: Date.today })
    old.number+= 1
    old.save
  end

  # Updates the IRC Report. Should be called at the beginning of every day.
  def update_irc_report_daily
    Rails.cache.write("irc_senders_today_for"+id.to_s,Array.new())
    Rails.cache.write("irc_today_messages_per_sender_for"+id.to_s, Hash.new(0))
    append_zero "irc_all_time_messages_for"
    append_zero "irc_senders_per_day_for"
  end

  # Updates the IRC Report. Should be called at the beginning of every week.
  def update_irc_report_weekly
    Rails.cache.write("irc_this_week_messages_per_sender_for"+id.to_s, Hash.new(0))
  end

  # Updates the IRC Report. Should be called at the beginning of every week.
  def update_irc_report_monthly
    Rails.cache.write("irc_this_month_messages_per_sender_for"+id.to_s, Hash.new(0))
    append_zero "irc_senders_per_month_for"
  end

  # Updates the IRC Report. Should be called at the beginning of every week.
  def update_irc_report_yearly
    Rails.cache.write("irc_this_year_messages_per_sender_for"+id.to_s, Hash.new(0))
    append_zero "irc_senders_per_year_for"
  end

# ----------------------------------------------------------------------------------------------------------------------------


  # Mailing Lists:


  # Returns all messages for the mailing list.
  def self.all_messages mlist
    return Mbox.open("mbox_files/"+mlist)
  end

  def get_all_lists
    return (mailing_lists).split("\n").map{|s| s.chomp}
  end

  # Returns an array of messages sent on date or after that.
  def messages_since mlist,date,options={:headers_only => true}
    res = Array.new()
    Project.all_messages(mlist).since(date.to_date,options) do |message|
      res << message
    end
    return res
  end

  # Returns an array of messages sent between 2 dates.
  def messages_between after,before,options={:headers_only => true}
    res = Array.new()
    Project.all_messages.between(after,before,options) do |message|
      res << message
    end
    return res
  end

  # Returns the number of messages on a certain day.
  def messages_on day,array_of_messages
    res = 0
    array_of_messages.each do |message|
      date = message.ruby_date
      res += 1 if date == day.to_date
    end
    return res
  end

  # Returns a hash that contains data needed to display general report about mailing lists.
  def lists_general_report
    res = Hash.new()
    res["number_of_messages"] = self.number_of_mailing_messages
    res["top_senders_of_all_time"] = hashify_for_general_report get_doers_chart("mailing_messages").reverse[0...10], "contributor", "number_of_messages"
    res["top_senders_this_year"] = hashify_for_general_report get_doers_chart("mailing_messages",Date.today.at_beginning_of_year).reverse[0...10], "contributor", "number_of_messages"
    res["top_senders_this_month"] = hashify_for_general_report get_doers_chart("mailing_messages",Date.today.at_beginning_of_month).reverse[0...10], "contributor", "number_of_messages"
    return res
  end

  # TODO: Fetch the mbox file(Needs coordinating with a sysadmin to complete this task.)
  def fetch_mbox_file
    all_lists = get_array_from_text mailing_lists
    all_lists.each do |mlist|
      open('mbox_files/'+mlist, 'wb') do |file|
        file << open('https://mail.kde.org/pipermail/'+ mlist +'/' + mlist).read
      end
    end
  end

  # Adds all mailing messages data needed to generate the report the the mailing_messages_data table.
  def prepare_mailing_messages_data
    mailing_messages_data.destroy_all
    number_of_messages = 0
    start_date = Date.today
    data = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    all_lists = get_array_from_text mailing_lists
    all_lists.each do |mlist|
      MailingMessagesReply.generate_for mlist
      successful = Project.all_messages mlist
      list_data = MailingListsData.find_or_create_by(list: mlist)
      list_number_of_messages = successful.length
      list_data.number_of_messages = list_number_of_messages
      number_of_messages+= list_number_of_messages
      mlist_start_date = Date.today
      successful.each do |message|
        the_date = message.ruby_date.in_time_zone("UTC").to_date
        next unless the_date
        the_date = the_date
        mlist_start_date = the_date if the_date < mlist_start_date
        the_sender = message.headers[:from]
        the_sender = Mail::Encodings.value_decode(the_sender).gsub('"','') # To decode the value.
        data[the_sender][the_date]+= 1
      end
      list_data.starting_month = mlist_start_date.at_beginning_of_month
      list_data.save
      start_date = mlist_start_date if mlist_start_date < start_date
      MailingListsData.split_mbox_file mlist
    end
    data.each do |contributor,days|
      days.each do |day,number|
        mailing_messages_data << MailingMessagesData.new({contributor: contributor,day: day, number: number})
      end
    end
    self.number_of_mailing_messages = number_of_messages
    self.mailing_messages_start_date = start_date
    all_kde = Project.all_kde_project
    if start_date < all_kde.mailing_messages_start_date
      all_kde.mailing_messages_start_date = start_date
      all_kde.save
    end
    self.save
    prepare_report_files "mailing_messages"
  end

  # Returns the starting month of the first message in the mailing list.
  def self.starting_month_for_list mbox
    return self.mailing_messages_start_date.at_beginning_of_month
  end

  # Returns the number of messages in the given month.
  def messages_on_month the_month
    the_array = get_array_from_text mailing_lists
    res = 0
    senders = Array.new()
    the_array.each do |mlist|
      the_list = open_month_archive mlist,the_month
      next unless the_list
      the_list.each do |message|
        res += 1
      end
    end
    return res
  end

  # Messages List index format:
  # The first item is the date of the first message.
  # starting from the second item, starts the number of messages in the month.
  # For example: [2002-02-07,4,7,20], means the first message was sent on date: February, 7th 2002.
  # February 2002 had 4 messages, March 2002 had 7 messages and April 2002 had 20 messages.


  # Adds data needed to produce messages per day graph to the cache.
  def prepare_messages_per_day
    messages_last_month = Array.new()
    all_lists = get_array_from_text mailing_lists
    all_lists.each do |mlist|
      the_list = open_month_archive mlist,Date.today
      next unless the_list
      messages_last_month = messages_last_month + get_messages_array(the_list)
      the_list_last_month = open_month_archive(mlist, 1.month.ago.to_date)
      next unless the_list_last_month
      messages_last_month = messages_last_month + get_messages_array(the_list_last_month)
    end
    messages_per_day = (1.month.ago.to_date..Date.today).map {|date| messages_on(date,messages_last_month)}.inspect
    Rails.cache.write("messages_per_day_for"+id.to_s,messages_per_day)
  end

  def prepare_messages_per_month
    Rails.cache.write("messages_per_month_for"+id.to_s,(13.months.ago.to_date...Date.today).select{|date| date.day == 1}.map { |date| messages_on_month(date)}.inspect)
  end

  # Adds data needed to produce senders per month report to the cache.
  def prepare_senders_per_month
    Rails.cache.write("senders_per_month_for"+id.to_s,(12.months.ago.at_beginning_of_month.to_date...Date.today).select{|date| date.day == 1}.map { |date| senders_on_month(date)}.inspect)
  end

  # Returns the number of senders in the given month.
  def senders_on_month the_month
    the_array = get_array_from_text mailing_lists
    res = 0
    senders = Array.new()
    the_array.each do |mlist|
      the_list = open_month_archive mlist,the_month
      next unless the_list
      the_list.each do |message|
        unless senders.include?(message.headers[:from])
          res += 1
          senders << message.headers[:from]
        end
      end
    end
    return res
  end

  # Returns a Hash containing data needed to generate mailing lists report.
  def get_lists_report
    res = Hash.new
    res["total_messages_by_sender"] = improved_pie_chart(get_doers_chart("mailing_messages"))
    res["this_year_messages_by_sender"] = improved_pie_chart(get_doers_chart("mailing_messages",Date.today.at_beginning_of_year))
    res["this_month_messages_by_sender"] = improved_pie_chart(get_doers_chart("mailing_messages",Date.today.at_beginning_of_month))
    res["this_week_messages_by_sender"] = improved_pie_chart(get_doers_chart("mailing_messages",Date.today.at_beginning_of_week))
    res["today_messages_by_sender"] = improved_pie_chart(get_doers_chart("mailing_messages",Date.today.at_beginning_of_day))
    res["first_date"] = mailing_messages_start_date.to_time.to_i * 1000
    return res
  end

  # Returns an array containing indeces for
  def get_messages_list_index mlist
    today = Date.today
    first = MailingListsData.find_by(list: mlist).starting_month
    num = (today.year*12 + today.month) - (first.year*12 + first.month) + 1
    return Array.new(num,0), first
  end

  # Returns the number of messages for a given month.
  def self.number_of_messages mlist,month
    list = open_month_archive mlist,month
    return 0 unless list
    return list.length
  end

  # Returns an array of arrays each of which represents a message, each contains the message's subject, index, id and the in_reply_to header value.
  def self.get_messages_list_by_subject mlist, month
    list = open_month_archive mlist,month
    return [] unless list
    res = Array.new()
    message_ids = Array.new()
    counter = 0
    list.each({:headers_only => true}) do |message|
      subject = message.headers[:subject]
      message_id = message.headers[:message_id]
      message_ids << message_id
      in_reply_to = message.headers[:in_reply_to] ||= ""
      if subject
        res << [subject,counter,message_id,in_reply_to]
      else
        res << ["Unknown",counter,message_id,in_reply_to]
      end
      counter += 1
    end
    list.close
    return [res,message_ids]
  end

  # Returns a message.
  def self.get_message mlist,month,index
    list = open_month_archive mlist,month
    res = Mail.new(list[index.to_i])
    list.close
    return res
  end

  # Updates the mailing list part of the app when a new message is sent to a mailing list of one of the projects.
   def self.update_mailing_list mbox,email
     date = email.date
     MailingListsData.increment_number_of_messages mbox
     MailingMessagesReply.process_message mbox, email
     month = date.strftime('%b%Y')
     MailingListsData.add_message_to_mbox mbox,month,email
     sender = email[:from].to_s.encode('utf-8', :invalid => :replace, :undef => :replace)
     affected = Project.where({mailing_lists: /#{mbox}/})
     affected.each do |project|
       project.add_new_mailing_message_data sender, date.in_time_zone("UTC").to_date
       project.prepare_report_files "mailing_messages"
     end
   end

   # Adds new data or edits old one to reflect the new message.
   def add_new_mailing_message_data sender, date
     old = mailing_messages_data.find_or_create_by({contributor: sender, day: date })
     old.number+= 1
     old.save
   end

  # Performs the actual updates to the mailing list report as the project is affected by the new message.
  def perform_mailing_list_update mbox,sender,date
    general_update_report_property "total_messages_per_sender_for",sender
    general_update_report_property "this_year_messages_per_sender_for",sender unless date < Date.today.at_beginning_of_year
    general_update_report_property "this_month_messages_per_sender_for",sender unless date < Date.today.at_beginning_of_month
    general_update_report_property "this_week_messages_per_sender_for",sender unless date < Date.today.at_beginning_of_week
    general_update_report_property "today_messages_per_sender_for",sender unless date < Date.today
    old = Rails.cache.read("mailing_lists_messages_all_line_graph_for"+id.to_s)
    old[-1]+= 1
    Rails.cache.write("mailing_lists_messages_all_line_graph_for"+id.to_s,old)
    update_all_doers_for_lists "messages", "sender", "lists_sender"
  end

# ----------------------------------------------------------------------------------------------------------------------------


  # Commits:

  # Updates the commits report for all projects.
  def self.update_all_projects_commits_data
    Project.each do |p|
      p.update_project_commits_data unless p.git_repos.empty? rescue nil
    end
  end

  # When called, fetches all git repositories that are listed in the project's git_repos field. New server doesn't need this.
  def fetch_project_repos
    the_array = get_array_from_text(git_repos)
    the_array.each { |product|
      fetch_repo product
    }
  end
  #handle_asynchronously :fetch_project_repos

  # Returns a hash that contains data needed to display general report about commits.
  def commits_general_report
    res = Hash.new()
    res["number_of_commits"] = self.number_of_commits
    res["latest_commits"] = project_list_of_commits
    res["top_committers_of_all_time"] = hashify_for_general_report get_doers_chart("commits").reverse[0...10], "contributor", "number_of_commits"
    res["top_committers_this_year"] = hashify_for_general_report get_doers_chart("commits",Date.today.at_beginning_of_year).reverse[0...10], "contributor", "number_of_commits"
    res["top_committers_this_month"] = hashify_for_general_report get_doers_chart("commits",Date.today.at_beginning_of_month).reverse[0...10], "contributor", "number_of_commits"
    return res
  end

  # Returns a hash that contains the latest commits in the products of the project.
  def latest_commits
    res = Hash.new()
    commits_list = project_list_of_commits
    commits_list.each do |k,v|
      temp_array = Array.new
      v.each do |commit|
        temp_hash = Hash.new()
        temp_hash["sha"] = commit.sha.to_s
        temp_hash["date"] = commit.date
        temp_hash["author"] = commit.author.name
        temp_hash["comment"] = commit.message
        begin
          temp_hash["files_changed"] = commit.diff_parent.size
        rescue
          temp_hash["files_changed"] = ""
        end
        temp_array << temp_hash
      end
      res[k] = temp_array
    end
    return res
  end


  def self.download_snapshot sha,format,product
    g = Project.repo(product)
    return g.archive(sha,nil,{format: format})
  end

  # Returns the total number of commits for the project.
  def project_number_of_commits
    res = 0
    the_array = get_array_from_text(git_repos)
    the_array.each do |product|
      res += Rails.cache.read("number_of_commits_for"+product)
    end
    return res
  end

  # Gets a list of commits since the sha ids present in the repos_last_sha array.
  # Also updates repos_last_sha array to reflect the latest explored commits.
  def search_commits limit = nil, with_stats = false, first = true
    the_array = get_array_from_text(git_repos)
    commits = []
    the_array.each_with_index { |product,index|
      g = Project.repo product
      the_log = g.log(limit,with_stats)
      the_log = the_log.between(repos_last_sha[index]) if repos_last_sha[index]
      begin
        the_log.each { |commit|
          commits << commit
        }
      rescue Git::GitExecuteError
        # It means that a force push happenned that deleted the last explored commit.
        if first
          reset_commits_data
          return search_commits limit, with_stats, false
        else
          return commits
        end
      end
      self.repos_last_sha[index] = g.log(1).first.sha
    }
    self.commits_last_updated_time = Time.now
    return commits
  end

  # Constructs a hash that contains commits data ready to be put in Database.
  # Also updates the start date for commits.
  def form_commits_data commits
    start_date = commits_start_date || Date.today
    data = Hash.new { |hash,key| hash[key] = Hash.new(0) }
    commits.each do |commit|
      next if commit.message.include?("SVN_SILENT")
      the_date = commit.author.date
      next unless the_date
      the_date = the_date.in_time_zone("UTC").to_date
      start_date = the_date if the_date < start_date
      data[commit.author.name][the_date]+= 1
    end
    self.commits_start_date = start_date
    data
  end

  def add_all_commits_data commits
    start_date = commits_start_date || Date.today
    commits.each do |commit|
      next if commit.message.include?("SVN_SILENT")
      the_date = commit.author.date
      next unless the_date
      the_date = the_date.in_time_zone("UTC").to_date
      start_date = the_date if the_date < start_date
      entry = commits_data.find_or_create_by(
        contributor: commit.author.name,
        day: the_date
      )
      entry.number+= 1
      entry.insertions+= commit.insertions
      entry.deletions+= commit.deletions
      entry.save
    end
    self.commits_start_date = start_date
  end

  # Adds the given data to the project's commits data.
  def add_commits_data data
    data.each do |contributor,days|
      days.each do |day,number|
        self.commits_data << CommitsData.new({contributor: contributor,day: day, number: number})
      end
    end
  end

  # Removes all previously generated commits data.
  def reset_commits_data
    self.commits_data.destroy_all
    self.number_of_commits = 0
    self.commits_start_date = nil
    restart_repos_sha
  end

  # Remove saved last sha ids of repos as the report is to be regenerated.
  def restart_repos_sha
    num_repos = get_array_from_text(git_repos).length
    self.repos_last_sha = Array.new(num_repos)
  end

  # Adds all commits data needed to generate the report to the commits_data table.
  # Used when the git repos of the project are changed.
  def prepare_commits_data
    reset_commits_data
    update_project_commits_data
  end

  # Adds new commits data to the commits_data table and updates relevant fields.
  def update_project_commits_data
    commits = search_commits nil, true
    self.number_of_commits+= commits.length
    add_all_commits_data commits
    #data = form_commits_data commits
    #add_commits_data data
    self.save
    prepare_report_files "commits", false unless commits.length == 0
    prepare_lines_of_code_files
  end

  # When a new commit is made, this is called to add determine the projects affected and update them.
  def self.update_commits_data repo
    affected = Project.where({:git_repos => /#{repo.sub('.','\.')}/})
    affected.each do |project|
      project.update_project_commits_data
    end
  end

  # Adds a new entry to the commits_data of the project to represent the new received commit.
  def add_new_commits_data author, date
    old = commits_data.find_or_create_by({contributor: author, day: date })
    old.number+= 1
    old.save
  end

  # Returns an array containing all branches in the repository.
  def self.list_branches product
    Project.repo(product).branches.map {|a| a.to_s}
  end

  # Adds files needed to display lines of code graphs.
  def prepare_lines_of_code_files
    entries = commits_data
    entries = CommitsData.all if all_kde_project?
    return if entries.empty?
    insertions = Timeline.get_objects_line_chart_hash entries, "insertions"
    insertions = Timeline.generate_array_from_per_period_hash insertions
    deletions = Timeline.get_objects_line_chart_hash entries, "deletions"
    deletions = Timeline.generate_array_from_per_period_hash deletions
    res = {
      insertions: insertions,
      deletions: deletions
    }
    file = project_files_path("commits") + "lines.json"
    File.write(file,res.to_json)
    nil
  end

  # Returns the data field for line graphs.
  def line_graph_for_commits property,per_what
    res = "["
    commits = Rails.cache.read(property+"_per_"+per_what+"_for"+id.to_s)
    commits.each do |k,v|
      res << v.to_s << ","
    end
    res << "]"
    return res
  end

  # Generates pie chart data field for commits given the data to plot in the form of a hash.
  def general_pie_chart to_plot
    res = "["
    to_plot.each do |k,v|
      res << '["' << k << '",' << v.to_s << "],"
    end
    return res + "]"
  end

  # Returns the commits report for this project.
  def get_commits_report
    res = Hash.new()
    res["total_commits_per_author"] = improved_pie_chart(get_doers_chart("commits"))
    res["this_year_commits_per_author"] = improved_pie_chart(get_doers_chart("commits",Date.today.at_beginning_of_year))
    res["this_month_commits_per_author"] = improved_pie_chart(get_doers_chart("commits",Date.today.at_beginning_of_month))
    res["this_week_commits_per_author"] = improved_pie_chart(get_doers_chart("commits",Date.today.at_beginning_of_week))
    res["today_commits_per_author"] = improved_pie_chart(get_doers_chart("commits",Date.today))
    res["first_date"] = commits_start_date.to_time.to_i * 1000
    return res
  end

  # Adds data needed to generate monthly commits graph to the cache.
  def prepare_monthly_commits_line_graph
    temp = (13.months.ago.to_date...Date.today).select{|date| date.day == 1}.map {|date| commits_on_month(date)}.inspect
    Rails.cache.write("monthly_commits"+id.to_s,temp)
  end

  # Adds data needed to generate daily commits graph to the cache.
  def prepare_daily_commits_line_graph
    temp = (1.month.ago.to_date..Date.today).map { |date| commits_on_day(date)}.inspect
    Rails.cache.write("daily_commits"+id.to_s,temp)
  end

  def update_daily_commits
    append_zero "commits_all_time"
    append_zero "authors_all_time_daily"
    Rails.cache.write("today_commits_per_author"+id.to_s,Hash.new(0))
  end

  def update_weekly_commits
    Rails.cache.write("this_week_commits_per_author"+id.to_s,Hash.new(0))
  end

  def update_monthly_commits
    append_zero "authors_all_time_monthly"
    Rails.cache.write("this_month_commits_per_author"+id.to_s,Hash.new(0))
  end

  def update_yearly_commits
    append_zero "authors_all_time_yearly"
    Rails.cache.write("this_year_commits_per_author"+id.to_s,Hash.new(0))
  end

  # Returns the number of commits for a given day.
  def commits_on_day the_day
    the_array = get_array_from_text(git_repos)
    res = 0
    the_array.each { |product|
        g = Project.repo product
        res += g.log(nil).since((the_day.at_beginning_of_day).to_s).until((the_day.end_of_day).to_s).to_a.length
    }
    return res
  end

  # Returns number of committers today.
  def committers_today
    the_array = get_array_from_text(git_repos)
    res = 0
    commiters = Array.new()
    the_array.each { |product|
        g = Project.repo product
        g.log(nil).since((Time.now.at_beginning_of_day).to_s).each do |commit|
          unless commiters.include?(commit.author.name)
            res += 1
            commiters << commit.author.name
          end
        end
    }
    return res
  end

  # Returns the number of commits for a given month.
  def commits_on_month the_month
    the_array = get_array_from_text(git_repos)
    res = 0
    the_array.each { |product|
        g = Project.repo product
        res += g.log(nil).since((the_month.at_beginning_of_month.at_beginning_of_day).to_s).until((the_month.end_of_month.end_of_day).to_s).to_a.length
    }
    return res
  end

  # Returns the number of commiters active for a given month.
  def commiters_on_month the_month
    the_array = get_array_from_text(git_repos)
    res = 0
    commiters = Array.new()
    the_array.each { |product|
        g = Project.repo product
        g.log(nil).since((the_month.at_beginning_of_month.at_beginning_of_day).to_s).until((the_month.end_of_month.end_of_day).to_s).each do |commit|
          unless commiters.include?(commit.author.name)
            res += 1
            commiters << commit.author.name
          end
        end
    }
    return res
  end

  # Returns a list of commits for a given product.
  def product_list_of_commits product, offset, limit
    offset -= 1
    commits = Array.new
    begin
      g = Project.repo product
      g.log(limit).skip(limit*offset).each { |commit|
        commits << commit
      }
    rescue ArgumentError, Git::GitExecuteError
    end
    return commits
  end

  # Used to generate list of commits.
  def project_list_of_commits offset = 0, limit = 10
    res = Hash.new
    the_array = get_array_from_text(git_repos)
    the_array.each do |product|
      res[product] = product_list_of_commits product, offset, limit
    end
    return res
  end

  # Returns the repository of a product.
  def self.repo product
    return Git.bare('.', { :repository => '/repositories/' + product ,:index => '.' })
  end

  # Returns the repository of a product.
  def repo product = git_repos.chomp
    #return Git.bare('.', { :repository => 'git_mirror/' + product ,:index => '.' })
    return Git.bare('.', { :repository => '/repositories/' + product ,:index => '.' })
  end

  # Returns an array of all repositories of the project.
  def get_repos
    return get_array_from_text(git_repos) if git_repos
    return []
  end

  # Updates the given repo.
  def self.update_repo repo
    g = Project.repo repo
    g.fetch
  end

  # Updates the commits report given the repo and the committer
  def self.update_commits_report repo, branch
    Project.all.each do |project|
      project.get_repos.each do |project_repo|
        if project_repo == repo
          g = Project.repo(repo)
          # old_commit = g.log(1).first # Removed for new server
          begin
            the_branch_key = repo+g.branch(branch).gcommit.log(2).to_a.last.sha
            num = Rails.cache.read(the_branch_key)
            Rails.cache.delete(the_branch_key)
          rescue
            num = 0
          end
          num = 0 unless num
          #Project.update_repo repo # Disabled for new server.
          g = Project.repo(repo)
          #Rails.cache.write(repo+g.branch(branch).gcommit.sha, g.gcommit(g.branch(branch).gcommit.sha).log(nil).count)
          new_commit = g.branch(branch).gcommit
          Rails.cache.write(repo+new_commit.sha, num + 1)
          # return if new_commit.to_s == old_commit.to_s # Disabled for new server.
          committer = new_commit.author.name
          project.perform_commits_report_update repo,committer
          break
        end
      end
    end
  end

  # Updates commits and committers line graphs.
  def update_commits_all_graph
    # Increment the number of commits in the last day by 1.
    nums = Rails.cache.read("commits_all_time"+id.to_s)
    nums[-1]+= 1
    Rails.cache.write("commits_all_time"+id.to_s,nums)
    update_all_doers_for "commits","author"
  end

  # Performs the actual updates to the commits report as the project is affected by the new commit.
  def perform_commits_report_update repo,committer
    num = Rails.cache.read("number_of_commits_for"+repo).to_i
    num+= 1
    Rails.cache.write("number_of_commits_for"+repo,num)
    general_update_report_property "total_commits_per_author_for",committer
    general_update_report_property "this_year_commits_per_author",committer
    general_update_report_property "this_month_commits_per_author",committer
    general_update_report_property "this_week_commits_per_author",committer
    general_update_report_property "today_commits_per_author",committer
    update_commits_all_graph
  end

  # --------------------------------------------------------------------------------------------------------------------------

  # Bugs:

  # Interface to data.

  # Given keys and values, updates(or creates if it doesn't exist) the corresponding entry in Database.
  def set_bugs_data keys, values
    unless self.bugs_data
      self.bugs_data = BugsData.new
      save
    end
    keys.zip(values).each do |key,value|
      self.bugs_data[key] = value
    end
    self.save
  end

  # Given keys, returns the value retrieved from Database.
  def get_bugs_data keys #TODO: Not implemented. Not sure it should be though.
    bugs_data
  end

  # Gets all data of bugs.
  def get_all_bugs_data
    bugs_data
  end

  # Available bug options from Bugzilla.
  def self.bugs_options
    ["assigned_to","classification","component","creator","op_sys","platform","priority","product","severity","status","version"]
  end

  # Returns the index which holds the data for that day in the bugs line graphs.
  def get_bug_day_index day
    (day.to_date-bugs_data.start_date.to_date).to_i
  end

  #  Generates a hash that includes general statistics about bugs for a certain project.
  def bugs_general_report
    res = Hash.new()
    data = get_bugs_data ["total_number","resolved_by_assigned_to","creator"]
    res["number_of_bugs"] = data["total_number"]
    res["latest_bugs"] = list_of_bugs
    res["top_bug_resolvers"] = hashify_for_general_report data["resolved_by_assigned_to"].reverse[0...10], "contributor", "number_of_resolved_bugs"
    res["top_bug_reporters"] = hashify_for_general_report data["creator"].reverse[0...10], "contributor", "number_of_created_bugs" if bugs_report_fields.include?("creator")
    return res
  end

  # Fetches from Bugzilla all data needed to generate graphical reports.
  def prepare_bugs_report
    self.bugs_data.destroy if self.bugs_data # Destroy the old bugs report.
    all_products = self.bugzilla_products.dup
    pie_data = Hash.new{|hash, key| hash[key] = Hash.new}
    line_data = Hash.new{|hash, key| hash[key] = Hash.new}
    total_number = 0
    while all_products.length != 0
      products = all_products.shift(10)
      successful = search_bugzilla(bugs_report_fields | ["creation_time","status","priority","assigned_to"],0,0,nil,nil,nil,products)
      if successful
        bugs_report_fields.each do |property|
          pie_data[property] = merge_hashes pie_data[property],count_bugs_by(property, successful) unless ["creation_time"].include?(property)
        end
        pie_data["resolved_by_assigned_to"] = merge_hashes pie_data["resolved_by_assigned_to"],count_bugs_by("assigned_to", successful, ["status","RESOLVED"])
        pie_data["open_by_priority"] = merge_hashes pie_data["open_by_priority"],count_bugs_by("priority", successful,["is_open",true])
        total_number+= successful.length
      end
    end
    pie_data.update(pie_data) {|k,v| v.sort_by{|key,val| val}}
    data = pie_data.merge(prepare_all_time_bugs_line_graphs)
    data["total_number"] = total_number
    self.bugs_data = BugsData.new
    self.save
    set_bugs_data data.keys,data.values
    prepare_bugs_report_files
  end
  #handle_asynchronously :prepare_bugs_report

  # Merges hashes in the correct way needed for report preparation.
  def merge_hashes h1,h2
    return h1.merge(h2){|k,a,b| a+b}
  end

  # Does the update for bugs line graphs that should be done at the beginning of every day.
  def update_bugs_line_graphs_daily
    data = self.bugs_data
    data.reported_line << 0
    data.resolved_line << 0
    data.total_created_line << data.total_created_line.last
    data.net_open_line << data.net_open_line.last
    data.still_open_line << 0
    data.average_close_time_line << 0
    data.resolved_after_a_year_line << 0
    self.save
    self.prepare_bugs_report_files
  end

  # Returns a hash where keys represent a property and values are the data to be represented.
  def get_bugs_report
    data = get_all_bugs_data.attributes.except!("total_number","id","created_at","updated_at")
    data["bugs_by_status"] = data.delete "status" # There's a problem with using status in HighCharts.
    data["start_date"] = data["start_date"].to_i*1000
    data
  end

  # Collects needed data for all time line graphs for bugs and saves them in the appropriate place in the cache.
  def prepare_all_time_bugs_line_graphs
    all_products = self.bugzilla_products.dup
    reported_bugs = Hash.new(0)
    resolved_bugs = Hash.new(0)
    time_to_close = Hash.new(0)
    resolved_after_a_year_line = Hash.new(0)
    still_open = Hash.new(0)
    closed_bugs_by_creation = Hash.new(0)
    total_reported = 0
    total_close_time = 0
    total_closed_bugs = 0
    net_open = 0
    start_date = Date.today
    while (all_products.length != 0)
      products = all_products.shift(10)
      bugs = search_bugzilla(["creation_time","last_change_time","status"],0,0,nil,nil,nil,products)
      return nil unless bugs
      bugs.each do |bug|
        creation_date = bug["creation_time"].to_date
        last_change_date = bug["last_change_time"].to_date
        start_date = creation_date if creation_date < start_date
        reported_bugs[creation_date]+= 1
        if closed_bugs_status.include?(bug["status"])
          resolved_bugs[last_change_date]+= 1
          cur_time_to_close = (last_change_date-creation_date)
          if (cur_time_to_close > 365)
            resolved_after_a_year_line[creation_date]+= 1
          else
            time_to_close[creation_date]+= cur_time_to_close
          end
          total_close_time+= cur_time_to_close
          closed_bugs_by_creation[creation_date]+= 1
          total_closed_bugs+= 1
        else
          still_open[creation_date]+= 1
        end
      end
    end
    res = Hash.new
    res["start_date"] = start_date
    res["total_average_close_time"] = total_close_time/total_closed_bugs unless total_closed_bugs == 0
    res["reported_line"] = ((start_date..Date.today).map { |date| reported_bugs[date]})
    res["resolved_line"] = ((start_date..Date.today).map { |date| resolved_bugs[date]})
    res["still_open_line"] = ((start_date..Date.today).map { |date| still_open[date]})
    res["total_created_line"] = ((start_date..Date.today).map { |date| total_reported+= reported_bugs[date]})
    res["net_open_line"] = ((start_date..Date.today).map { |date| net_open+= (reported_bugs[date] - resolved_bugs[date])})
    res["average_close_time_line"] = ((start_date..Date.today).map { |date| ((time_to_close[date]/closed_bugs_by_creation[date]).to_f if closed_bugs_by_creation[date] != 0) || 0})
    res["resolved_after_a_year_line"] = ((start_date..Date.today).map { |date| resolved_after_a_year_line[date]})
    return res
  end

  # Produces an array whose each element is itself an array of 2 elements, the first of which is the name of a property and the second is the number of bugs that has it.
  def count_bugs_by property, successful, condition = nil
    bugs = Hash.new(0)
    successful.each do |bug|
      unless condition
        bugs[bug[property]] += 1
      else
        bug["is_open"] = true unless closed_bugs_status.include?(bug["status"])
        if bug[condition[0]] == condition[1]
          bugs[bug[property]] += 1
        end
      end
    end
    return bugs
  end

  # Returns the number of bugs for the project.
  def number_of_bugs
    return bugs_data.total_number
  end

  # Returns an array whose each element represents a bug. Used to provide a list of bugs for a given project.
  def list_of_bugs offset = 1, limit = 10
    number_bugs = number_of_bugs
    temp = number_bugs-(offset.to_i)*limit
    if temp > 0
      successful = search_bugzilla ["id","assigned_to","component","last_change_time","status","summary"],limit,temp
    else
      successful = (search_bugzilla ["id","assigned_to","component","last_change_time","status","summary"],limit,0)[0...(limit+temp)]
    end
    return successful.reverse if successful
    return []
  end

  # Given a bug id, it fetches data about that bug.
  def self.fetch_bug id
    full_string = 'https://bugs.kde.org/jsonrpc.cgi?method=Bug.get&params=[{"ids":[' + id.to_s + ']}]'
    unparsed = open_link full_string
    parsed = ActiveSupport::JSON.decode(unparsed)
    return parsed["result"]["bugs"][0] if parsed["result"]
  end

  # Given a bug id, it fetches its comments.
  def self.fetch_comments id
    full_string = 'https://bugs.kde.org/jsonrpc.cgi?method=Bug.comments&params=[{"ids":[' + id.to_s + ']}]'
    unparsed = open_link full_string
    parsed = ActiveSupport::JSON.decode(unparsed)
    return parsed["result"]["bugs"][id.to_s]["comments"] if parsed["result"]
  end

  # Returns an array of all bug products
  def get_bug_products
    return bugzilla_products
  end

  # Updates projects containing the given product
  def self.update_bugs product,data
    Project.where(bugzilla_products: product).each do |project|
      project.perform_bugs_report_update(data)
      project.prepare_bugs_report_files
    end
  end

  def self.list_selectable_products
    full_string = 'https://bugs.kde.org/jsonrpc.cgi?method=Product.get_selectable_products'
    unparsed = open_link full_string
    parsed = ActiveSupport::JSON.decode(unparsed)
    ids = parsed["result"]["ids"]
    res = Array.new
    while true
      all_products = 'https://bugs.kde.org/jsonrpc.cgi?method=Product.get&params=[{"include_fields":["name"],"ids":'
      all_products+= ids.shift(500).to_s
      all_products+= '}]'
      unparsed = open_link all_products
      parsed = ActiveSupport::JSON.decode(unparsed)
      parsed["result"]["products"].each do |product|
        res << product["name"]
      end
      return res if ids.length == 0
    end
  end

  # Performs the actual update to cache data used to produce bugs report(Used when a new bug is reported).
  def perform_bugs_report_update data
    old_data = bugs_data
    old_data.total_number += 1 # Increase total number of bugs by one as a new bug is added.
    update_bugs_report_property old_data,"priority",data["priority"],"open"
    bugs_report_fields.each do |property|
      update_bugs_report_property old_data,property,data[property]
    end
    #increment_line_graphs_for_new_bug old_data
    old_data.reported_line[-1]+= 1
    old_data.total_created_line[-1]+= 1
    old_data.net_open_line[-1]+= 1
    old_data.still_open_line[-1]+= 1
    self.save
  end

  # Increment the suitable line graphs on creation of a new bug.
  def increment_line_graphs_for_new_bug old_data
    increment_last_db old_data, "reported_line"
    increment_last_db old_data, "total_created_line"
    increment_last_db old_data, "net_open_line"
  end

  # Increases the value for a certain property in the bugs report by 1.
  def update_bugs_report_property old_data,property,key,extra=""
    property = extra + "_by_" + property unless extra.length == 0
    the_hash = hashify_array old_data[property]
    the_hash[key] = 0 unless the_hash[key]
    the_hash[key] += 1
    old_data[property] = (the_hash.sort_by {|k, v| v})
  end

  # Updates data in the cache to reflect an update made to an existing bug.
  def self.update_existing_bug product,data,bug_id
    Project.where(bugzilla_products: product).each do |project|
      project.perform_bugs_report_update_existing(data,bug_id)
      project.prepare_bugs_report_files
    end
  end

  # Performs the actual update to cache data used to produce bugs report(Used when a bug is edited or commented on).
  def perform_bugs_report_update_existing data,bug_id
    #TODO: This is unlikely but in case the assignee or the state of the bug was changed, the methods should be handled separately.
    old_data = get_all_bugs_data
    masked_properties = {"OS" => "op_sys", "Assignee" => "assigned_to"}
    data.each do |property|
      if masked_properties.keys.include?(property[0])
        property[0] = masked_properties[property[0]]
      else
        property[0] = property[0].downcase
      end
      if bugs_report_fields.include?(property[0])
        update_bugs_report_existing_property old_data, property
      end
      if property[0] == "status"
        bug_data = Project.fetch_bug bug_id
        if !(closed_bugs_status.include?(property[1].to_s)) && closed_bugs_status.include?(property[2].to_s)
          update_bugs_report_existing_property_status_change old_data, bug_data,"priority","open",false
          # A bug was closed.
          update_line_graphs_on_closed_bug old_data, bug_data
        elsif closed_bugs_status.include?(property[1].to_s) && !(closed_bugs_status.include?(property[2].to_s))
          update_bugs_report_existing_property_status_change old_data, bug_data,"priority","open",true
          # A bug was reopened.
          update_line_graphs_on_reopened_bug old_data,bug_data
        end
        if property[1] == "RESOLVED"
          update_bugs_report_existing_property_status_change old_data, bug_data,"assigned_to","resolved",false
        elsif property[2] == "RESOLVED"
          update_bugs_report_existing_property_status_change old_data, bug_data,"assigned_to","resolved",true
        end
      end
    end
    save
  end

  # Called when a bug is closed, it reflects that on line graphs.
  def update_line_graphs_on_closed_bug data, bug_data
    number_of_closed_bugs = data["total_number"]-data["net_open_line"][-1]
    total_days = number_of_closed_bugs*data["total_average_close_time"]
    number_of_closed_bugs+= 1
    creation_time = bug_data["creation_time"].to_date
    extra_days = (bug_data["last_change_time"].to_date - creation_time).to_i
    total_days+= extra_days
    data.total_average_close_time = total_days/number_of_closed_bugs.to_f
    day_index = get_bug_day_index creation_time
    number_of_closed_bugs = data["reported_line"][day_index] - data["still_open_line"][day_index]
    total_days = number_of_closed_bugs*data["average_close_time_line"][day_index]
    total_days+= extra_days
    number_of_closed_bugs+= 1
    data.average_close_time_line[day_index] = total_days/number_of_closed_bugs.to_f
    data.still_open_line[day_index]-= 1
    data.resolved_line[-1]+= 1
    data.net_open_line[-1]-= 1
  end

  # Called when a bug is closed, it reflects that on line graphs.
  def update_line_graphs_on_reopened_bug data,bug_data
    #TODO: Add a job to delayed job to regenerate the bugs report at the end of the day.
    creation_time = bug_data["creation_time"].to_date
    day_index = get_bug_day_index creation_time
    data["still_open_line"][day_index]+= 1
    data["net_open_line"][-1]+= 1
  end

  # Updates a property values by removing old value and adding the new one.
  def update_bugs_report_existing_property data,property,extra = ""
    property[0] = extra + "_by_" + property[0] unless extra.length == 0
    the_hash = hashify_array data[property[0]]
    the_hash[property[1]] -= 1
    the_hash.except!([property[1]]) if the_hash[property[1]] == 0
    the_hash[property[2]] += 1
    data[property[0]] = the_hash.sort_by {|k, v| v}
  end

  # Updates a property values in the cache when the bug status is changed by removing old value and adding the new one.
  def update_bugs_report_existing_property_status_change data,bug_data,property,extra,add
    if extra.length == 0
      new_property = property
    else
      new_property = extra + "_by_" + property
    end
    the_hash = hashify_array data[new_property]
    if add
      the_hash[bug_data[property]] += 1
    else
      the_hash[bug_data[property]] -= 1
      the_hash.except!(bug_data[property]) if the_hash[bug_data[property]] == 0
    end
    data[new_property] = the_hash.sort_by{|k, v| v}
  end
end
