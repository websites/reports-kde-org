class GeneralObjects
  include Mongoid::Document

  field :name
  field :value, type: Hash, default: Hash.new

  def self.get_last_updated type
    self.find_or_create_by(name: "#{type}_last_updated").value["time"]
  end

  def self.set_last_updated type, time
    item = self.find_or_create_by(name: "#{type}_last_updated")
    item.value["time"] = time
    item.save
  end

  def self.get_pid type
    self.find_or_create_by(name: "pids").value[type]
  end

  def self.set_pid type,pid
    item = self.find_or_create_by(name: "pids")
    item.value[type] = pid
    item.save
  end

  def self.restart_twitter_stream
    ids = Project.all.map(&:twitter_profile_id).compact
    File.open("bots/twitter.txt",'w') do |f|
      f << ids.join(",")
    end
  end

  def self.restart_irc_bot
    channels = Project.all.map(&:irc_channels).map { |e| get_array_from_text(e) }.flatten.uniq
    File.open("bots/irc.txt",'w') do |f|
      f << channels.join(",")
    end
  end

  # status determines whether to show pie charts in social media reports or not.
  def self.set_social_media_pie_charts_status status = false
    entry = self.find_or_create_by(name: "social_media_pie_charts")
    entry.value["enabled"] = state
    entry.save
  end

  def self.show_social_media_pie_charts?
    self.find_or_create_by(name: "social_media_pie_charts").value["enabled"]
  end

end