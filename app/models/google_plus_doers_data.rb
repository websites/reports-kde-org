class GooglePlusDoersData
  include Mongoid::Document
  
  field :doer
  field :day, type: Date
  field :replies, type: Integer, default: 0
  field :plusones, type: Integer, default: 0
  field :reshares, type: Integer, default: 0
  
  belongs_to :project, index: true
    
  index doer: 1
  index day: 1
  
end