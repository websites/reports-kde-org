class CustomDj < Struct.new(:custom_report_id)
  def perform
    custom_report = CustomReport.find(custom_report_id)
    custom_report.prepare_reports
  end
end