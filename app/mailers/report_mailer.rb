class ReportMailer < ActionMailer::Base
  def receive(email)
    begin
      to = email.header["X-Original-To"].to_s
      if to == "kde-commits@kde.org"
        update_commits email
      elsif to == "kde-bugs-dist@kde.org"
        product = email.header["X-Bugzilla-Product"].to_s
        if email.header["X-Bugzilla-Type"].to_s == "new"
          new_bug email,product
        else
          update_bug email,product
        end
      else
        update_mailing_list email
      end
      return nil
    rescue
      open('failing/messages/'+Time.now.strftime("%d%m%H%M")+'.mbox', 'wb') do |file|
        file << email.to_s
      end
      open('failing/messages/'+Time.now.strftime("%d%m%H%M")+'.error', 'wb') do |file|
        file << $!.backtrace
      end
    end
  end

  # Updates the commits part of the app based on the received email.
  def update_commits email
    return unless email.header["X-Commit-Ref"].to_s == "master" # Statistics are currently supported for master branch only.
    return if email.subject.include?("(silent)")
    repo = email.header["X-Commit-Project"].to_s
    return if repo.length == 0
    repo += ".git"
    Project.update_commits_data repo
  end

  # Updates the bugs part of the app based on the received email when a new bug is reported.
  def new_bug email,product
    bug_id = email.header["Message-ID"].to_s.match(/bug-(?<id>\d+)-/)["id"]
    data = Project.fetch_bug bug_id
    Project.update_bugs product,data
  end

  # Updates the bugs part of the app based on the received email when a bug is changed or commented on.
  def update_bug email,product
    data = email.body.to_s.scan(/(?<property>\w+)\|(?<old>\w+)\s+\|(?<new>\w+)/)
    return unless data
    bug_id = email.header["Message-ID"].to_s.match(/bug-(?<id>\d+)-/)
    return unless bug_id
    Project.update_existing_bug product, data, bug_id["id"]
  end

  # Updates the mailing list part of the app based on the received email when a new message is posted to a mailing list.
  def update_mailing_list email
    mbox = email.header["X-Original-To"].to_s.match(/(?<mbox>.+)@kde.org/)["mbox"]
    Project.update_mailing_list mbox,email if File.directory?("mbox_files/" + mbox)
  end

end