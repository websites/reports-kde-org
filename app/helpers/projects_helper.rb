module ProjectsHelper
  def build_bug_products_and_jobs(project)
    @bug_products = Project.list_selectable_products
    @build_jobs = BuildDataManager.all_jobs_selection_array
    project
  end

  def render_property name,value
    return render :partial => "property", :locals => {
     :name => name, :value => value
    }
  end

  def render_commits_charts type, to_plot, unloaded = nil
    types = {true => "tabbed_pie_chart_display", false => "single_pie_chart_display"}
    masks = {
      "commits" => ["commits","author"],
      "mailing_messages" => ["messages","sender"],
      "irc_messages" => ["messages","sender"],
      "facebook_posts" => [nil,"user"],
      "twitter_tweets" => ["retweets", "user"],
      "google_plus_posts" => [nil,"user"],
      "forum_posts" => ["posts","poster"],
      "planet_posts" => ["posts","poster"],
      "dot_posts" => [nil,"posts"],
      "review_requests" => ["requests","contributor"],
    }
    WikiDataManager.explorable_sites.each do |site|
      masks["#{site}_wiki"] = ["revisions", "contributor_for_#{site}"]
    end
    BuildDataManager.all_stats.each do |status|
      masks["#{status}_build"] = ["builds", "contributor"]
    end
    if masks[type][0]
      title = "#{to_plot}_#{masks[type][0]}_per_#{masks[type][1]}"
    else
      title = "#{to_plot}_per_#{masks[type][1]}"
    end
    return render :partial => types[@project.drawables[type][to_plot]], :locals => {
        :title => title,
        :type => type,
        :to_plot => to_plot,
        :unloaded => unloaded
      }
  end
  
  def render_standard_pie_charts path, title, tabbed, unloaded = nil
    types = {true => "tabbed_pie_chart", false => "single_pie_chart"}
    return render :partial => types[tabbed], :locals => {
        :path => path,
        :title => title,
        :unloaded => unloaded
      }
  end

  def render_bugs_chart to_plot, unloaded = nil
    types = {true => "tabbed_pie_chart_display", false => "single_pie_chart_display"}
    return render :partial => types[@project.drawables["bugs"][to_plot]], :locals => {
        :title => "bugs_by_#{to_plot}",
        :type => "bugs",
        :to_plot => to_plot,
        :unloaded => unloaded
      }
  end

  # Draws a single chart with single line.
  def render_objects_chart type, approx = "sum", decimals = nil, custom_agg = false
    #TODO: This hash should be moved out of here.
    types = {
      "commits" => ["number_of_commits","objects"],
      "mailing_messages" => ["number_of_messages","objects"],
      "irc_messages" => ["number_of_messages","objects"],
      "total-bugs" => ["total_number_of_bugs","total_created"],
      "open-bugs" => ["total_number_of_open_bugs","net_open"],
      "still-open-bugs" => ["number_of_reported_bugs_still_open","still_open"],
      "average_close_time-bugs" => ["average_number_of_days_to_close_bugs_closed_in_less_than_a_year","average_close_time"],
      "resolved_after_a_year-bugs" => ["number_of_bugs_closed_after_a_year","resolved_after_a_year"],
      "average_first_reply-mailing_messages" => ["average_number_of_hours_to_first_reply","average_time_to_first_reply"],
      "average_first_reply-irc_messages" => ["average_number_of_hours_to_first_reply","average_time_to_first_reply"],
      "average_first_reply-forum_posts" => ["average_number_of_hours_to_first_reply","average_time_to_first_reply"],
      "average_hours_to_fix-build" => ["average_number_of_hours_to_fix","average_hours_to_fix"],
      "average_tries_to_fix-build" => ["average_number_of_tries_to_fix","average_tries_to_fix"],
      "comments-facebook_posts" => ["number_of_comments","comments"],
      "replies-google_plus_posts" => ["number_of_replies","replies"],
      "retweets_users-twitter_tweets" => ["retweets_by_users","retweets-users"],
      "planet_posts" => ["number_of_posts","objects"],
      "average_close_time-review_requests" => ["average_number_of_days_to_close_reviews","average_close_time"],
      "closed_after_a_year-review_requests" => ["number_of_reviews_closed_after_a_year","closed_after_a_year"],
      }
      WikiDataManager.explorable_sites.each do |wiki|
        types["users_registered-#{wiki}_wiki"] = ["number_of_users_registered-#{wiki}","registrations"]
        types["revisions-#{wiki}_wiki"] = ["number_of_revisions-#{wiki}","revisions"]
      end
      label = types[type][0].split("-").first
    return render :partial => "full_objects_line_graph", :locals => {
      :objects => type.split("-").last,
      :line => types[type][1],
      # Title and y_label are the same in these graphs except for that title should have an extra "_over_time"
      :title => types[type][0],
      :y_label => label,
      :lang_label => label + "_over_time",
      :approx => approx,
      :decimals => decimals,
      :custom_agg => custom_agg
    }
  end

  # Draws multiple lines in the a single chart.
  def render_multiple_objects_chart type, approx
    types = {
      "bugs" => "rep_res",
      "commits" => "lines",
      "facebook_posts" => "posts",
      "tweets-twitter_tweets" => "tweets",
      "retweets_page-twitter_tweets" => "retweets-page",
      "google_plus_posts" => "objects",
      "forum_posts" => "objects",
      "dot_posts" => "objects",
      "review_requests" => "objects",
      "build" => "objects"
    }
    titles = {
      "bugs" => "number_of_bugs",
      "commits" => "number_of_lines",
      "facebook_posts" => "numbers",
      "tweets-twitter_tweets" => "tweets_by_page",
      "retweets_page-twitter_tweets" => "retweets_by_page",
      "google_plus_posts" => "numbers",
      "forum_posts" => "numbers",
      "dot_posts" => "numbers",
      "review_requests" => "numbers",
      "build" => "build"
    }
    return render :partial => "full_objects_multiple_line_graph", :locals => {
      :objects => type.split("-").last,
      :lines => types[type],
      :approx => approx,
      :title => titles[type] + "_over_time",
      :y_label => titles[type],
    }
  end

  def render_doers_chart type
    types = {
      "commits" => "authors",
      "mailing_messages" => "senders",
      "irc_messages" => "senders",
      "facebook_posts" => "users",
      "twitter_tweets" => "retweeters",
      "google_plus_posts" => "users",
      "forum_posts" => "posters",
      "planet_posts" => "posters",
      "dot_posts" => "posters",
      "review_requests" => "requesters"
    }
    WikiDataManager.explorable_sites.each do |wiki|
      types["#{wiki}_wiki"] = "editors-#{wiki}"
    end
    label = types[type].split("-").first
    return render :partial => "full_doers_line_graph", :locals => {
      :title => types[type],
      :y_label => label,
      :lang_label => label + "_over_time",
      :type => type
        }
  end

  def render_json_hash to_render
    res = ""
    to_render.each do |k,v|
      res += '"' + k.to_s + '":'
      res += '"' if v.class == String
      res += v.to_s.gsub(/"/,'\\"')
      res += '"' if v.class == String
      res += ","
    end
    return res[0...-1]
  end

  def render_xml_hash to_render
    res = to_render.to_xml(:skip_instruct => true)
    return res[6...-8]
  end

end
