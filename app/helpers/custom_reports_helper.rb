module CustomReportsHelper

  def render_line_chart path, start_date, title = "number", approx = "sum", decimals = nil, custom_agg = nil
    return render :partial => "full_line_graph", :locals => {
      :path => path,
      :start_date => start_date,
      # Title and y_label are the same in these graphs except for that title should have an extra "_over_time"
      :title => title + "_over_time",
      :y_label => title,
      :approx => approx,
      :decimals => decimals,
      :custom_agg => custom_agg
    }
  end

  # Draws multiple lines in a single chart.
  def render_multiple_lines_chart path, start_date, title = "numbers", approx = "sum"
    return render :partial => "full_multiple_line_graph", :locals => {
      :path => path,
      :approx => approx,
      :title => title + "_over_time",
      :y_label => title,
      :start_date => start_date
    }
  end
  
  def render_standard_doers_chart path, start_date, title
    return render :partial => "full_doers_line_graph", :locals => {
      :path => path,
      :start_date => start_date,
      :title => title + "_over_time",
      :y_label => title
    }
  end
  
  def render_pie_charts path, title, tabbed, unloaded = nil
    types = {true => "tabbed_pie_chart_display", false => "single_pie_chart_display"}
    return render :partial => types[tabbed], :locals => {
        :path => path,
        :title => title,
        :unloaded => unloaded
      }
  end

  def render_single_line type, path, title, start_date
    return render :partial => "single_line_graph", :locals => {
      :type => type,
      :path => path,
      # Title and y_label are the same in these graphs except for that title should have an extra "_over_time"
      :title => title+"_over_time",
      :y_label => title,
      :start_date => start_date.to_i * 1000
    }
  end

  def render_multiple_lines type, path, title, start_date
    return render :partial => "multiple_line_graph", :locals => {
      :type => type,
      :path => path,
      # Title and y_label are the same in these graphs except for that title should have an extra "_over_time"
      :title => title+"_over_time",
      :y_label => title,
      :start_date => start_date.to_i * 1000
    }
  end

end
