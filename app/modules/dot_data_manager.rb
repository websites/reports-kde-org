module DotDataManager
  
  @@TYPE = "dot_posts"
  
  def self.prepare_report object
    entries = DotPostsData.entries_for object
    start_date = entries.min(:day) || Date.today
    object.set_start_date @@TYPE, start_date 
    files = prepare_line_graphs entries
    files.merge! prepare_pie_charts(entries, object)
    ReportFileManager.write_report_files(object.path_for(@@TYPE), files)
  end
  
  def self.prepare_line_graphs entries
    res = {
      objects: Timeline.get_multiline_graph(entries, ["posts", "comments_count"]),
    }
    res.merge Timeline.get_doers_per_periods(entries)
  end
  
  def self.prepare_pie_charts entries, object
    hashes = Hash.new
    ["contributor", "category"].each do |type|
      hashes[type] = PieChart.full_pie_chart_handler "#{type}_dot_posts", entries, object, "posts", type
      hashes[type] = Hash[hashes[type].map {|k, v| ["#{type}_#{k}", v] }]
    end
    hashes["contributor"].merge(hashes["category"])
  end
  
  def self.prepare_choices_list
    res = {
      contributors: DotPostsData.distinct(:contributor).sort,
      products: DotPostsData.distinct(:category).sort
    }
    ReportFileManager.write_choices_file @@TYPE, res
  end
end