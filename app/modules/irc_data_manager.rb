module IrcDataManager
  
  @@TYPE = "irc_messages"
  
  def self.prepare_report object
    entries = IrcMessagesData.entries_for object
    start_date = entries.min(:day) || Date.today
    object.set_start_date @@TYPE, start_date 
    files = prepare_line_graphs entries, object, start_date
    files.merge! prepare_pie_charts(entries, object)
    ReportFileManager.write_report_files(object.path_for(@@TYPE), files)
  end
  
  def self.prepare_line_graphs entries, object, start_date
    replies_entries = IrcMessagesReply.entries_for object
    res = {
      objects: Timeline.get_timeline_for(entries, "number"),
      average_time_to_first_reply: Timeline.get_average_timeline_for(replies_entries, "first_reply_period", start_date)
    }
    res.merge Timeline.get_doers_per_periods(entries)
  end
  
  def self.prepare_pie_charts entries, object
    PieChart.full_pie_chart_handler "irc_messages", entries, object
  end
  
  def self.prepare_choices_list
    res = {
      contributors: IrcMessagesData.distinct(:contributor).sort,
      products: IrcMessagesData.distinct(:channel).sort
    }
    ReportFileManager.write_choices_file @@TYPE, res
  end
end