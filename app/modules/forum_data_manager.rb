module ForumDataManager
  
  @@TYPE = "forum_posts"
  
  def self.prepare_report object
    posts_entries = ForumPostsData.entries_for object
    start_date = posts_entries.min(:day) || Date.today
    object.set_start_date @@TYPE, start_date 
    replies_entries = ForumPostsReply.entries_for object
    files = prepare_line_graphs posts_entries, replies_entries, start_date
    files.merge! prepare_pie_charts posts_entries, object
    ReportFileManager.write_report_files(object.path_for(@@TYPE), files)
  end
  
  def self.prepare_choices_list
    res = {
      contributors: ForumPostsData.distinct(:contributor).sort,
      products: ForumPostsData.distinct(:fid).sort
    }
    ReportFileManager.write_choices_file @@TYPE, res
  end
  
  private

  def self.prepare_pie_charts entries, object
    PieChart.full_pie_chart_handler "forum_posts", entries, object, "posts"
  end

  def self.prepare_line_graphs posts_entries, replies_entries, start_date
    res = get_posts_lines_report_files posts_entries
    res[:average_time_to_first_reply] = get_replies_line_report_files replies_entries, start_date
    res
  end

  def self.get_posts_lines_report_files entries
    res = {objects: prepare_objects_line_chart(entries)}
    res.merge Timeline.get_doers_per_periods(entries)
  end
  
  def self.get_replies_line_report_files entries, start_date
    Timeline.get_average_timeline_for entries, "first_reply_period", start_date
  end
  
  def self.prepare_objects_line_chart entries
    Timeline.get_multiline_graph entries, ["posts","threads","resolved"]  
  end
  
end