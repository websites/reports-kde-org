# Responsible for preparing and updating Wiki Report.
module WikiDataManager

  def self.prepare_report object, site
    entries = WikiRevisionsData.get_entries_for object, site
    object.set_start_date "#{site}_wiki", (entries.min(:day)||Date.today)
    files = prepare_line_graphs entries
    files.merge! prepare_pie_charts entries, object, site
    ReportFileManager.write_report_files(object.path_for("#{site}_wiki"), files)
  end

  def self.explorable_sites
    ["community","techbase","userbase"]
  end

  def self.fields
    res = Array.new
    explorable_sites.each do |site|
      res << product_field_for(site)
    end
    res
  end
  
  def self.product_field_for site
    "#{site}_wiki_pages"
  end
  
  def self.prepare_choices_list
    explorable_sites.each do |site|
      entries = WikiRevisionsData.where(site: site)
      res = {
        contributors: entries.distinct(:contributor).sort,
        products: entries.distinct(:page).sort
      }
      ReportFileManager.write_choices_file "#{site}_wiki", res
    end
  end
  
  def self.drawable_wikis_for object
    res = Array.new
    explorable_sites.each do |site|
      (res << site) if object.is_included?("#{site}_wiki")
    end
    res
  end
  
  def self.start_dates_for object, drawables = drawable_wikis_for(object)
    res = Hash.new
    drawables.each do |site|
      res[site] = ReportHelpers.pointStart_date(object["#{site}_wiki_start_date"])
    end
    res
  end
  
  def self.path_for object, drawables = drawable_wikis_for(object)
    res = Hash.new
    drawables.each do |site|
      res[site] = object.display_path_for "#{site}_wiki"
    end
    res
  end
  
  def self.total_views_for object, drawables = drawable_wikis_for(object)
    res = Hash.new
    drawables.each do |site|
      res[site] = WikiPagesData.get_entries_for(object, site).sum(:views)
    end
    res
  end
  
  def self.total_revisions_for object, drawables = drawable_wikis_for(object)
    res = Hash.new
    drawables.each do |site|
      res[site] = WikiRevisionsData.get_entries_for(object, site).sum(:number)
    end
    res
  end
  
  # When you want to prepare a report for all sites, call the method
  # and add to it _all_sites.
  def self.method_missing name
    orig_func = name.to_s.chomp("_all_sites")
    super unless respond_to?(orig_func)
    orig_func = orig_func.to_sym
    explorable_sites.each do |site|
      send(orig_func,site)
    end
    nil
  end

  # This should be called daily. Takes care of updating all aspects of the wiki report
  # that should be updated daily.
  def self.daily_update
    prepare_registrations_timeline
    explorable_sites.each do |site|
      update_all_pages_revisions_data site, 2.days.ago.to_date
    end
    prepare_choices_list
  end

  # Should be called daily for each project.
  # It prepares a final ready to show revisions charts(line graphs and pie charts).
  def self.prepare_project_revisions_report project, sites = project.drawable_wikis
    return if sites.empty?
    prepare_timeline_for "revisions", project, sites
    ["year","month","day"].each do |period|
      prepare_timeline_for "doers", project, sites, period
    end
    prepare_pie_charts_for project, sites
    nil
  end

 

  # Generates all data for the given site.
  # Calling this with generate_revisions set to false should be done daily
  # if number of visitors graphs are needed.
  # Calling this with generate_revisions set to true should be done only once.
  # Update of revisions is done using 'update_all_pages_revisions_data'.
  def self.generate_all_pages_data site, generate_revisions = false
    WikiRevisionsData.where(site: site).destroy_all if generate_revisions
    MediawikiAdapter.loop_list(site, "allpages") do |page|
      WikiPagesData.add_page_data site, page["title"]
      WikiRevisionsData.add_revisions_data site, page["title"] if generate_revisions
    end
  end

  # Updates the revisions data for all pages by regenerating them starting from the
  # given date. To update safely, don't set date to less than 30 days ago.
  def self.update_all_pages_revisions_data site, date = 2.days.ago.to_date
    WikiRevisionsData.where(:day.gte => date, site: site).destroy_all
    pages = get_updated_pages site, date
    pages.each do |page|
      WikiRevisionsData.add_revisions_data site, page, date
    end
    nil
  end

  def self.get_updated_pages site, date
    res = Set.new
    MediawikiAdapter.loop_recentchanges(site) do |entry|
      break if entry["timestamp"].to_date < date
      res << entry["title"]
    end
    res
  end

  # Should be called daily to prepare a final ready to show
  # registrations timeline graph for all sites.
  def self.prepare_registrations_timeline sites = explorable_sites
    prepare_timeline_for "registrations", Project.all_kde_project, sites
  end

  # Generates the files needed to show the wiki reports.
  def self.generate_wiki_json_file project, site, file_name, array, period = "day"
    file_name+= "_#{period_to_adj(period)}" if file_name == "doers"
    file = project.project_files_path("#{site}_wiki") + "#{file_name}.json"
    File.write(file,array.to_json)
    nil
  end

  def self.prepare_timeline_for type, project = Project.all_kde_project, sites = project.drawable_wikis, period = "day"
    per_period = Hash.new
    start_date = Date.today
    sites.each do |site|
      per_period[site], site_start_date = send("get_#{type}_per_period_for", project, site, period)
      start_date = site_start_date if site_start_date < start_date
    end
    project.set_wiki_start_date start_date unless project.all_kde_project? && type != "registrations"
    sites.each do |site|
      res = Timeline.generate_array_from_per_period_hash per_period[site], start_date, period
      generate_wiki_json_file project, site, type, res, period
    end
    nil
  end

  def self.prepare_pie_charts_for project, sites
    res = Hash.new
    sites.each do |site|
      PieChart.pie_charts_periods.each do |period|
        entries = WikiRevisionsData.entries_for(
          project,
          site,
          PieChart.pie_charts_dates[period]
        )
        next if entries.empty?
        data = PieChart.map_reduce_doers_chart entries
        res[period] = PieChart.convert_map_reduce_array_to_pie_chart data
      end
      colors = PieChart.colored_pie_charts res
      project.add_pie_cache_data "#{site}_wiki",res,colors
    end
    nil
  end

  def self.get_registrations_per_period_for project, site, period = "day"
    res = get_registrations_per_day site
    return res, res.first.first
  end

  def self.get_revisions_per_period_for project, site, period = "day"
    get_doers_per_period_for project, site, period
  end

  def self.get_doers_per_period_for project, site, period = "day"
    entries = WikiRevisionsData.entries_for project, site
    return Hash.new,Date.today if entries.empty?
    return Timeline.get_doers_line_chart_hash(entries,period), entries.min(:day).to_date
  end

  # Returns a hash with dates as keys and number of registrations on that day
  # as values. The hash is sorted(while enumerating you get earlier dates first.)
  def self.get_registrations_per_day site
    res = Hash.new(0)
    MediawikiAdapter.loop_list(site, "allusers") do |user|
      next unless user["registration"]
      res[user["registration"].to_date]+= 1
    end
    Hash[res.sort]
  end
  
  private
  
  def self.prepare_pie_charts entries, object, site
    data = PieChart.get_single_pie_charts entries, "contributor", "number"
    colors = PieChart.colored_pie_charts data
    files = PieChart.hash_for_files data, colors
    object.set_drawables "#{site}_wiki", data
    files
  end
  
  def self.prepare_line_graphs entries
    res = {revisions: Timeline.get_timeline_for(entries,"number")}
    res.merge Timeline.get_doers_per_periods(entries)
  end
end