module PlanetDataManager
  
  @@TYPE = "planet_posts"
  
  def self.prepare_report object
    entries = PlanetPostsData.entries_for object
    start_date = entries.min(:day) || Date.today
    object.set_start_date @@TYPE, start_date 
    files = prepare_line_graphs entries
    files.merge! prepare_pie_charts(entries, object)
    ReportFileManager.write_report_files(object.path_for(@@TYPE), files)
  end
  
  def self.prepare_line_graphs entries
    res = { objects: Timeline.get_timeline_for(entries, "number") }
    res.merge Timeline.get_doers_per_periods(entries)
  end
  
  def self.prepare_pie_charts entries, object
    PieChart.full_pie_chart_handler "planet_posts", entries, object
  end
  
  def self.prepare_choices_list
    res = {
      contributors: PlanetPostsData.distinct(:contributor).sort,
    }
    ReportFileManager.write_choices_file @@TYPE, res
  end
end