module BuildDataManager

  @@stats = ["UNSTABLE", "FAILURE", "SUCCESS", "ABORTED"]
  @@TYPE = "build"

  def self.prepare_report object
    objects = Hash.new
    start_date = Date.today
    entries = Hash.new
    fix_entries = BuildsFixData.entries_for object
    start_date = Date.today
    start_date = fix_entries.min(:time).to_date unless fix_entries.empty?
    @@stats.each do |status|
      cur_entries = BuildsData.entries_for object, status
      min_date = cur_entries.min(:day)
      start_date = min_date if (min_date && min_date < start_date)
      entries[status] = cur_entries
    end
    object.set_start_date "build", start_date
    entries.each do |status, ent|
      objects[status] = Timeline.get_timeline_for(ent,"number", start_date)
      pie_ent = ent.ne(contributor: nil)
      next if pie_ent.empty?
      files = prepare_pie_charts pie_ent, object, status
      ReportFileManager.write_report_files(object.path_for("build"), files)
    end
    lines = {objects: objects}
    lines.merge! average_lines(fix_entries, start_date)
    ReportFileManager.write_report_files(object.path_for("build"), lines)
  end

  def self.average_lines entries, start_date
    {
      average_hours_to_fix: Timeline.get_average_timeline_for(entries, "fix_period", start_date),
      average_tries_to_fix: Timeline.get_average_timeline_for(entries, "number", start_date)
    }
  end

  def self.prepare_pie_charts entries, object, status
    return if entries.empty?
    data = PieChart.get_single_pie_charts entries, "contributor", "number"
    colors = PieChart.colored_pie_charts data
    files = PieChart.hash_for_files data, colors
    object.set_drawables "#{status}_build", data
    Hash[files.map {|k, v| [status+"_"+k, v] }]
  end

  def self.drawables_for object
    res = Array.new
    @@stats.each do |status|
      res << status if object.drawables["#{status}_build"]
    end
    res
  end
  
  def self.all_stats
    @@stats
  end

  def self.all_jobs
    JSON.parse(open_link("https://build.kde.org/api/json?tree=jobs[name]"))["jobs"]
  end

  def self.all_jobs_selection_array
    res = Array.new
    all_jobs.each do |job|
      res << job["name"]
    end
    res
  end

  def self.daily_update
    Project.ne(title: "KDE Community").each do |p|
      p.build_jobs.reject! { |c| c.empty? }
      p.save
      prepare_report p
    end
    all = Project.all_kde_project
    all.build_jobs = all_jobs_selection_array
    all.save
    prepare_report all
  end

  def self.job_builds job
    JSON.parse(open_link("https://build.kde.org/job/#{job}/api/json?tree=builds[timestamp,result,url]"))["builds"]
  end
  
  def self.build_builder build_link
    res = JSON.parse(open_link(URI.decode(build_link)+"api/json?tree=changeSet[items[author[id]]]"))["changeSet"]["items"]
    return nil if res.length == 0
    res.first["author"]["id"]
  end

  def self.prepare_all_data
    BuildsData.destroy_all
    BuildsFixData.destroy_all
    all_jobs_selection_array.each do |job|
      job_builds(job).reverse.each do |build|
        time = Time.at(build["timestamp"]/1000)
        status = build["result"]
        contributor = build_builder(build["url"]) 
        entry = BuildsData.find_or_create_by(
          contributor: contributor,
          status: status,
          job: job,
          day: time.to_date
        )
        entry.number+= 1
        entry.save
        BuildsFixData.process_entry(job, status, time, contributor)
      end
    end
  end
  
  def self.prepare_choices_list
    res = {
      contributors: BuildsData.with_contributors.distinct(:contributor).sort,
      products: BuildsData.distinct(:job).sort
    }
    ReportFileManager.write_choices_file @@TYPE, res
  end
end