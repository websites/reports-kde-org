#!/bin/sh

while RES=$(inotifywait -e modify bots --format %f .); do
	[ "$RES" = "irc.txt" ] && cat bots/irc.txt | nohup ruby new_irc_bot.rb
	[ "$RES" = "twitter.txt" ] && cat bots/twitter.txt | nohup ruby new_twitter_stream.rb
done
