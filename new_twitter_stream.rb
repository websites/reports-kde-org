require 'yaml'
require 'tweetstream'

twitter_stream = "tmp/pids/twitter_stream.pid"

if (File.exist?(twitter_stream))
  system "kill #{File.read(twitter_stream)}"
end

File.open(twitter_stream,'w') do |f|
  f << Process.pid
end

configs = YAML.load_file("config/tokens.yml")

TweetStream.configure do |config|
  config.consumer_key       = configs["twitter"]["consumer_key"]
  config.consumer_secret    = configs["twitter"]["consumer_secret"]
  config.oauth_token        = configs["twitter"]["oauth_token"]
  config.oauth_token_secret = configs["twitter"]["oauth_token_secret"]
  config.auth_method        = :oauth
end

class Project
end

require_relative 'insert_to_mongo'

client = TweetStream::Client.new

client.on_delete do |s_id,u_id|
  # TODO: Handle delete.
end

ids = $stdin.read.split(",")
client.follow(ids) do |tweet|
  $coll.insert insertion(Project,"handle_new_tweet",tweet)
end