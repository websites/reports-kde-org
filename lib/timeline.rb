# Contains functions that can help generating Timeline graphs.
module Timeline

  # Returns an array representing the final timeline for the given entries
  # counting the given object.
  def self.get_timeline_for entries, object, start_date = nil
    hash = get_objects_line_chart_hash(entries, object)
    return generate_array_from_per_period_hash(hash, start_date.to_date) if start_date
    generate_array_from_per_period_hash(hash)
  end
  
  # Returns a hash with keys: doers_daily, doers_monthly, doers_yearly.
  # Values represent the number of unique doers for each of the periods.
  def self.get_doers_per_periods entries, start_date = nil
    res = Hash.new
    ["day","month","year"].zip(["daily","monthly","yearly"]).each do |type, name|
      hash = get_doers_line_chart_hash(entries, type) 
      res["doers_#{name}"] = generate_array_from_per_period_hash(
        hash,
        (start_date || hash.first.first),
        type
      )
    end
    res
  end
  
  # Returns a hash that represents multiline graph from the given entries
  # for the given items.  
  def self.get_multiline_graph entries, items
    res = Hash.new
    items.each do |item|
      res[item] = Timeline.get_timeline_for entries, item
    end
    res
  end
  
  def self.get_average_timeline_for entries, number, start_date
    generate_array_from_per_period_hash(
      convert_map_reduce_array_to_timeline_hash(
        map_reduce_average_line_chart(entries, number), "convert_to_f"
      ), start_date.to_date
    )
  end
  
  #------------------------------------------------------------------------------------------------------------------------
  
  # Given a hash containing numbers per day, returns an array
  # starting from start_date if given else starts from the first date in the hash.
  def self.generate_array_from_per_period_hash hash, start_date = hash.first.first, period = "day"
    res = Array.new
    # This is needed for the first iteration of the loop to work correctly.
    prev_date = initial_prev_date_for period, start_date
    hash.each do |date, number|
      num_zeros = num_zeros_for(period, date, prev_date) - 1
      res += Array.new(num_zeros,0)
      res << number
      prev_date = date
    end
    date = Date.today
    num_zeros = num_zeros_for period, date, prev_date
    res + Array.new(num_zeros,0)
  end

  # Given the array that results from a map reduce query, returns a sorted
  # hash with dates as keys and corresponding numbers as values.
  def self.convert_map_reduce_array_to_timeline_hash data, value_handler = "convert_to_i"
    res = Hash.new
    data.each do |h|
      res[h["_id"].to_date] = send(value_handler,h["value"])
    end
    Hash[res.sort]
  end

  # Specify the object that is considered the count for each entry
  # in the map function. A number can be used instead.
  def self.map_function_number_for_objects_chart object
    return object.to_s if object.class == Fixnum
    "this.#{object}"
  end

  # Performs the map reduce query on entries to get the objects line chart
  def self.map_reduce_objects_line_chart entries, object
    return [] if entries.empty?
    map = %Q{
      function() {
        emit(this.day,#{map_function_number_for_objects_chart(object)});
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values);
      };
    }
    entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
  end

  def self.map_reduce_doers_line_chart entries, type
    return [] if entries.empty?
    map = %Q{
      function() {
        date = this.day;
        date = #{get_map_date_function_for(type)};
        emit(date,this.contributor);
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.unique(values).join(',');
      };
    }
    entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
  end
  
  def self.map_reduce_average_line_chart entries, object
    return [] if entries.empty?
    map = %Q{
      function() {
        var day = new Date(Date.UTC(this.time.getFullYear(),this.time.getMonth(),this.time.getDate()));
        emit(day,#{map_function_number_for_objects_chart(object)});
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values)/values.length;
      };
    }
    entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
  end

  # Returns a hash with dates as keys and number of revisions done on that day
  # as values. The hash is sorted(while enumerating you get earlier dates first.)
  def self.get_objects_line_chart_hash entries, object = "number"
    return {Date.today => 0} if entries.empty?
    data = map_reduce_objects_line_chart entries, object
    convert_map_reduce_array_to_timeline_hash data
  end

  def self.get_doers_line_chart_hash entries, type = "day"
    return {Date.today => 0} if entries.empty?
    data = map_reduce_doers_line_chart entries, type
    convert_map_reduce_array_to_timeline_hash data, "count_unique"
  end

  # Functions that differentiate between day, month & year when generating
  # timeline graphs.

  def self.get_map_date_function_for period
    case period
    when "day"
      return "date"
    when "month"
      return "new Date(date.getFullYear(),date.getMonth(),2)"
    when "year"
      return "new Date(date.getFullYear(),1,1)"
    end
  end

  def self.initial_prev_date_for period, start_date
    case period
    when "day"
      return start_date - 1.day
    when "month"
      return start_date - 1.month
    when "year"
      return start_date - 1.year
    end
  end

  def self.num_zeros_for period, cur_date, prev_date
    case period
    when "day"
      return (cur_date - prev_date).to_i
    when "month"
      return (cur_date.year*12 + cur_date.month) - (prev_date.year*12 + prev_date.month)
    when "year"
      return cur_date.year - prev_date.year
    end
  end

  # Some custom functions that apply to value in map reduce results.

  def self.convert_to_i value
    value.to_i
  end

  def self.convert_to_f value
    value.to_f
  end

  def self.count_unique value
    value.split(',').uniq.length
  end

end