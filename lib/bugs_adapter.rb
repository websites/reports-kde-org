module BugsAdapter
  include ReportHelpers
  
  @@BASE_URL = "https://bugs.kde.org/"
  
  def search products, creators = Array.new, options = Hash.new
    # include_fields: An array of fields that should be fetched.
    # limit, offset: Used to display lists of bugs.
    # status: An array of bugs status to search for.
    options["creation_time"] ||= Date.new(2002,9,16) # KDE Bugs before that time do not contain full information so they're discarded.
    options["limit"] ||= 0 # Fetch all matching results.
    full_string = @@BASE_URL + 'jsonrpc.cgi?method=Bug.search&params=[{"product":' + products.to_json
    full_string += ',"creator":' + creators.to_jsons
    options.each do |key, value|
      full_string += ',"' + key + '":' + value.to_json
    end
    full_string += '}]'
    unparsed = open_link full_string
    parsed = JSON.parse unparsed
    return parsed["result"]["bugs"] if parsed["result"]
  end
  
end