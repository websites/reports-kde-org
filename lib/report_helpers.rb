module ReportHelpers
  
  # Converts the given date to a date that can be passed to pointStart parameter in HighStock. 
  def self.pointStart_date date
    date.to_time.to_i * 1000
  end
  
  def self.open_link link
    Curl.get(URI::encode(link)).body_str
  end
  
end