# Contains methods used to interact with MediaWiki API.
module MediawikiAdapter

  # Returns a MediawikiApi::Client initialized to the specified site.
  def self.get_client site
    MediawikiApi::Client.new "https://#{site}.kde.org/api.php"
  end

  # Given a page title, returns the info of the page in the form of a hash.
  def self.get_page_info site, page_title
    params = {
      prop: "info",
      titles: page_title
    }
    loop_through(site, params) do |page|
      return page["pages"].values.first
    end
  end

  # Loops through all revisions of the given page.
  def self.loop_revisions site, page_title
    params = {
      prop: "revisions",
      titles: page_title,
      rvprop: ["timestamp", "user"],
      rvlimit: "max"
    }
    loop_through(site, params) do |page|
      next unless page["pages"].values.first["revisions"]
      page["pages"].values.first["revisions"].each do |revision|
        yield revision
      end
    end
  end

  # Loops through some list. Currently supporting: allusers and allpages.
  def self.loop_list site, list
    limit_name = {
      "allusers" => "au",
      "allpages" => "ap"
    }[list] + "limit"
    params = {list: list, limit_name.to_sym => 'max'}
    params.merge!({ auprop: 'registration' }) if list == 'allusers'
    loop_through(site, params) do |page|
      page[list].each do |entry|
        yield entry
      end
    end
  end

  def self.loop_recentchanges site
    params = {
      list: "recentchanges",
      rcprop: ["title","timestamp"],
      rclimit: "max"
    }
    MediawikiAdapter.loop_through(site, params) do |page|
      page["recentchanges"].each do |entry|
        yield entry
      end
    end
  end

  # Loops through something that's bigger than the limit going through all pages.
  def self.loop_through site, params = {}
    client = get_client site
    params.merge!({token_type: false})
    request = client.action :query, params
    loop do
      yield request.data
      following = request["query-continue"]
      return unless following
      new_params = params.merge(following.values.first)
      request = client.action :query, new_params
    end
  end

  # Returns the content of a wiki page in html after making it suitable for direct display.
  def self.get_page_content site, page
    request = get_client(site).action :parse, page: page, prop: 'text', token_type: false
    return '' unless request.success?
    convert_to_showable site,request.data["text"]["*"]
  end

  # Given plain HTML body content of a page, converts it to HTML that can be shown in the site directly.
  def self.convert_to_showable site, text
    ng = Nokogiri::HTML(text)
    ng.css("span.editsection").remove # Remove all edit links.
    ng.css("a").each do |link|
      link["href"] = ("https://#{site}.kde.org" + link["href"]) if (link["href"][0] == '/')
      link["target"] = "_blank"
    end
    ng.to_s
  end

  # Given a string, converts it to a regexp that can be used
  # to query entries for that page and it's subpages.
  #TODO: This could live in a better place.
  def self.convert_to_wiki_regexp page
    Regexp.new("^#{page}.*")
  end

end