# Contains functions that can help generating Pie Charts.
module PieChart

  # Takes care of full pie chart generation in addition to setting drawables of objects.
  def self.full_pie_chart_handler type, entries, object, number = "number", contributor = "contributor"
    data = get_single_pie_charts entries, contributor, number
    colors = colored_pie_charts data
    files = hash_for_files data, colors
    object.set_drawables type, data
    files
  end

  # Returns a hash ready to be converted files from the given data and colors.
  def self.hash_for_files data, colors
    res = Hash.new
    data.each do |key,value|
      new_key = key.partition("_").first
      if value.class == Hash
        res["#{new_key}_all"] = {
          data: value["all"],
          colors: colors[key]["all"]
        }
        res["#{new_key}_top_10"] = {
          data: value["top_10"],
          colors: colors[key]["top_10"]
        }
      else
        next if value.length == 0
        res[new_key] = {
          data: value,
          colors: colors[key]
        }
      end
    end
    res
  end

  # Returns a hash with keys representing file names and values
  # representing number of contributions per each contributor in that period.
  def self.get_single_pie_charts entries, contributor = "contributor", number = "number"
    res = Hash.new
    PieChart.pie_charts_periods.each do |period|
      period_entries = exclude_earlier_entries entries, pie_charts_dates[period] 
      res[period] = convert_map_reduce_array_to_pie_chart(
        map_reduce_doers_chart period_entries, contributor, number
      )
    end
    res
  end

  #------------------------------------------------------------------------------------------------------------------------

  # Returns an array of possible pie charts.
  def self.pie_charts_periods
    ["total","year","month","week","today"]
  end

  def self.pie_charts_dates
    {
      "today" => Date.today,
      "week" => Date.today.at_beginning_of_week,
      "month" => Date.today.at_beginning_of_month,
      "year" => Date.today.at_beginning_of_year,
    }
  end

  def self.exclude_earlier_entries entries, cut_off
    res = entries
    res = res.where(:day.gte => cut_off) if cut_off
    res
  end

  def self.improved_pie_chart_array original
    lower_max_number = 15 #TODO: This can be made specific setting per project.
    upper_max_number = 100
    the_length = original.length
    return original if the_length < lower_max_number
    res = Hash.new()
    res["all"] = original.dup unless the_length > upper_max_number
    res["top_10"] = original.pop(10)
    others = 0
    original.each do |item|
      others+= item[1]
    end
    res["top_10"] << ["others",others]
    return res["top_10"] unless the_length <= upper_max_number
    res
  end
  
  def self.convert_hash_to_pie_chart hash
    return [] if hash.length == 0
    res = Array.new
    hash.each do |key, value|
      res << [key, value] 
    end
    res.sort_by! { |item| item.last }
  end

  def self.convert_map_reduce_array_to_pie_chart data
    return [] if data.length == 0
    data.sort_by! {|item| item["value"]}
    res = Array.new
    data.each do |entry|
      res << [entry["_id"],entry["value"].to_i] unless entry["value"] == 0
    end
    improved_pie_chart_array res
  end

  def self.map_reduce_doers_chart entries, contributor = "contributor", number = "number"
    return [] if entries.empty?
    map = %Q{
      function() {
        emit(this.#{contributor},this.#{number});
      };
    }
    reduce = %Q{
      function(key,values) {
        return Array.sum(values);
      };
    }
    entries.map_reduce(map, reduce).out(replace: "map-reduce").entries
  end

  # Returns a complex hash to be used to generate pie charts where each user has a unique color for all charts.
  #TODO: Colors can be done in a better way. Get rid of this.
  def self.colored_pie_charts data,unique_colors = Hash.new(nil)
    starting_colors = all_colors
    res = Hash.new()
    data.each do |k,v|
      next if [Fixnum].include?(v.class)
      if v.class == Array
        res[k] = Array.new()
        v.reverse.each do |item|
          color = unique_colors[item[0]]
          if color
            res[k] << color
          else
            if starting_colors.length > 0
              res[k] << starting_colors.shift
            else
              res[k] << "#" + "%06x" % (rand * 0xffffff)
            end
            unique_colors[item[0]] = res[k][-1]
          end
        end
        res[k] = res[k].reverse
      else
        unless v.values.first.class == Array
          res[k] = Project.pie_chart_colors v
        else
          res[k] = {"top_10" => Array.new, "all" => Array.new}
          v.each do |key,val|
            val.reverse.each do |item|
              color = unique_colors[item[0]]
              if color
                res[k][key] << color
              else
                if starting_colors.length > 0
                  res[k][key] << starting_colors.shift
                else
                  res[k][key] << "#" + "%06x" % (rand * 0xffffff)
                end
                unique_colors[item[0]] = res[k][key][-1]
              end
            end
            res[k][key] = res[k][key].reverse
          end
        end
      end
    end
    return res
  end

end