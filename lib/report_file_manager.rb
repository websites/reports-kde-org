module ReportFileManager
  
  def self.write_report_files path, files
    files.each do |k,v|
      File.write(path+"#{k}.json",v.to_json)
    end
  end
  
  def self.write_choices_file name, content
    File.write("public/#{name}_choices.json",content.to_json)
  end
  
end